Overview of the project
-----------------------

When I started playing Texas Holdem, I read a comment by Phil Gordon to the effect 
> Betting on the river is a simple matter of
> counting the combinations in my opponents hand range
> which beat my own hand.

Struggling to perform this feat of mental agility in the time available before the clock
ticked down on William Hill Poker, I envisaged a computer program that would not struggle.

Thus was born Bettorbettor, a tool to measure opponent hand ranges from an iPoker
session, read my own hand from the screen and calculate probabilistic betting advice.

As I got better at holdem, began seeking to win big hands at the cost of small ones,
I learnt that betting on the river is not a simple matter of counting hands that fall in a well defined range.
Quite the opposite, stacking your opponent or being stacked often happens for hands that fall
outside any 'sane' playing range.

As such, I let this project go. The code is here, however, for whoever would like to pick it up. I dare say, if complete,
betterbettor would make its user a formidable player. To my knowledge, it has the only 
non-isometric hand simulator (not all hands in a range are assumed to have equal probability) 
with an open source licence.

Structure
---------

* IPokerSpooker. This is a C# project to read hands from an iPoker chat window and spool the information
to a stream. In effect, it is a message producer for game events.

* bettoreval. This contains all the poker maths and a message consumer to pick up spooler events.

* meerkat. In theory, one should be able to hookup bettor bettor with PokerGenius via the meerkat API and thus
do manual testing against the PokerGenius robots.

* ui. An embryonic user interface to tie together the spooler and the bettoreval engine.

Prerequisites
-------------
 Java v8
 .NET 4.5/Windows7+ and msbuild or visual C#(for the IPokerSpooler, which is written in C#)
 
Building
--------

mvn clean install

Note for using IDEs: the maven build generates sources using JAXB and so you need to include generated-source
on the sourcepath.

If modifying IPokerSpooler, it is currently necessary to manually copy the IPokerSpooler.exe
file into the resources directory for net.jtownson.poker.handreplay.ipokerchat.

The build uses the umlgraph javadoc doclet (http://www.umlgraph.org) to generate class diagrams.
For this to work, you need graphviz installed and on the path (http://www.graphviz.org/).

IPoker Chat spooler
-------------------
To enable iPoker chat parsing, you need to ensure you enable all the chat options in iPoker
(with the exception of card images, which are not necessary).
