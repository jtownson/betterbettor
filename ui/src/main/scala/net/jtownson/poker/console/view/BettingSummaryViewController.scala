/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.console.view

import net.jtownson.poker.controller.DefaultActionController
import net.jtownson.poker.table.{Players, PokerPlayer, PokerTable}

/**
 * Created by jtownson on 23/01/14.
 */
class BettingSummaryViewController(heroName: String) extends DefaultActionController {

  private def hero(table: PokerTable) = table.getPlayers.getByName(heroName)

  private def playerBeforeHero(table: PokerTable) = Option(table.getPlayers.getPlayerBefore(hero(table)))

  private def isHerosTurn(table: PokerTable, lastToAct: PokerPlayer): Boolean =
    if (Some(lastToAct) == playerBeforeHero(table)) true else false

  private def isHerosFirst(table: PokerTable): Boolean = hero(table) == table.getPlayers.getSmallBlind

  private def printIfOurGo(table: PokerTable, lastToAct: PokerPlayer) =
    if (isHerosTurn(table, lastToAct)) itsYourGo

  private def printIfWereFirst(table: PokerTable) =
    if (isHerosFirst(table)) itsYourGo

  private def itsYourGo = println("It's your go, " + heroName + "!")

  var herosHand: String = ""


  override def handSeen(table: PokerTable, player: PokerPlayer, cards: String): Unit = {
    herosHand = if (heroName == player.getName) cards else ""
  }

  // Step 1 Figure out when it is hero's turn.
  // i.e. get the player to act before hero.
  // when they act, it becomes heros turn

  override def postBigBlind(table: PokerTable, player: PokerPlayer, amount: Double): Unit = printIfOurGo(table, player)

  override def folds(table: PokerTable, player: PokerPlayer): Unit = printIfOurGo(table, player)

  override def checks(table: PokerTable, player: PokerPlayer): Unit = printIfOurGo(table, player)

  override def calls(table: PokerTable, player: PokerPlayer): Unit = printIfOurGo(table, player)

  override def calls(table: PokerTable, player: PokerPlayer, amount: Double): Unit = printIfOurGo(table, player)

  override def bets(table: PokerTable, player: PokerPlayer, amount: Double): Unit = printIfOurGo(table, player)

  override def allIn(table: PokerTable, player: PokerPlayer): Unit = printIfOurGo(table, player)

  override def allIn(table: PokerTable, player: PokerPlayer, betAmount: Double): Unit = printIfOurGo(table, player)

  override def raises(table: PokerTable, player: PokerPlayer, raiseAmount: Double): Unit = printIfOurGo(table, player)

  override def dealsFlop(table: PokerTable, flop: String): Unit = printIfWereFirst(table)

  override def dealsTurn(table: PokerTable, turn: String): Unit = printIfWereFirst(table)

  override def dealsRiver(table: PokerTable, river: String): Unit = printIfWereFirst(table)

}
