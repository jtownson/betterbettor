/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.console.app

import net.jtownson.poker.handreplay.ipokerchat.{IPokerChatSpoolerProvider, IPokerChatGamePlayer}
import net.jtownson.poker.controller.{PrintlnActionController, CompositeActionController, SituationController, ModelUpdatingActionController}
import net.jtownson.poker.controller.SupervisingControllerFactory._
import net.jtownson.poker.table.PokerTable

/**
 * Created by jtownson on 21/01/14.
 */
object Main extends App {

  def herosUserName = args(0)

  def chatProvider = new IPokerChatSpoolerProvider(herosUserName)

  def pokerTable = new PokerTable(herosUserName)

  def chatPlayer = new IPokerChatGamePlayer(chatProvider, pokerTable)

  def modelController = new ModelUpdatingActionController

  def situationController = new SituationController

  def verbosePrintingController = new PrintlnActionController

  def controllerChain = new CompositeActionController(verbosePrintingController, situationController)

  def controller = SupervisingActionController(modelController, controllerChain)

  chatPlayer.start(controller)
}
