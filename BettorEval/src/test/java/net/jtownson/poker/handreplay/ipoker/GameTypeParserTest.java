package net.jtownson.poker.handreplay.ipoker;

import net.jtownson.poker.controller.GameType;
import org.junit.Test;

import static net.jtownson.poker.controller.GameType.Currency;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import static net.jtownson.poker.handreplay.ipoker.GameTypeParser.GameType;

/**
 * Created by jtownson on 16/01/14.
 */
public class GameTypeParserTest {


    @Test
    public void shouldGetNLHoldemRight() {

        // given
        String iPokerDesc = "Holdem NL $0.01/$0.02";

        // when
        GameType gameType = GameType(iPokerDesc, "USD");

        // then
        assertThat(gameType.isNoLimit(), is(true));
        assertThat(gameType.isHoldem(), is(true));
        assertThat(gameType.getSmallBlind(), is(0.01));
        assertThat(gameType.getBigBlind(), is(0.02));
    }

    @Test
    public void shouldGetPLOmahaRight() {

        // given
        String iPokerDesc = "Omaha PL $0.50/$1";

        // when
        GameType gameType = GameType(iPokerDesc, "USD");

        // then
        assertThat(gameType.isOmaha(), is(true));
        assertThat(gameType.isPotLimit(), is(true));
        assertThat(gameType.getSmallBlind(), is(0.5));
        assertThat(gameType.getBigBlind(), is(1.0));
    }

    @Test
    public void shouldParseFunMoneyFromChatWindow() {
        // given
        String iPokerDesc = "Holdem NL 1.0/2.0";

        // when
        GameType gameType = GameType(iPokerDesc);

        // then
        assertThat(gameType.isHoldem(), is(true));
        assertThat(gameType.isNoLimit(), is(true));
        assertThat(gameType.getBigBlind(), is(2.0));
        assertThat(gameType.getSmallBlind(), is(1.0));
        assertThat(gameType.getCurrencySymbol(), is("F"));
        assertThat(gameType.getTableCurrency(), is("FUN"));
    }

    @Test
    public void shouldParseFunmoneyGameType() {

        // given
        String iPokerDesc = "Holdem NL 3/6";

        // when
        GameType gameType = GameType(iPokerDesc, "F");

        // then
        assertThat(gameType.isNoLimit(), is(true));
        assertThat(gameType.isHoldem(), is(true));
        assertThat(gameType.getSmallBlind(), is(3.0));
        assertThat(gameType.getBigBlind(), is(6.0));
        assertThat(gameType.getTableCurrency(), is(Currency.FUN.toString()));
    }

}
