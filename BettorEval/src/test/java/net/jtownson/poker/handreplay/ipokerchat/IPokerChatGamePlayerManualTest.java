/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.controller.DefaultActionController;
import net.jtownson.poker.controller.ModelUpdatingActionController;
import net.jtownson.poker.controller.PrintlnActionController;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;

import java.util.Date;
import java.util.Map;

/**
 *
 */
public class IPokerChatGamePlayerManualTest {

    public static void main(String [] args) {
        if (args.length == 0) {
            System.err.println("Usage: " + IPokerChatGamePlayerManualTest.class.getSimpleName() + " <ipoker username>");
            System.exit(1);
        }
        String iPokerUsername = args[0];

        PokerTable table = new PokerTable();
        IPokerChatSpoolerProvider chatProvider = new IPokerChatSpoolerProvider(iPokerUsername);
        final IPokerChatGamePlayer gamePlayer = new IPokerChatGamePlayer(chatProvider, table);

        ActionController callback = new PrintlnActionController();

        Runtime.getRuntime().addShutdownHook(new Thread(){
            @Override
            public void run() {
                gamePlayer.stop();
            }
        });

        gamePlayer.start(callback);
    }

}
