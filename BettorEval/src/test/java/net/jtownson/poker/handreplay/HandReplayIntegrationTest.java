package net.jtownson.poker.handreplay;

import net.jtownson.poker.controller.*;
import net.jtownson.poker.handreplay.ipokerchat.CandPChatWindowSpoolerProvider;
import net.jtownson.poker.handreplay.ipokerchat.IPokerChatGamePlayer;
import net.jtownson.poker.handreplay.ipokerchat.IPokerChatProvider;
import net.jtownson.poker.table.PokerTable;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 */
public class HandReplayIntegrationTest {

    @Test
    public void stuffShouldAllWorkTogether() throws Exception {

        String herosUserName = "Fricassee";

        Path chatFilePath = Paths.get(getClass().getResource("ipokerchat/ipoker-chat-output.txt").toURI());

        IPokerChatProvider chatProvider = new CandPChatWindowSpoolerProvider(chatFilePath);

        PokerTable table = new PokerTable(herosUserName);

        IPokerChatGamePlayer chatPlayer = new IPokerChatGamePlayer(chatProvider, table);

        HandFilter filter = (t, handId) -> "12173142".equals(handId);

        ActionController modelController = new ModelUpdatingActionController();

        ActionController situationController = new SituationController(filter);

        ActionController verbosePrintingController = new PrintlnActionController(filter);

        ActionController controllerChain = new CompositeActionController(
                modelController, verbosePrintingController, situationController);

        chatPlayer.start(controllerChain);
    }

}
