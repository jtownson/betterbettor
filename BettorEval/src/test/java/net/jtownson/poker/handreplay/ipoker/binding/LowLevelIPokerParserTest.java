package net.jtownson.poker.handreplay.ipoker.binding;

import net.jtownson.poker.handreplay.ipoker.LowLevelIPokerParser;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 *
 */
public class LowLevelIPokerParserTest {

    private LowLevelIPokerParser parser = new LowLevelIPokerParser();

    @Test
    public void shouldGetCorrectPlayers() {

        // given
        InputStream is = getTestResource("GeneralHand.xml");

        // when
        Session session = parser.parseIPokerHandHistory(is);
        List<Player> players = session.getGame().get(0).getGeneral().getPlayers().getPlayer();

        // then
        assertThat(players.size(), is(4));
        assertThat(players.get(0).getName(), is("Yatze1977"));
        assertThat(players.get(1).getName(), is("pk9443"));
        assertThat(players.get(2).getName(), is("TomasKatz"));
        assertThat(players.get(3).getName(), is("GJHDEYFXFCNB"));

        assertThat(players.get(0).getSeat(), is(2));
        assertThat(players.get(1).getSeat(), is(4));
        assertThat(players.get(2).getSeat(), is(7));
        assertThat(players.get(3).getSeat(), is(9));
    }

    private InputStream getTestResource(String name) {
        return this.getClass().getResourceAsStream(name);
    }
}
