/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import net.jtownson.poker.controller.ModelUpdatingActionController;
import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.controller.GameType;
import net.jtownson.poker.controller.HandFilter;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

import static net.jtownson.poker.handreplay.ipoker.Matchers.*;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class IPokerChatGamePlayerTest {

    @Mock
    private IPokerChatSpoolerProvider chatProvider;
    @Mock
    private ActionController controller;

    private IPokerChatGamePlayer chatGamePlayer;

    private PokerTable table;

    @Before
    public void setup() {
        table = new PokerTable();
        chatGamePlayer = new IPokerChatGamePlayer(chatProvider, table);
    }

    @Test
    public void givenASplitPotShouldCorrectlyCalculateWinnings() throws Exception {

        // given
        BlockingQueue<String> chatQueue = getTestChat("chat-with-your-cards-message.txt");
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        ModelUpdatingActionController controller = new ModelUpdatingActionController(new HandFilter() {
            @Override
            public boolean acceptHand(PokerTable table, String handId) {
                return "23212320".equals(handId);
            }
        });

        // when
        chatGamePlayer.start(controller);
        Map<String, Double> winnings = table.getPlayersWinnings();

        // then
        assertThat(table.getMainPot().getPotValue(), closeTo(1325.0, 0.0001));
        assertThat(winnings.get("Fricassee"), closeTo(662.5, 0.001));
        assertThat(winnings.get("RrenanN"), closeTo(662.5, 0.001));
    }

    @Test
    public void givenChatWithAYourCardsMessageShouldCorrectlyParseThem() throws Exception {
        // given
        BlockingQueue<String> chatQueue = getTestChat("chat-with-your-cards-message.txt");
        when(controller.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(controller.acceptHand(any(PokerTable.class), eq("23212320"))).thenReturn(true);
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);
        players("RrenanN", "PerSLB", "Fricassee", "luoxingcheng");
        table.setHerosName("Fricassee");

        // when
        chatGamePlayer.start(controller);

        // then
        verify(controller).handSeen(eq(table), player("Fricassee"), eq("Js Qd"));
    }

    @Test
    public void shouldCorrectlyReplayTestHand_4548221415_from_chat() throws Exception {

        // given
        BlockingQueue<String> chatQueue = getTestChat("chat.txt");
        players("47id47", "yangyunpeng", "DO0OKIE");
        when(controller.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(controller.acceptHand(any(PokerTable.class), eq("4548221415"))).thenReturn(true);
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        // when
        chatGamePlayer.start(controller);

        // then
        InOrder sequence = inOrder(controller);
        sequence.verify(controller).startHand(eq(table), eq("4548221415"), any(Date.class));
        sequence.verify(controller).postSmallBlind(eq(table), player("yangyunpeng"), eq(0.05));
       sequence.verify(controller).postBigBlind(eq(table), player("DO0OKIE"), eq(0.10));
        sequence.verify(controller).calls(eq(table), player("47id47"), eq(0.10));
        sequence.verify(controller).calls(eq(table), player("yangyunpeng"), eq(0.05));
        sequence.verify(controller).checks(eq(table), player("DO0OKIE"));
        sequence.verify(controller).dealsFlop(eq(table), eq("4h Jd Qs"));
        sequence.verify(controller).checks(eq(table), player("yangyunpeng"));
        sequence.verify(controller).bets(eq(table), player("DO0OKIE"), eq(0.30));
        sequence.verify(controller).folds(eq(table), player("47id47"));
        sequence.verify(controller).folds(eq(table), player("yangyunpeng"));
        sequence.verify(controller).endHand(eq(table));
    }

    @Test
    public void givenHandg4548221415WhenThereIsARealControllerInstanceTheWinningsShouldBeCorrectlyCalculated() throws Exception {
        // given
        BlockingQueue<String> chatQueue = getTestChat("chat.txt");
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        ModelUpdatingActionController controller = new ModelUpdatingActionController(new HandFilter() {
            @Override
            public boolean acceptHand(PokerTable table, String handId) {
                return "4548221415".equals(handId);
            }
        });

        // when
        chatGamePlayer.start(controller);
        PokerTable table = chatGamePlayer.getTable();

        // then
        assertThat(table.getPlayersWinnings().get("47id47"), closeTo(0.0, 0.0001));
        assertThat(table.getPlayersWinnings().get("yangyunpeng"), closeTo(0.0, 0.0001));
        assertThat(table.getPlayersWinnings().get("DO0OKIE"), closeTo(0.6, 0.0001));
    }

    @Test
    public void shouldCorrectlyReplayTestHand_4548222708_from_chat() throws IOException {

        // given
        BlockingQueue<String> chatQueue = getTestChat("chat.txt");
        players("DO0OKIE", "yangyunpeng");
        table.getPlayers().setDealerForRound("DO0OKIE");
        when(controller.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(controller.acceptHand(any(PokerTable.class), eq("4548222708"))).thenReturn(true);
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        // when
        chatGamePlayer.start(controller);
        InOrder sequence = inOrder(controller);

        // then
        sequence.verify(controller).startHand(eq(table), eq("4548222708"), any(Date.class));
        sequence.verify(controller).postSmallBlind(eq(table), player("DO0OKIE"), eq(0.05));
        sequence.verify(controller).postBigBlind(eq(table), player("yangyunpeng"), eq(0.10));

        sequence.verify(controller).raises(eq(table), player("DO0OKIE"), eq(0.20));
        sequence.verify(controller).calls(eq(table), player("yangyunpeng"), eq(0.10));

        sequence.verify(controller).dealsFlop(eq(table), eq("Qh Th Ac"));

        sequence.verify(controller).bets(eq(table), player("yangyunpeng"), eq(0.40));
        sequence.verify(controller).calls(eq(table), player("DO0OKIE"), eq(0.40));

        sequence.verify(controller).dealsTurn(eq(table), eq("3c"));

        sequence.verify(controller).checks(eq(table), player("yangyunpeng"));
        sequence.verify(controller).checks(eq(table), player("DO0OKIE"));

        sequence.verify(controller).dealsRiver(eq(table), eq("3h"));

        sequence.verify(controller).bets(eq(table), player("yangyunpeng"), eq(1.20));
        sequence.verify(controller).folds(eq(table), player("DO0OKIE"));

        sequence.verify(controller).endHand(eq(table));

        assertThat(table.getPlayers().size(), is(2));
        assertThat(table.getPlayers().getButton(), isAPlayerCalled("DO0OKIE"));
        assertThat(table.getPlayers().getSmallBlind(), isAPlayerCalled("DO0OKIE"));
        assertThat(table.getPlayers().getBigBlind(), isAPlayerCalled("yangyunpeng"));
    }

    @Test
    public void givenHand4548222708WhenRealControllerShouldGetWinningsCorrect() throws Exception {

        // given
        BlockingQueue<String> chatQueue = getTestChat("chat.txt");
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        ModelUpdatingActionController controller = new ModelUpdatingActionController(new HandFilter() {
            @Override
            public boolean acceptHand(PokerTable table, String handId) {
                return "4548222708".equals(handId);
            }
        });

        // when
        chatGamePlayer.start(controller);
        PokerTable table = chatGamePlayer.getTable();

        // then
        assertThat(table.getPlayersWinnings().get("yangyunpeng"), closeTo(2.40, 0.0001));
        assertThat(table.getPlayersWinnings().get("DO0OKIE"), closeTo(0.0, 0.0001));
    }

    @Test
    public void givenHand4548222708WhenThereIsARealControllerInstanceTheWinningsShouldBeCorrect() throws Exception {
        // given
        BlockingQueue<String> chatQueue = getTestChat("chat.txt");
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        ModelUpdatingActionController controller = new ModelUpdatingActionController((table1, handId) -> "4548222708".equals(handId));

        // when
        chatGamePlayer.start(controller);
        PokerTable table = chatGamePlayer.getTable();

        // then
        assertThat(table.getPlayers().getDealtIn().size(), is(2));
        assertThat(table.getPlayers().getButton(), isAPlayerCalled("DO0OKIE"));
        assertThat(table.getPlayers().getSmallBlind(), isAPlayerCalled("DO0OKIE"));
        assertThat(table.getPlayers().getBigBlind(), isAPlayerCalled("yangyunpeng"));

        assertThat(table.getPlayersWinnings().get("yangyunpeng"), closeTo(2.40, 0.0001));
        assertThat(table.getPlayersWinnings().get("DO0OKIE"), closeTo(0.0, 0.0001));
    }

    @Test
    public void shouldWorkCorrectlyForMultipleHands() throws Exception {
        shouldCorrectlyReplayTestHand_4548221415_from_chat();
        Mockito.reset(controller, chatProvider);
        shouldCorrectlyReplayTestHand_4548222708_from_chat();
    }

    @Test
    public void shouldWorkCorrectlyForAllInWithShowdown() throws Exception {

        // given
        BlockingQueue<String> chatQueue = getTestChat("all-in-with-showdown.txt");
        when(controller.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(controller.acceptHand(any(PokerTable.class), eq("25153147"))).thenReturn(true);
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        players("GIBA10", "johncp", "inominado", "quguar");
        table.getPlayers().setDealerForRound("GIBA10");

        // when
        chatGamePlayer.start(controller);

        // then
        InOrder sequence = inOrder(controller);
        sequence.verify(controller).startHand(eq(table), eq("25153147"), any(Date.class));

        sequence.verify(controller).postSmallBlind(eq(table), player("johncp"), eq(1.0));
        sequence.verify(controller).postBigBlind(eq(table), player("inominado"), eq(2.0));

        sequence.verify(controller).calls(eq(table), player("quguar"), eq(2.0));
        sequence.verify(controller).calls(eq(table), player("GIBA10"), eq(2.0));
        sequence.verify(controller).calls(eq(table), player("johncp"), eq(1.0));
        sequence.verify(controller).checks(eq(table), player("inominado"));

        sequence.verify(controller).dealsFlop(eq(table), eq("3h 4c 9c"));

        sequence.verify(controller).checks(eq(table), player("johncp"));
        sequence.verify(controller).checks(eq(table), player("inominado"));
        sequence.verify(controller).bets(eq(table), player("quguar"), eq(197.0));
        sequence.verify(controller).folds(eq(table), player("GIBA10"));
        sequence.verify(controller).folds(eq(table), player("johncp"));
        sequence.verify(controller).allIn(eq(table), player("inominado"), eq(192.0)); // all-in for 192

        sequence.verify(controller).dealsTurn(eq(table), eq("7d"));
        sequence.verify(controller).dealsRiver(eq(table), eq("3c"));

        sequence.verify(controller).shows(eq(table), player("quguar"), eq("3h 3c 5s 7d 9c"));
        sequence.verify(controller).shows(eq(table), player("inominado"), eq("3h 3c 4d 4c Ah"));

        assertThat(table.getPlayers().size(), is(4));
        assertThat(table.getPlayers().getButton(), isAPlayerCalled("GIBA10"));
        assertThat(table.getPlayers().getSmallBlind(), isAPlayerCalled("johncp"));
        assertThat(table.getPlayers().getBigBlind(), isAPlayerCalled("inominado"));
    }

    @Test
    public void givenHand25153147WithRealControllerShouldGetWinningsCorrect() throws Exception {

        // given
        BlockingQueue<String> chatQueue = getTestChat("all-in-with-showdown.txt");
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);

        ModelUpdatingActionController controller = new ModelUpdatingActionController(new HandFilter() {
            @Override
            public boolean acceptHand(PokerTable table, String handId) {
                return "25153147".equals(handId);
            }
        });

        // when
        chatGamePlayer.start(controller);
        PokerTable table = chatGamePlayer.getTable();

        // then
        assertThat(table.getDealer().getWinningsInHand().get("inominado"), closeTo(392.0, 0.0001));
        assertThat(table.getDealer().getWinningsInHand().get("quguar"), closeTo(5, 0.0001));
    }

    @Test
    public void givenChatCausingExceptionShouldNotThrowAnException() throws IOException {
        // given
        String chatFile = "chat-causing-ex.txt";

        // when
        BlockingQueue<String> chatQueue = getTestChat(chatFile);
        when(chatProvider.getMessageQueue()).thenReturn(chatQueue);
        ModelUpdatingActionController controller = new ModelUpdatingActionController();
        chatGamePlayer.start(controller);

        // then
        // okay.
    }

    private BlockingQueue<String> getTestChat(String chatFile) throws IOException {
        InputStream chatStream = getClass().getResourceAsStream(chatFile);
        try {
            List<String> chat = IOUtils.readLines(chatStream, "UTF-8");
            return new LinkedBlockingDeque<>(chat);
        } finally {
            chatStream.close();
        }
    }

    private void players(String ... playerNames) {
        table.getPlayers().clear();
        int seat = 1;
        for (String playerName : playerNames) {
            PokerPlayer player = new PokerPlayer(playerName, seat++);
            player.dealIn();
            table.getPlayers().insert(player);
        }
    }
}
