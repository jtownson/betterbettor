package net.jtownson.poker.handreplay.ipokerchat;

import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class CandPChatWindowSpoolerProviderTest {

    @Test
    public void givenCopiedAndPastedChatShouldRelayIt() throws Exception {
        // given
        String chatFile = "ipoker-chat-output.txt";
        Path chatFilePath = Paths.get(getClass().getResource(chatFile).toURI());
        CandPChatWindowSpoolerProvider chatProvider = new CandPChatWindowSpoolerProvider(chatFilePath);

        // when
        List<String> chatList = new ArrayList<>(chatProvider.getMessageQueue());

        // then
        assertThat(chatList.get(0), is("Welcome to the table & good luck. Please note that any chat should be English only."));
        assertThat(chatList.get(6), is("RohamMJ shows Full house"));
        assertThat(chatList.get(7), is("ARTWINER shows Straight"));
    }
}
