/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class IPokerChatSpoolerProviderTest {

    @Test
    public void shouldWorkWithTheRealSpoolerExe() throws Exception {

        // given the spooler starts and exits immediately
        IPokerChatSpoolerProvider spoolerProvider = new IPokerChatSpoolerProvider("Not a real ipoker user");

        // when
        spoolerProvider.startSpooler();
        List<String> spoolerMsgs = readSpoolerMessages(spoolerProvider.getMessageQueue());
        List<String> errorMsgs = readSpoolerMessages(spoolerProvider.getErrorQueue());

        // then
        assertThat(spoolerMsgs.size(), is(0));
        assertThat(errorMsgs.size(), is(0));
    }

    private List<String> readSpoolerMessages(BlockingQueue<String> msgQueue) throws InterruptedException {
        List<String> msgs = new ArrayList<>();
        String msg = msgQueue.poll(100, TimeUnit.MILLISECONDS);
        while (msg != null) {
            msgs.add(msg);
            msg = msgQueue.poll(100, TimeUnit.MILLISECONDS);
        }
        return msgs;
    }
}
