package net.jtownson.poker.handreplay.ipoker;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class CardStringParserTest {

    @Test
    public void shouldGetTheRightAnswerForFlop() {

        // given
        String iPokerCards = "C8 SA H7";

        // when
        String normalCards = CardStringParser.parseIPokerCards(iPokerCards);

        // then
        assertThat(normalCards, is("8c As 7h"));
    }

    @Test
    public void shouldGetTheRightAnswerForPocketCards() {

        // given
        String iPokerCards = "C8 SA";

        // when
        String normalCards = CardStringParser.parseIPokerCards(iPokerCards);

        // then
        assertThat(normalCards, is("8c As"));
    }

    @Test
    public void shouldGetTheRightAnswerForTurnOrRiver() {

        // given
        String iPokerCards = "C8";

        // when
        String normalCards = CardStringParser.parseIPokerCards(iPokerCards);

        // then
        assertThat(normalCards, is("8c"));
    }

    @Test
    public void shouldParseRandom() {

        // given
        String iPokerCards = "X X";

        // when
        String normalCards = CardStringParser.parseIPokerCards(iPokerCards);

        // then
        assertThat(normalCards, is("X X"));
    }

    @Test
    public void shouldParseATen() {

        // given
        String iPokerCards = "C10 D10";

        // when
        String normalCards = CardStringParser.parseIPokerCards(iPokerCards);

        // then
        assertThat(normalCards, is("Tc Td"));
    }
}
