/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.controller.GameType;
import net.jtownson.poker.controller.HandFilter;
import net.jtownson.poker.controller.ModelUpdatingActionController;
import net.jtownson.poker.table.Players;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.nio.file.Path;
import java.util.Date;
import java.util.Map;

import static net.jtownson.poker.handreplay.ipoker.GameTypeParser.GameType;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class IPokerHistoryGamePlayerTest {

    @Mock
    private ActionController callback;

    private PokerTable table = new PokerTable();

    @Before
    public void setup() {
        when(callback.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(callback.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(true);
    }

    @Test
    public void shouldReplayHandFoldedPreflopCorrectly() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("one-hand-folded-preflop.xml"), table);

        // when
        gamePlayer.start(callback);
        InOrder sequence = inOrder(callback);

        // then
        verify(callback).startHand(eq(table), eq("4475230068"), eq(parseIsoDate("2013-01-17 18:27:56")));
        Players players = table.getPlayers();
        assertThat(players.size(), is(9));
        assertThat(players.getButton().getName(), is("ODyBAHbI4"));
        assertThat(players.getSmallBlind().getName(),  is("KAPTABPYKABE"));
        assertThat(players.getBigBlind().getName(), is("Fricassee"));

        sequence.verify(callback).postSmallBlind(eq(table), argThat(isAPlayerCalled("KAPTABPYKABE")), eq(0.01));
        sequence.verify(callback).postBigBlind(eq(table), argThat(isAPlayerCalled("Fricassee")), eq(0.02));

        sequence.verify(callback).raises(eq(table), argThat(isAPlayerCalled("Foratog27")), eq(0.08));
        sequence.verify(callback).folds(eq(table), argThat(isAPlayerCalled("mrgiraffes")));
        sequence.verify(callback).folds(eq(table), argThat(isAPlayerCalled("lilas7")));
        sequence.verify(callback).folds(eq(table), argThat(isAPlayerCalled("Demid89")));
        sequence.verify(callback).folds(eq(table), argThat(isAPlayerCalled("KIr0m4n")));
        sequence.verify(callback).folds(eq(table), argThat(isAPlayerCalled("ODyBAHbI4")));
        sequence.verify(callback).folds(eq(table), argThat(isAPlayerCalled("KAPTABPYKABE")));
        sequence.verify(callback).folds(eq(table), argThat(isAPlayerCalled("Fricassee")));

        sequence.verify(callback).endHand(eq(table));
    }

    @Test
    public void givenHandFoldedPreflopWhenRealControllerInUseThenWinningsShouldBeCorrect() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("one-hand-folded-preflop.xml"), table);
        ModelUpdatingActionController controller = new ModelUpdatingActionController((table1, handId) -> "4475230068".equals(handId));

        // when
        gamePlayer.start(controller);
        PokerTable table = gamePlayer.getTable();
        Players players = table.getPlayers();

        // then
        Map<String, Double> winnings = table.getPlayersWinnings();
        assertThat(winnings.size(), is(9));
        assertThat(winnings.get("Foratog27"), is(0.11));
        assertThat(winnings.get("KAPTABPYKABE"), is(0.0));
        assertThat(winnings.get("Fricassee"), is(0.0));
        assertThat(winnings.get("lilas7"), is(0.0));
        assertThat(winnings.get("Demid89"), is(0.0));
        assertThat(winnings.get("KIr0m4n"), is(0.0));
        assertThat(winnings.get("ODyBAHbI4"), is(0.0));
        assertThat(winnings.get("mrgiraffes"), is(0.0));


        assertThat(players.getByName("Foratog27").getStack(), is(2.40));
        assertThat(players.getByName("KAPTABPYKABE").getStack(), is(2.85));
        assertThat(players.getByName("Fricassee").getStack(), is(1.98));
        assertThat(players.getByName("mrgiraffes").getStack(), is(1.05));
    }

    ArgumentMatcher<PokerPlayer> isAPlayerCalled(final String name) {
        return Matchers.isAPlayerCalled(name);
    }

    @Test
    public void shouldGracefullyIgnoreAPLOGame() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("pot-limit-omaha.xml"), table);
        when(callback.acceptGame(any(PokerTable.class), eq(GameType("Omaha PL $0.50/$1", "USD")))).thenReturn(false);

        // when
        gamePlayer.start(callback);

        // then
        verify(callback, never()).startHand(any(PokerTable.class), any(String.class), any(Date.class));
    }

    @Test
    public void shouldGracefullyIgnoreALimitHoldemGame() {
        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("fixed-limit-holdem.xml"), table);
        when(callback.acceptGame(any(PokerTable.class), eq(GameType("Holdem L $2/$4", "USD")))).thenReturn(false);

        // when
        gamePlayer.start(callback);

        // then
        verify(callback, never()).startHand(any(PokerTable.class), any(String.class), any(Date.class));
    }

    @Test
    public void shouldParseSittingOutCaseCorrectly() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("WithSittingOut.xml"), table);

        // when
        gamePlayer.start(callback);

        // then okay.
    }

    @Test
    public void shouldFailGracefullyForInvalidHand() {
        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("invalid-hand.xml"), table);

        // when
        gamePlayer.start(callback);

        // then
        verify(callback).errorCondition(
                eq(gamePlayer.getTable()), contains("3302203753"),
                eq(ActionController.ErrorType.parsingOrFormatError));
    }

    @Test
    public void shouldParseADirectoryWithoutErrors() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("history-directory"), table);

        // when
        gamePlayer.start(callback);

        // then


    }

    // test that we don't get confused with showdowns and the way
    // ipoker records mucked cards (for collusion detection).
    @Test
    public void shouldCorrectlyAssignPotsForTheBetCallMuckScenario() {
        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("bet-call-lose-on-river.xml"), table);

        // when
        gamePlayer.start(callback);

        ArgumentCaptor<PokerTable> tableCapture = ArgumentCaptor.forClass(PokerTable.class);


        // then
        verify(callback).endHand(tableCapture.capture());
    }

    @Test
    public void givenHandLostOnRiverWhenRealControllerThenShouldGetWinningsCorrect() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("bet-call-lose-on-river.xml"), table);
        ModelUpdatingActionController controller = new ModelUpdatingActionController((table1, handId) -> "4486379797".equals(handId));

        // when
        gamePlayer.start(controller);
        PokerTable table = gamePlayer.getTable();
        Map<String, Double> winnings = table.getPlayersWinnings();

        // then
        assertThat(winnings.size(), is(5));
        assertThat(winnings.get("Marcin133z"), is(0.12));
        assertThat(winnings.get("begbie1"), is(0.0));
        assertThat(winnings.get("Fricassee"), is(0.0));
        assertThat(winnings.get("GlNeo"), is(0.0));
        assertThat(winnings.get("bluffmanxx"), is(0.0));
    }

    @Test
    public void shouldParseAFunMoneyGameCorrectly() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("fun-money.xml"), table);

        // when
        gamePlayer.start(callback);

        ArgumentCaptor<PokerTable> tableCapture = ArgumentCaptor.forClass(PokerTable.class);

        // then
        verify(callback).endHand(tableCapture.capture());
        PokerTable table = tableCapture.getValue();
        assertThat(table.getGameType().getTableCurrency(), is(GameType.Currency.FUN.toString()));
        assertThat(table.getGameType().isNoLimit(), is(true));
        assertThat(table.getGameType().isHoldem(), is(true));
        assertThat(table.getGameType().getSmallBlind(), is(3.0));
        assertThat(table.getGameType().getBigBlind(), is(6.0));

    }

    @Test
    public void givenAFunMoneyHandWhenRealControllerThenWinningsShouldBeCorrect() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("fun-money.xml"), table);
        ModelUpdatingActionController controller = new ModelUpdatingActionController(new HandFilter() {
            @Override
            public boolean acceptHand(PokerTable table, String handId) {
                return "5173125".equals(handId);
            }
        });

        // when
        gamePlayer.start(controller);
        PokerTable table = gamePlayer.getTable();
        Map<String, Double> winnings = table.getPlayersWinnings();

        // then
        assertThat(winnings.size(), is(6));
        assertThat(winnings.get("ooKKOUROSSoo"), is(30.0));
        assertThat(winnings.get("Fricassee"), is(0.0));
        assertThat(winnings.get("vxvmalicevxv"), is(0.0));
        assertThat(winnings.get("guillermo83"), is(0.0));
        assertThat(winnings.get("judit003"), is(0.0));
        assertThat(winnings.get("Str8FlushGordon"), is(0.0));
    }

    @Test
    public void shouldNotParseAHandWhenTheHandFilterRejectsIt() {

        // given
        IPokerHistoryGamePlayer gamePlayer = new IPokerHistoryGamePlayer(
                getPathTo("fun-money.xml"), table);

        when(callback.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(callback.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(false);

        // when
        gamePlayer.start(callback);

        // then
        verify(callback, never()).startHand(any(PokerTable.class), any(String.class), any(Date.class));
    }

    private Path getPathTo(String testFileName) {
        return new File(getClass().getResource(testFileName).getFile()).toPath();
    }

    private Date parseIsoDate(String date) {
        return IPokerHistoryGamePlayer.parseDate(date);
    }
}
