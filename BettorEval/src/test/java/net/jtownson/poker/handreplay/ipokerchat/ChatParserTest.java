package net.jtownson.poker.handreplay.ipokerchat;

import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static net.jtownson.poker.handreplay.ipoker.Matchers.player;
import static net.jtownson.poker.handreplay.ipokerchat.ChatParser.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class ChatParserTest {

    @Mock
    private ActionController controller;

    private String herosName = "hero";

    private PokerTable pokerTable = new PokerTable();

    private PokerPlayer hero = new PokerPlayer(herosName, 1);

    @Before
    public void setup() {

        pokerTable.getPlayers().insert(hero);
        pokerTable.setHerosName(herosName);
        dealer.playerName = herosName;

        for (ChatParser cp : ChatParser.values()) {
            cp.isDisabled = false;
            cp.pokerTable = pokerTable;
        }
    }

    @Test
    public void givenTheYourHandMessageShouldMatchIt() {
        // given
        String message1 = "Your cards";
        String message2 = "Ah";
        String message3 = "2d";


        // when
        boolean b1 = yourcards.actionApplied(message1, controller);
        boolean b2 = yourcards.actionApplied(message2, controller);
        boolean b3 = yourcards.actionApplied(message3, controller);

        // then
        verify(controller, times(1)).handSeen(eq(pokerTable), player(herosName), eq("Ah 2d"));
        assertThat(b1 && b2 && b3, is(true));
    }

    @Test
    public void givenANumberWithCommasShouldCorrectlyParseIt() {

        // given
        String message = "hero calls 4,700";

        // when
        call.actionApplied(message, controller);

        // then
        verify(controller).calls(eq(pokerTable), player("hero"), eq(4700.0));
    }

    @Test
    public void givenMainPotMessage2ShouldMatchIt() {
        // given
        String message = "Game #25153147: hero wins the main pot (390) with Two Pair - 3h 3c 4d 4c Ah";

        // when
        winsMainPot2.actionApplied(message, controller);

        // then
        verify(controller).shows(eq(pokerTable), player("hero"), eq("3h 3c 4d 4c Ah"));

    }

    @Test
    public void givenSidePotMessageShouldMatchIt() {
        // given
        String leaderMessage = "Game #24125416: hero wins the side pot (10) with Two Pair -";
        String card1 = "4s";
        String card2 = "4c";
        String card3 = "Ts";
        String card4 = "Ad";
        String card5 = "Ac";

        // when
        winsSidePot.actionApplied(leaderMessage, controller);
        winsSidePot.actionApplied(card1, controller);
        winsSidePot.actionApplied(card2, controller);
        winsSidePot.actionApplied(card3, controller);
        winsSidePot.actionApplied(card4, controller);
        winsSidePot.actionApplied(card5, controller);

        // then
        verify(controller, times(1)).shows(eq(pokerTable), player("hero"), eq("4s 4c Ts Ad Ac"));
    }

    @Test
    public void givenAWinsMainPotMessageWinsMainPotShouldMatchIt() {
        // given
        String leaderMessage = "Game #23212320: hero wins the main pot (661.50) with Two Pair -";
        String card1 = "Td";
        String card2 = "Jh";
        String card3 = "Js";
        String card4 = "Qd";
        String card5 = "Qs";

        // when
        winsMainPot.actionApplied(leaderMessage, controller);
        winsMainPot.actionApplied(card1, controller);
        winsMainPot.actionApplied(card2, controller);
        winsMainPot.actionApplied(card3, controller);
        winsMainPot.actionApplied(card4, controller);
        winsMainPot.actionApplied(card5, controller);

        // then
        verify(controller, times(1)).shows(eq(pokerTable), player("hero"), eq("Td Jh Js Qd Qs"));
    }
}
