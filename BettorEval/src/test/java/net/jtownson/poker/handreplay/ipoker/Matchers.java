/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import org.mockito.ArgumentMatcher;

import static org.mockito.Matchers.argThat;

/**
 *
 */
public class Matchers {

    public static ArgumentMatcher<PokerTable> isATableWithBoard(final String board) {
        return new ArgumentMatcher<PokerTable>() {
            @Override
            public boolean matches(Object o) {
                String s = ((PokerTable)o).getBoardCards();
                return s.equals(board);
            }
        };
    }

    public static ArgumentMatcher<PokerTable> isATableWithFlop(final String flop) {
        return new ArgumentMatcher<PokerTable>() {
            @Override
            public boolean matches(Object o) {
                String s = ((PokerTable)o).getFlop();
                return s.equals(flop);
            }
        };
    }

    public static ArgumentMatcher<PokerTable> isATableWithTurn(final String turn) {
        return new ArgumentMatcher<PokerTable>() {
            @Override
            public boolean matches(Object o) {
                String s = ((PokerTable)o).getTurn();
                return s.equals(turn);
            }
        };
    }

    public static ArgumentMatcher<PokerTable> isATableWithRiver(final String river) {
        return new ArgumentMatcher<PokerTable>() {
            @Override
            public boolean matches(Object o) {
                String s = ((PokerTable)o).getRiver();
                return s.equals(river);
            }
        };
    }

    public static PokerTable board(String board) {
        return argThat(isATableWithBoard(board));
    }

    public static PokerTable tableWithFlop(String flop) {
        return argThat(isATableWithFlop(flop));
    }

    public static PokerTable tableWithTurn(String turn) {
        return argThat(isATableWithTurn(turn));
    }

    public static PokerTable tableWithRiver(String river) {
        return argThat(isATableWithRiver(river));
    }

    public static PokerPlayer player(String name) {
        return argThat(isAPlayerCalled(name));
    }

    public static ArgumentMatcher<PokerPlayer> isAPlayerCalled(final String name) {
        return new ArgumentMatcher<PokerPlayer>() {
            @Override
            public boolean matches(Object o) {
                String s = ((PokerPlayer)o).getName();
                return s.equals(name);
            }
        };
    }
}
