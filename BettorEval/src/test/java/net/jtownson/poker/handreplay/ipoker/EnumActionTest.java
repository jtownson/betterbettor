package net.jtownson.poker.handreplay.ipoker;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class EnumActionTest {

    @Test
    public void shouldMapCorrectlyFromIPokerActionCodes() {
        assertThat(EnumAction.forCode(0), is(EnumAction.fold));
        assertThat(EnumAction.forCode(1), is(EnumAction.postSmallBlind));
        assertThat(EnumAction.forCode(2), is(EnumAction.postBigBlind));
        assertThat(EnumAction.forCode(3), is(EnumAction.call));
        assertThat(EnumAction.forCode(4), is(EnumAction.check));
        assertThat(EnumAction.forCode(5), is(EnumAction.bet));
        assertThat(EnumAction.forCode(7), is(EnumAction.allIn));
        assertThat(EnumAction.forCode(8), is(EnumAction.sitOut1));
        assertThat(EnumAction.forCode(9), is(EnumAction.sitOut2));
        assertThat(EnumAction.forCode(23), is(EnumAction.raise));
        assertThat(EnumAction.forCode(101), is(EnumAction.unknown));
    }
}
