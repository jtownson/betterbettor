package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Hand;
import ca.ualberta.cs.poker.HandEvaluator;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
  *
  */
public class HandHeapTest {

    private HandHeap handHeap = new HandHeap();

    @Test
    public void givenRepeatedAddingOfSameHandShouldCountItOnlyOnce() {

        // given
        Hand h = new Hand("Ac Ad 2s 8s 3h");

        // when
        handHeap.push(h, HandEvaluator.rankHand(h), 0.0, 0.0);
        handHeap.push(h, HandEvaluator.rankHand(h), 0.0, 0.0);
        handHeap.push(h, HandEvaluator.rankHand(h), 0.0, 0.0);

        // then
        assertThat(handHeap.getHandCategories().size(), is(1));
        assertThat(handHeap.getHandCategoriesByEquity().get(0).getHandCount(), is(1));
    }

    @Test
    public void givenMultipleAdditionOfTheSameHandButWithDifferentEquityShouldGetTheAverageEquity() {

        // given
        Hand h = new Hand("Ac Ad 2s 8s 3s");
        int r = HandEvaluator.rankHand(h);

        // when
        handHeap.push(h, r, 1, 1.0); // win with a pair of aces
        handHeap.push(h, r, 1, 0.0); // lose to a space flush
        handHeap.push(h, r, 1, 0.5); // tie
        handHeap.push(h, r, 1, 0.0); // lose to trip 8s

        // then
        assertThat(handHeap.getHandCategoriesByEquity().size(), is(1));
        assertThat(handHeap.getHandCategoriesByEquity().get(0).getHandCount(), is(1));
        assertThat(handHeap.getHandCategoriesByEquity().get(0).getEquity(), is(1.5));
    }

    @Test
    public void givenAdditionOfDifferentHandInSameCategoryWithDifferentRanksShouldCountTwoCombos() {

        // given
        Hand h1 = new Hand("Ac Ad 2s 8s 3h");
        Hand h2 = new Hand("As Ad 2s 9s 3h");

        int r1 = HandEvaluator.rankHand(h1);
        int r2 = HandEvaluator.rankHand(h2);

        // when
        handHeap.push(h1, r1, 0.0, 0.0);
        handHeap.push(h2, r2, 0.0, 0.0);

        // then
        assertThat(handHeap.getHandCategories().size(), is(1));
        assertThat(handHeap.getHandCategoriesByEquity().get(0).getHandCount(), is(2));
    }

    @Test
    public void givenAdditionOfDifferentHandsWithSameRankAndCategoryShouldCountTwoCombos() {

        // given
        Hand h1 = new Hand("Ac Ad 2s 8s 3h");
        Hand h2 = new Hand("As Ad 2s 8s 3h");

        int r1 = HandEvaluator.rankHand(h1);
        int r2 = HandEvaluator.rankHand(h2);

        // when
        handHeap.push(h1, r1, 0.0, 0.0);
        handHeap.push(h2, r2, 0.0, 0.0);

        // then
        assertThat(handHeap.getHandCategories().size(), is(1));
        assertThat(handHeap.getHandCategoriesByEquity().get(0).getHandCount(), is(2));
    }

    @Test
    public void givenSomeNonzeroWeightsShouldCalculateTheCorrectTotalWeight() {

        // given
        Hand h1 = new Hand("Ac Ad 2s 8s 3h");
        Hand h2 = new Hand("Ah As 2s 8s 3h");

        // when
        handHeap.push(h1, HandEvaluator.rankHand(h1), 0.25, 0.0);
        handHeap.push(h2, HandEvaluator.rankHand(h2), 0.75, 0.0);

        // then
        assertThat(handHeap.getHandCategories().size(), is(1));
        assertThat(handHeap.getHandCategoriesByWeight().get(0).getWeight(), is(1.0));
    }

    @Test
    public void givenSomeNonzeroEquititesShouldCalculateTheCorrectTotalEquity() {

        // given
        Hand h = new Hand("Ac Ad 2s 8s 3h");

        // when
        handHeap.push(h, HandEvaluator.rankHand(h), 0.0, 1.0);
        handHeap.push(h, HandEvaluator.rankHand(h), 0.0, 0.0);
        handHeap.push(h, HandEvaluator.rankHand(h), 0.0, 0.333);

        // then
        assertThat(handHeap.getHandCategories().size(), is(1));
        assertThat(handHeap.getHandCategoriesByWeight().get(0).getEquity(), is(1.333));
    }


    @Test
    public void givenMultipleHandsShouldCalculateCorrectWeightAndEquityAndCount() {

        // given
        Hand hand1 = new Hand("Ac Ad 2s 8s 3h");
        Hand hand2 = new Hand("Kh Ks 2s 8s 3h");
        Hand hand3 = new Hand("2c 3s 2s 8s 3h");
        int r1 = HandEvaluator.rankHand(hand1);
        int r2 = HandEvaluator.rankHand(hand2);
        int r3 = HandEvaluator.rankHand(hand3);
        HandCategory h1 = new HandCategory(hand1, r1);
        HandCategory h2 = new HandCategory(hand2, r2);
        HandCategory h3 = new HandCategory(hand3, r3);

        // when
        handHeap.push(hand1, r1, 1.0, 1.0);
        handHeap.push(hand1, r1, 1.0, 1.0);
        handHeap.push(hand1, r1, 1.0, 0.5);
        handHeap.push(hand2, r2, 4.0, 1.0);
        handHeap.push(hand2, r2, 4.0, 0.33);
        handHeap.push(hand3, r3, 6.0, 0.0);

        List<HandCategory> byEquity = handHeap.getHandCategoriesByEquity();
        List<HandCategory> byWeight = handHeap.getHandCategoriesByWeight();

        // then
        assertThat(byEquity.size(), is(3));
        assertThat(byWeight.size(), is(3));

        assertThat(byEquity.get(0), is(h1));
        assertThat(byEquity.get(1), is(h2));
        assertThat(byEquity.get(2), is(h3));

        assertThat(byEquity.get(0).getEquity(), is(2.5));
        assertThat(byEquity.get(1).getEquity(), is(1.33));
        assertThat(byEquity.get(2).getEquity(), is(0.0));

        assertThat(byWeight.get(0), is(h3));
        assertThat(byWeight.get(1), is(h2));
        assertThat(byWeight.get(2), is(h1));

        assertThat(byWeight.get(0).getWeight(), is(6.0));
        assertThat(byWeight.get(1).getWeight(), is(4.0));
        assertThat(byWeight.get(2).getWeight(), is(1.0));
    }

}
