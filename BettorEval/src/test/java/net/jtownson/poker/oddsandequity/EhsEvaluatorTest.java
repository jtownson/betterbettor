package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import org.junit.Test;

//import static net.jtownson.poker.oddsandequity.EffectiveHandStrength.iAhead;
//import static net.jtownson.poker.oddsandequity.EffectiveHandStrength.iBehind;
//import static net.jtownson.poker.oddsandequity.EffectiveHandStrength.iTied;
import static net.jtownson.poker.oddsandequity.EhsEvaluator.iAhead;
import static net.jtownson.poker.oddsandequity.EhsEvaluator.iBehind;
import static net.jtownson.poker.oddsandequity.EhsEvaluator.iTied;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 30/01/14.
 */
public class EhsEvaluatorTest {

    private EhsEvaluator ehsEvaluator = new EhsEvaluator();
    private Deck deck = new Deck();

    @Test
    public void givenTheNutsOnTheFlopEhsShouldBeCloseTo1() {

        // given
        Hand hand = new Hand("Ah Ac");
        Hand board = new Hand("Ad As 2c"); // hey, quad aces!

        // when
        Ehs effectiveHandStrength = ehsEvaluator.getEhs(deck, hand, board);
        double ehs = effectiveHandStrength.ehsAggressive;

        // then
        assertThat(ehs, closeTo(1, 0.001));
    }

    @Test
    public void givenHandPotentialsFromBillingsPaperOursShouldAgree() {

        // given
        Hand hand = new Hand("Ad Qc");
        Hand board = new Hand("3h 4c Jh");

        // when
        Ehs ehs = ehsEvaluator.getEhs(deck, hand, board);
        double[] hpTotal = ehs.hpdTotal;
        double[][] hp = ehs.hpd;
        double ppot = ehs.pPot;
        double npot = ehs.nPot;

        // then
        assertThat(hp[iAhead][iAhead], is(449005.0));
        assertThat(hp[iAhead][iTied], is(3211.0));
        assertThat(hp[iAhead][iBehind], is(169504.0));

        assertThat(hp[iTied][iAhead], is(0.0));
        assertThat(hp[iTied][iTied], is(8370.0));
        assertThat(hp[iTied][iBehind], is(540.0));

        assertThat(hp[iBehind][iAhead], is(91981.0));
        assertThat(hp[iBehind][iTied], is(1036.0));
        assertThat(hp[iBehind][iBehind], is(346543.0));

        assertThat(ppot, closeTo(0.206, 0.001));
        assertThat(npot, closeTo(0.272, 0.001));
    }

    @Test
    public void givenUniformHandWeightsThenWeightedEhsShouldBeEquivalentToUnweighted() {

        // given
        Hand hand = new Hand("Ad Qc");
        Hand board = new Hand("3h 4c Jh");
        HandWeights oppHandWeights = new HandWeights();

        // when
        Ehs ehs = ehsEvaluator.getEhs(deck, hand, oppHandWeights, board);
        double [] hpTotal = ehs.hpdTotal;
        double[][] hp = ehs.hpd;
        double ppot = ehs.pPot;
        double npot = ehs.nPot;

        // then
        assertThat(hp[iAhead][iAhead], is(449005.0));
        assertThat(hp[iAhead][iTied], is(3211.0));
        assertThat(hp[iAhead][iBehind], is(169504.0));

        assertThat(hp[iTied][iAhead], is(0.0));
        assertThat(hp[iTied][iTied], is(8370.0));
        assertThat(hp[iTied][iBehind], is(540.0));

        assertThat(hp[iBehind][iAhead], is(91981.0));
        assertThat(hp[iBehind][iTied], is(1036.0));
        assertThat(hp[iBehind][iBehind], is(346543.0));

        assertThat(ppot, closeTo(0.206, 0.001));
        assertThat(npot, closeTo(0.272, 0.001));
    }
}
