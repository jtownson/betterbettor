/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Hand;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;

import static net.jtownson.poker.oddsandequity.HandWeights.rangeBasedHandWeights;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 30/01/14.
 */
public class TrialEvaluationTest {


    @Test
    public void givenATwoWayHandShouldGetTheEquititesCorrect() {

        // given
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(new Hand("Ac Kc"), new Hand("Ad Kd")));
        Hand board = new Hand("2c 3c 4c 5d 6d");

        // when
        t.dealHands();
        t.test(board);

        // then
        assertThat(t.getRiverEquities()[0], is(1.0));
        assertThat(t.getRiverEquities()[1], is(0.0));
    }

    @Test
    public void givenAThreeWayHandShouldGetTheEquititesCorrect() {

        // given
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(new Hand("Ac Kc"), new Hand("Ad Kd"), new Hand("2s 2d")));
        Hand board = new Hand("2c 3c 4c 5d Td");

        // when
        t.dealHands();
        t.test(board);

        // then
        assertThat(t.getRiverEquities()[0], is(1.0));
        assertThat(t.getRiverEquities()[1], is(0.0));
        assertThat(t.getRiverEquities()[2], is(0.0));
    }

    @Test
    public void givenATwoWayTiedBoardShouldGetTheEquitiesCorrect() {

        // given
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(new Hand("8h 8d"), new Hand("8c 8s")));
        Hand board = new Hand("2c 3c 4c 5d Td");

        // when
        t.dealHands();
        t.test(board);

        // then
        assertThat(t.getRiverEquities()[0], is(0.5));
        assertThat(t.getRiverEquities()[1], is(0.5));
    }

    @Test
    public void givenAThreeWayBoardWithTwoTiedShouldGetTheEquitiesCorrect() {

        // given
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(new Hand("9h 9d"), new Hand("9c 9s"), new Hand("8c 8s")));
        Hand board = new Hand("2s 3c 4c 5d Td");

        // when
        t.dealHands();
        t.test(board);

        // then
        assertThat(t.getRiverEquities()[0], is(0.5));
        assertThat(t.getRiverEquities()[1], is(0.5));
        assertThat(t.getRiverEquities()[2], is(0.0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenAnInvalidBoardShouldThrowAnException() {

        // given
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(new Hand("9h 9d"), new Hand("9c 9s")));
        Hand board = new Hand("2s");

        // when
        t.dealHands();
        t.test(board);
    }

    @Test
    public void givenAnMatchupOnTheFlopShouldGetTheEquitiesCorrect() {

        // given
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(new Hand("9h 9d"), new Hand("8c 8s")));
        Hand board = new Hand("2s 3s 9s Ks Ac");

        // when
        t.dealHands();
        t.test(board);

        // then
        assertThat(t.getFlopEquities()[0], is(1.0));
        assertThat(t.getFlopEquities()[1], is(0.0));
    }

    @Test
    public void givenAnMatchupOnTheTurnShouldGetTheEquitiesCorrect() {

        // given
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(new Hand("9h 9d"), new Hand("5c 3c")));
        Hand board = new Hand("2c 6c 9s 8c Ks");

        // when
        t.dealHands();
        t.test(board);

        // then
        assertThat(t.getTurnEquities()[0], is(0.0));
        assertThat(t.getTurnEquities()[1], is(1.0));
    }

    @Test
    public void givenMultipleTrialsEquitiesShouldConvergeCorrectly() {

        // given
        Hand h1 = new Hand("9h 9d");
        Hand h2 = new Hand("5c 3c");
        TrialEvaluation t = new TrialEvaluation(Arrays.asList(h1, h2));

        Hand board1 = new Hand("2c 6c 9s 8c Ts"); // h2 wins with a flush
        Hand board2 = new Hand("2s 6s 9s 8c Ts"); // h1 wins with trip 9s

        // when
        t.dealHands();
        t.test(board1);
        t.test(board2);

        // then
        assertThat(t.getRiverEquities()[0], closeTo(0.5, 0.0001));
        assertThat(t.getRiverEquities()[1], closeTo(0.5, 0.0001));
    }

    @Test
    public void givenAImprovedHandEquitiesShouldChangeOverCourseOfHand() {

        // given
        // our AT hits top pair on the flop
        // but the 99 gets trips on the turn
        // finally, the 53c hits his flush on the river
        Hand h1 = new Hand("Ah Ts"); // watch our aces get cracked
        Hand h2 = new Hand("9h 9d");
        Hand h3 = new Hand("5c 3c");

        TrialEvaluation t = new TrialEvaluation(Arrays.asList(h1, h2, h3));

        Hand board = new Hand("Ac 6c 8s 9s Tc"); // h2 wins with a flush

        // when
        t.dealHands();
        t.test(board);

        // then h1 ahead on the flop
        assertThat(t.getFlopEquities()[0], closeTo(1.0, 0.0001));
        assertThat(t.getFlopEquities()[1], closeTo(0.0, 0.0001));
        assertThat(t.getFlopEquities()[2], closeTo(0.0, 0.0001));

        // h2 ahead on the turn
        assertThat(t.getTurnEquities()[0], closeTo(0.0, 0.0001));
        assertThat(t.getTurnEquities()[1], closeTo(1.0, 0.0001));
        assertThat(t.getTurnEquities()[2], closeTo(0.0, 0.0001));

        // h3 ahead on the river
        assertThat(t.getRiverEquities()[0], closeTo(0.0, 0.0001));
        assertThat(t.getRiverEquities()[1], closeTo(0.0, 0.0001));
        assertThat(t.getRiverEquities()[2], closeTo(1.0, 0.0001));
    }

    @Test
    public void givenSingleHandCountsASimulationShouldCompileTheHandsCorrectly() {

        // given
        // our AT hits top pair on the flop
        // but the 99 gets trips on the turn
        // finally, the 53c hits his flush on the river
        Hand h1 = new Hand("Ah Ts");
        Hand h2 = new Hand("9h 9d");
        Hand h3 = new Hand("5c 3c");

        TrialEvaluation t = new TrialEvaluation(Arrays.asList(h1, h2, h3));

        Hand board = new Hand("Ac 6c 8s 9s Tc"); // h2 wins with a flush

        // when
        t.dealHands();
        t.test(board);

        HandHeap p1sFlopHands = t.getFlopCategories()[0];
        HandHeap p1sTurnHands = t.getTurnCategories()[0];
        HandHeap p1sRiverHands = t.getRiverCategories()[0];

        HandHeap p2sFlopHands = t.getFlopCategories()[1];
        HandHeap p2sTurnHands = t.getTurnCategories()[1];
        HandHeap p2sRiverHands = t.getRiverCategories()[1];

        HandHeap p3sFlopHands = t.getFlopCategories()[2];
        HandHeap p3sTurnHands = t.getTurnCategories()[2];
        HandHeap p3sRiverHands = t.getRiverCategories()[2];

        // then
        assertThat(p1sFlopHands.getHandCategories().size(), is(1));
        assertThat(p1sTurnHands.getHandCategories().size(), is(1));
        assertThat(p1sRiverHands.getHandCategories().size(), is(1));

        assertThat(p2sFlopHands.getHandCategories().size(), is(1));
        assertThat(p2sTurnHands.getHandCategories().size(), is(1));
        assertThat(p2sRiverHands.getHandCategories().size(), is(1));

        assertThat(p3sFlopHands.getHandCategories().size(), is(1));
        assertThat(p3sTurnHands.getHandCategories().size(), is(1));
        assertThat(p3sRiverHands.getHandCategories().size(), is(1));
    }

}


