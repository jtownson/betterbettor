package net.jtownson.poker.oddsandequity;

import net.jtownson.poker.oddsandequity.IntegerRange;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 07/01/14.
 */
public class IntegerRangeTest {

    @Test
    public void shouldWorkForAnExclusiveRange() {
        // given
        IntegerRange range = new IntegerRange(10, 12);

        // when
        boolean b1 = range.contains(10);
        boolean b2 = range.contains(11);
        boolean b3 = range.contains(12);

        // then
        assertThat(b1, is(false));
        assertThat(b2, is(true));
        assertThat(b3, is(false));
    }

    @Test
    public void shouldWorkForALeftInclusiveRange() {
        // given
        IntegerRange range = new IntegerRange(true, false, 10, 12);

        // when
        boolean b1 = range.contains(10);
        boolean b2 = range.contains(11);
        boolean b3 = range.contains(12);

        // then
        assertThat(b1, is(true));
        assertThat(b2, is(true));
        assertThat(b3, is(false));
    }

    @Test
    public void shouldWorkForARightInclusiveRange() {
        // given
        IntegerRange range = new IntegerRange(false, true, 10, 12);

        // when
        boolean b1 = range.contains(10);
        boolean b2 = range.contains(11);
        boolean b3 = range.contains(12);

        // then
        assertThat(b1, is(false));
        assertThat(b2, is(true));
        assertThat(b3, is(true));
    }

    @Test
    public void shouldWorkForAnInclusiveRange() {
        // given
        IntegerRange range = new IntegerRange(true, true, 10, 12);

        // when
        boolean b1 = range.contains(10);
        boolean b2 = range.contains(11);
        boolean b3 = range.contains(12);

        // then
        assertThat(b1, is(true));
        assertThat(b2, is(true));
        assertThat(b3, is(true));
    }

}
