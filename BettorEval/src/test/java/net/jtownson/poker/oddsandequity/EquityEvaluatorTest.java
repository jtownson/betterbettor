/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static net.jtownson.poker.oddsandequity.HandWeights.rangeBasedHandWeights;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class EquityEvaluatorTest {

    private EquityEvaluator equityEvaluator = new EquityEvaluator();

    @Test
    public void givenAcesVsKingsShouldGetTheCorrectEquity() {

        // given
        Deck deck = new Deck();
        HandWeights h1 = HandWeights.rangeBasedHandWeights("AA");
        HandWeights h2 = HandWeights.rangeBasedHandWeights("KK");
        Hand board = new Hand();

        // when
        double[] equities = equityEvaluator.getEquity(deck, board, Arrays.asList(h1, h2), 0.01).getRiverEquities();

        // then
        assertThat(equities.length, is(2));
        assertThat(equities[0], closeTo(0.8194, 0.1));
        assertThat(equities[1], closeTo(0.1806, 0.1));

    }

    @Test
    public void givenAcesVsKingsVsQueensVsJacksShouldGetTheCorrectEquity() {

        // given
        Deck deck = new Deck();
        HandWeights h1 = HandWeights.rangeBasedHandWeights("AA");
        HandWeights h2 = HandWeights.rangeBasedHandWeights("KK");
        HandWeights h3 = HandWeights.rangeBasedHandWeights("QQ");
        HandWeights h4 = HandWeights.rangeBasedHandWeights("JJ");
        Hand board = new Hand();

        // when
        double[] equities = equityEvaluator.getEquity(deck, board, Arrays.asList(h1, h2, h3, h4), 0.0001).getRiverEquities();

        // then
        assertThat(equities.length, is(4));
        assertThat(equities[0], closeTo(0.5452, 0.01));
        assertThat(equities[1], closeTo(0.1781, 0.01));
        assertThat(equities[2], closeTo(0.1479, 0.01));
        assertThat(equities[3], closeTo(0.1288, 0.01));
    }

    @Test
    public void givenAcesAgainstRandomCardsShouldGetTheCorrectEquity() {

        // given
        Deck deck = new Deck();
        HandWeights h1 = HandWeights.rangeBasedHandWeights("AA");
        HandWeights h2 = new HandWeights();

        // when
        double []equities = equityEvaluator.getStartingHandEquities(deck, Arrays.asList(h1, h2), 0.0001).getRiverEquities();

        // then
        assertThat(equities.length, is(2));
        assertThat(equities[0], closeTo(0.852, 0.01));
        assertThat(equities[1], closeTo(0.148, 0.01));
    }

    @Test
    public void givenHandRangesASimulationShouldCompileTheRiverHandCountsCorrectly() {

        // given
        HandWeights h1 = rangeBasedHandWeights("ATo, ATs"); // 16 combos
        HandWeights h2 = rangeBasedHandWeights("99");       // 6 combos
        //HandWeights h3 = rangeBasedHandWeights("53s");    // 4 combos but 1 blocked by the board = 3 poss hands

        Hand board = new Hand("Ac 6c 8s 4c 5c");

        // when
        TrialEvaluation t = equityEvaluator.getEquity(new Deck(), board, Arrays.asList(h1, h2/*, h3*/), 0.0001);

        HandHeap p1sRiverHands = t.getRiverCategories()[0];
        HandHeap p2sRiverHands = t.getRiverCategories()[1];
        List<HandCategory> p1Cats = p1sRiverHands.getHandCategoriesByEquity();
        List<HandCategory> p2Cats = p2sRiverHands.getHandCategoriesByEquity();

        // then
        // on the river:

        // h1 can have a flush with combos: AxTc (i.e. AhTc, AsTc, AdTc = 3 combos)
        //    and a pair of aces with any other hand in his range: 9 combos (board is blocking Ac and any Tc hand becomes a flush not a pair)
        //    This flush is the nuts so his equity will be about 1/16

        assertThereAreTwoCategoriesIn(p1Cats);
        assertThereAreTwoCategoriesIn(p2Cats);

        assertRespectiveHandsCountsAre(p1Cats, 9, 3);
        assertRespectiveHandsCountsAre(p2Cats, 3, 3);

        assertThatAllHandWeightsSumTo(p1Cats, 1.0);
        assertThatAllHandWeightsSumTo(p2Cats, 1.0);

        assertThat(p1Cats.get(0).getWeight(), is(0.75));
        assertThat(p1Cats.get(1).getWeight(), is(0.25));

        assertThat(p2Cats.get(0).getWeight(), is(0.5));
        assertThat(p2Cats.get(1).getWeight(), is(0.5));

        asserThatTotalEquityIs(p1Cats, p2Cats, 1.0);

        assertThatEachPlayersEquityIsCorrectAndconsistentWithTrial(t, p1sRiverHands, p2sRiverHands);

    }

    private void assertThatEachPlayersEquityIsCorrectAndconsistentWithTrial(TrialEvaluation t, HandHeap p1sRiverHands, HandHeap p2sRiverHands) {
        assertThat(p1sRiverHands.getTotalEquity(), closeTo(t.getEquities().getRiverEquities()[0], 0.01));
        assertThat(p2sRiverHands.getTotalEquity(), closeTo(t.getEquities().getRiverEquities()[1], 0.01));

        assertThat(p1sRiverHands.getTotalEquity(), closeTo(0.62, 0.01));
        assertThat(p2sRiverHands.getTotalEquity(), closeTo(0.38, 0.01));
    }

    private void assertThatAllHandWeightsSumTo(List<HandCategory> cats, double sum) {
        assertThat(cats.get(0).getWeight() + cats.get(1).getWeight(), is(sum));
    }

    private void asserThatTotalEquityIs(List<HandCategory> p1Cats, List<HandCategory> p2Cats, double equity) {
        assertThat(
                 p1Cats.get(0).equity +
                 p1Cats.get(1).equity +
                 p2Cats.get(0).equity +
                 p2Cats.get(1).equity,
                 closeTo(equity, 0.01));
    }

    private void assertRespectiveHandsCountsAre(List<HandCategory> cats, int count1, int count2) {
        assertThat(cats.get(0).getHandCount(), is(count1));
        assertThat(cats.get(1).getHandCount(), is(count2));
    }

    private void assertThereAreTwoCategoriesIn(List<HandCategory> cats) {
        assertThat(cats.size(), is(2));
    }

}
