package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Hand;
import net.jtownson.poker.oddsandequity.RangeNotationParser;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class RangeNotationParserTest {

    private RangeNotationParser rangeNotationParser = new RangeNotationParser();

    @Test
    public void givenARangeWithLeadingSpaceTheHandShouldStillParse() {
        // given
        String range = " AA";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(6));
    }

    @Test
    public void testSimplePair() {
        // given
        String range = "QQ";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(6));
    }

    @Test
    public void testRangedPair() {

        // given
        String range = "QQ+";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(18));
    }

    @Test
    public void testOtherRangedPair() {

        // given
        String range = "22-66";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(30));
    }

    @Test
    public void testOffsuitConnector() {

        // given
        String range = "AJo+";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(36));
    }

    @Test
    public void testSuitedConnector() {

        // given
        String range = "AJs+";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(12));
    }

    @Test
    public void testCommaSeparation() {

        // given
        String range = "QQ+, AJo+, AJs+, 22-66";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(18 + 36 + 12 + 30));
    }

    @Test
    public void shouldParseASuitedDashPattern() {

        // given
        String range = "AQ-AKs";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(8));
    }

    @Test
    public void shouldParseAnUnSuitedDashPattern() {

        // given
        String range = "AQ-AKo";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(24));
        //for (Hand hand : )
    }

    @Test
    public void shouldNotIncludePairsInSuitedRanges() {

        // given
        String range = "98-9Ts";

        // when
        List<Hand> handRange = rangeNotationParser.parse(range);

        // then
        assertThat(handRange.size(), is(8));
    }

    @Test
    public void shouldParseARangeWithASpecificSuit() {

        // given
        String range = "AsAc";

        // when
        List<Hand> hands = rangeNotationParser.parse(range);

        // then
        assertThat(hands.size(), is(1));
        assertThat(hands.get(0), is(new Hand("As Ac")));
    }
}
