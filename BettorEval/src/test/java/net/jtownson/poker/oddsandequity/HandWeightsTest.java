package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static net.jtownson.poker.oddsandequity.HandWeights.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 08/01/14.
 */
public class HandWeightsTest {

    @Test
    public void givenWeightsOfZeroOrOneHandSampleShouldReturnOnlyNonZeroWeightedHands() {

        // given
        Deck deck = new Deck(4); // 2c, 3c, 4c, 5c
        Hand h1 = new Hand("2c 3c");
        Hand h2 = new Hand("2c 4c"); // so we assert 2c5c, 3c4c, 3c5c, 4c5c are impossible hands
        HandWeights matrix = handBasedWeights(deck, Arrays.asList(h1, h2));

        // when
        List<Hand> sampledHands = new ArrayList<>(2500);
        for (int i = 0; i < 2500; i++) {
            sampledHands.add(matrix.sample());
        }
        List<Hand> h1Samples = new ArrayList<>(sampledHands);
        h1Samples.removeIf(h -> ! h.equals(h1));
        List<Hand> h2Samples = new ArrayList<>(sampledHands);
        h2Samples.removeIf(h -> ! h.equals(h2));

        // then
        assertThat(sampledHands.contains(new Hand("2c 5c")), is(false));
        assertThat(sampledHands.contains(new Hand("3c 4c")), is(false));
        assertThat(sampledHands.contains(new Hand("3c 5c")), is(false));
        assertThat(sampledHands.contains(new Hand("4c 5c")), is(false));

        assertThat((double)h1Samples.size() / (double)h2Samples.size(), closeTo(1.0, 0.1));
    }

    @Test
    public void givenNonZeroHandWeightsSammpleShouldReturnHandsInProportion() {

        // given
        Deck deck = new Deck(4); // 2c, 3c, 4c, 5c
        Hand h1 = new Hand("2c 3c");
        Hand h2 = new Hand("2c 4c"); // so we assert 2c5c, 3c4c, 3c5c, 4c5c are impossible hands
        HandWeights matrix = handBasedWeights(deck, Arrays.asList(h1, h2));
        double w1 = matrix.weightFor(h1);

        // when
        matrix.setWeightFor(h1, 2 * w1);
        matrix.normalizeTo(1.0);
        List<Hand> sampledHands = new ArrayList<>(2500);
        for (int i = 0; i < 2500; i++) {
            sampledHands.add(matrix.sample());
        }
        List<Hand> h1Samples = new ArrayList<>(sampledHands);
        h1Samples.removeIf(h -> ! h.equals(h1));
        List<Hand> h2Samples = new ArrayList<>(sampledHands);
        h2Samples.removeIf(h -> ! h.equals(h2));

        // then
        assertThat(sampledHands.contains(new Hand("2c 5c")), is(false));
        assertThat(sampledHands.contains(new Hand("3c 4c")), is(false));
        assertThat(sampledHands.contains(new Hand("3c 5c")), is(false));
        assertThat(sampledHands.contains(new Hand("4c 5c")), is(false));

        assertThat((double)h1Samples.size() / (double)h2Samples.size(), closeTo(2.0, 0.2));
    }

    @Test
    public void pShouldSumToNForNewMatrix() {
        // given
        HandWeights handMatrix = new HandWeights();
        int numHands = handMatrix.getNumHands();

        // then
        assertThat(handMatrix.getDeltaP(), lessThan(0.00001));
        assertThat(handMatrix.getNormalization(), is((double)numHands));
        assertThat(handMatrix.sumOfWeights(), is((double)numHands));
    }

    @Test
    public void pShouldSumToNAfterFlop() {
        // given
        HandWeights handMatrix = new HandWeights();
        double numHands = handMatrix.getNumHands();

        // when
        double sum1 = handMatrix.sumOfWeights();
        handMatrix.setDeadCards(new Hand("3c 2d Ah"));
        double sum2 = handMatrix.sumOfWeights();

        // then
        assertThat(handMatrix.getDeltaP(), lessThan(0.00001));
        assertThat(sum1, is(numHands));
        assertThat(sum1, closeTo(sum2, 0.0001));

        assertThat(handMatrix.weightFor(new Hand("3c As")), is(0.0));
        assertThat(handMatrix.weightFor(new Hand("2d 7s")), is(0.0));
        assertThat(handMatrix.weightFor(new Hand("As Ah")), is(0.0));

        assertThat(handMatrix.weightFor(new Hand("As Kc")), closeTo(1.13, 0.01));
    }

    @Test
    public void pShouldSumTo1AfterTurn() {
        // given
        HandWeights handMatrix = new HandWeights();

        // when
        handMatrix.setDeadCards(new Hand("3c 2d Ah Kd"));
        handMatrix.normalizeTo(1.0);

        // then
        assertThat(handMatrix.sumOfWeights(), closeTo(1.0, 0.0001));
        assertThat(handMatrix.getDeltaP(), lessThan(0.00001));
    }

    @Test
    public void pShouldSumTo1AfterRiver() {
        // given
        HandWeights handMatrix = new HandWeights();

        // when
        handMatrix.setDeadCards(new Hand("3c 2d Ah Kd 9s"));
        handMatrix.normalizeTo(1.0);

        // then
        assertThat(handMatrix.sumOfWeights(), closeTo(1.0, 0.0001));
        assertThat(handMatrix.getDeltaP(), lessThan(0.00001));
    }

    @Test
    public void givenOneOpStartingHandWeightsShouldEqualValuesInLookup() {

        // given
        int numOpps = 1;
        Hand hand1 = new Hand("Ac Ad");
        Hand hand2 = new Hand("7s 2h");
        Hand hand3 = new Hand("3d 3c");

        // when
        HandWeights handWeights = startingEquityHandWeights(numOpps);

        // then
        assertThat(handWeights.weightFor(hand1), closeTo(0.854018, 0.000001));
        assertThat(handWeights.weightFor(hand2), closeTo(0.347153, 0.000001));
        assertThat(handWeights.weightFor(hand3), closeTo(0.542, 0.000001));

        // when
        handWeights.normalizeTo(handWeights.sumOfWeights());
        assertThat(handWeights.weightFor(hand1), closeTo(0.854018, 0.000001));
        assertThat(handWeights.weightFor(hand2), closeTo(0.347153, 0.000001));
        assertThat(handWeights.weightFor(hand3), closeTo(0.542, 0.000001));
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenARangeWithHandsOutsideTheDeckShouldGetAnException() {

        // given
        Deck deck = new Deck(13);
        String handRange = "AsAc";

        // when
        rangeBasedHandWeightsForDeck(deck, handRange);
    }

    @Test
    public void rangeBasedFactoryMethodsShouldAssignTheCorrectWeights() {

        // given
        Deck deck = new Deck(4); // 2c, 3c, 4c, 5c
        String range = "2c3c";

        // when
        HandWeights matrix = rangeBasedHandWeightsForDeck(deck, range);
        matrix.normalizeTo(1.0);

        // then
        assertThat(matrix.weightFor(new Hand("2c 3c")), is(1.0));
        assertThat(matrix.getNumHands(), is(6));
    }

}
