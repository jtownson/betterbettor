/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Card;
import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static ca.ualberta.cs.poker.Card.*;
import static org.apache.commons.math3.util.ArithmeticUtils.binomialCoefficient;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 28/01/14.
 */
public class HandEnumeratorTest {

    private HandEnumerator handEnumerator = new HandEnumerator();

    @Test(expected = IllegalArgumentException.class)
    public void givenAllDeadCardsMcEnumerateShouldThrowAnException() {

        // given
        Deck deck = new Deck(5);
        Card[] deadCards = new Card[]{Card(the_2c), Card(the_3c), Card(the_4c), Card(the_5c), Card(the_6c)};

        // when
        List<Hand> hands = new ArrayList<>();
        handEnumerator.mcEnumerate2CardHands(deck, deadCards, hand -> hands.add(hand) && hands.size() < 2);

        // then either numTrials or hands.size() must be wrong. Throw a wobbly
    }

    @Test
    public void givenOnlyOnePossiblePermutationMcEnumerateShouldInvokeItRepeatedly() {

        // given
        Deck deck = new Deck(5);
        Card[] deadCards = new Card[]{Card(the_2c), Card(the_3c), Card(the_4c)};
        Hand expectedHand = new Hand(Card(the_5c), Card(the_6c));

        // when
        List<Hand> hands = new ArrayList<>();
        handEnumerator.mcEnumerate2CardHands(deck, deadCards, hand -> hands.add(hand) && hands.size() < 100);

        // then
        assertThat(hands.size(), is(100));
        for (Hand hand : hands) {
            assertThat(hand, is(expectedHand));
        }
    }

    @Test
    public void givenCardLikeNandKShouldEnumerateCorrectNumberOfTimes() {

        // given
        int n = 52;
        int k = 5;
        sum = 0;

        // when
        handEnumerator.exEnumerate(n, k, this::sum);

        // then
        assertThat(sum, is(binomialCoefficient(n, k)));
    }

    @Test
    public void givenABiggerDeckShouldEnumerateCorrectNumTimes() {

        // given
        Deck deck = new Deck(52);

        Card[] deadCards = new Card[2];
        deadCards[0] = deck.getCard(the_Kc); // Kc
        deadCards[1] = deck.getCard(the_Ac);    // Ac

        // when
        List<Hand> handsPassed = new ArrayList<>(1225);
        handEnumerator.exEnumerateCards(deck, deadCards, 2,
                (Hand hand) -> {
                    handsPassed.add(new Hand(hand));
                });

        // then
        assertThat(handsPassed.size(), is(1225));
        for (Hand hand : handsPassed) {
            assertThat(hand.containsCard(deadCards[0]), is(false));
            assertThat(hand.containsCard(deadCards[1]), is(false));
        }

    }

    @Test
    public void givenBoardToTheRiverShouldEnumerateCorrectly() {

        // given
        Deck deck = new Deck(52);

        Card[] deadCards = new Card[4];
        deadCards[0] = deck.getCard(the_Kc);
        deadCards[1] = deck.getCard(the_Ac);
        deadCards[2] = deck.getCard(the_Kd);
        deadCards[3] = deck.getCard(the_Ad);

        // when / then
        List<Hand> handsPassed = new ArrayList<>(1712304);
        handEnumerator.exEnumerate5CardBoards(deck, deadCards,
                (Hand hand) -> {
                    handsPassed.add(new Hand(hand));
                });

        assertThat(handsPassed.size(), is(1712304)); // 48 choose 5
        for (Hand hand : handsPassed) {
            assertThat(hand.containsCard(deadCards[0]), is(false));
            assertThat(hand.containsCard(deadCards[1]), is(false));
            assertThat(hand.containsCard(deadCards[2]), is(false));
            assertThat(hand.containsCard(deadCards[3]), is(false));
        }
    }

    @Test
    public void shouldEnumerateTheCorrectNumberOfPossibleFlops() {

        // given
        Deck deck = new Deck();

        Card[] deadCards = new Card[2];
        deadCards[0] = deck.getCard(the_Kc);
        deadCards[1] = deck.getCard(the_Ac);

        // when
        handEnumerator.exEnumerateFlops(deck, deadCards, (Hand hand) -> sum += 1);

        // then
        assertThat(sum, is(19600L));
    }

    @Test
    public void givenAFlopShouldCorrectEnumerateToTheRiver() {

        // given
        Deck deck = new Deck(13); // just clubs

        Card[] deadCards = new Card[0];

        Hand flop = new Hand(deck.getCard(the_2c), deck.getCard(the_3c), deck.getCard(the_4c));
        // leaving only 5c, 6c, 7c, 8c, 9c, Tc, Jc, Qc, Kc, Ac

        // when
        List<Hand> handsPassed = new ArrayList<>(45);
        handEnumerator.exEumerateRivers(deck, deadCards, flop, (Hand hand) -> handsPassed.add(new Hand(hand)));

        // then
        assertThat(handsPassed.size(), is(45));
        for (Hand hand : handsPassed) {
            assertThat(hand.size(), is(5));
            assertThat(hand.getCard(1), is(flop.getCard(1)));
            assertThat(hand.getCard(2), is(flop.getCard(2)));
            assertThat(hand.getCard(3), is(flop.getCard(3)));

            for (int i = 0; i < flop.size(); i++) {
                assertThat(hand.getCard(4), not(is(flop.getCard(i+1))));
                assertThat(hand.getCard(5), not(is(flop.getCard(i+2))));
            }
        }
    }

    @Test
    public void givenATurnShouldCorrectEnumerateToTheRiver() {

        // given
        Deck deck = new Deck(13); // just clubs

        Card[] deadCards = new Card[0];

        Card[] flop = new Card[3];
        flop[0] = deck.getCard(the_2c);
        flop[1] = deck.getCard(the_3c);
        flop[2] = deck.getCard(the_4c);

        Card turn = deck.getCard(the_5c);// leaving only 6c, 7c, 8c, 9c, Tc, Jc, Qc, Kc, Ac

        // when
        List<Hand> handsPassed = new ArrayList<>();
        handEnumerator.exEnumerateRivers(deck, deadCards, flop, turn, (Hand hand) -> {
            handsPassed.add(new Hand(hand));
        });

        // then
        assertThat(handsPassed.size(), is(9));
        for (Hand hand : handsPassed) {
            assertThat(hand.size(), is(5));
            assertThat(hand.getCard(1), is(flop[0]));
            assertThat(hand.getCard(2), is(flop[1]));
            assertThat(hand.getCard(3), is(flop[2]));
            assertThat(hand.getCard(4), is(turn));

            for (Card aFlop : flop) {
                assertThat(hand.getCard(4), not(is(aFlop)));
                assertThat(hand.getCard(5), not(is(aFlop)));
            }

            assertThat(hand.getCard(5), not(is(turn)));
        }
    }

    private long sum = 0;

    private void sum(int[] s) {
        sum += 1;
    }

}
