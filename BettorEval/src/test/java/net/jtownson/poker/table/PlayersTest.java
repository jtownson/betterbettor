package net.jtownson.poker.table;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class PlayersTest {

    private PokerPlayer sb = new PokerPlayer("SB");
    private PokerPlayer bb = new PokerPlayer("BB");
    private PokerPlayer ep = new PokerPlayer("EP");
    private PokerPlayer mp = new PokerPlayer("MP");
    private PokerPlayer lp = new PokerPlayer("LP");
    private PokerPlayer bu  = new PokerPlayer("BU");
    private Players players = new Players(Arrays.asList(ep, mp, lp, bu, sb, bb));

    @Before
    public void setup() {
        players.dealInEverybody();
        players.setDealerForRound(bu);
    }

    @Test
    public void givenPlayersWithSomeFoldsIteratorShouldIncludeUnfolded() {

        // given
        bb.foldAndMuck();
        bu.foldAndMuck();

        // when
        Iterator<PokerPlayer> iterator = players.unfoldedPlayerIterator();

        // then
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(sb));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(ep));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(mp));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(lp));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void givenEverybodyFoldedIteratorShouldIncludeNobody() {
        // given
        sb.foldAndMuck();
        bb.foldAndMuck();
        ep.foldAndMuck();
        mp.foldAndMuck();
        lp.foldAndMuck();
        bu.foldAndMuck();

        // when
        Iterator<PokerPlayer> iterator = players.unfoldedPlayerIterator();

        // then
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void givenAllFoldedToButtonIteratorShouldIncludeOnlyButton() {
        // given
        sb.foldAndMuck();
        bb.foldAndMuck();
        ep.foldAndMuck();
        mp.foldAndMuck();
        lp.foldAndMuck();

        // when
        Iterator<PokerPlayer> iterator = players.unfoldedPlayerIterator();

        // then
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(bu));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void givenPlayersWithNoFoldsIteratorShouldIncludeAll() {

        // given
        Iterator<PokerPlayer> iterator = players.unfoldedPlayerIterator();

        // when / then
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(sb));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(bb));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(ep));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(mp));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(lp));
        assertThat(iterator.hasNext(), is(true));
        assertThat(iterator.next(), is(bu));
        assertThat(iterator.hasNext(), is(false));
    }

    @Test
    public void givenSomePlayersPlayerAfterAndBeforeShouldBeCorrect() {

        assertThat(players.getPlayerBefore(sb), nullValue());
        assertThat(players.getPlayerAfter(sb), is(bb));
        assertThat(players.getPlayerBefore(bb), is(sb));
        assertThat(players.getPlayerAfter(bb), is(ep));
        assertThat(players.getPlayerBefore(ep), is(bb));
        assertThat(players.getPlayerAfter(ep), is(mp));
        assertThat(players.getPlayerBefore(mp), is(ep));
        assertThat(players.getPlayerAfter(mp), is(lp));
        assertThat(players.getPlayerBefore(lp), is(mp));
        assertThat(players.getPlayerAfter(lp), is(bu));
        assertThat(players.getPlayerBefore(bu), is(lp));
        assertThat(players.getPlayerAfter(bu), nullValue());
    }

    @Test
    public void givenSomePlayersHaveSatOutPlayerAfterAndBeforeShouldBeCorrect() {

        // given
        mp.sitOut();

        // then
        assertThat(players.getPlayerBefore(sb), nullValue());
        assertThat(players.getPlayerAfter(sb), is(bb));
        assertThat(players.getPlayerBefore(bb), is(sb));
        assertThat(players.getPlayerAfter(bb), is(ep));
        assertThat(players.getPlayerBefore(ep), is(bb));
        assertThat(players.getPlayerAfter(ep), is(lp));
        assertThat(players.getPlayerBefore(lp), is(ep));
        assertThat(players.getPlayerAfter(lp), is(bu));
        assertThat(players.getPlayerBefore(bu), is(lp));
        assertThat(players.getPlayerAfter(bu), nullValue());
    }

    @Test
    public void givenSomePlayersShouldGetPlayersAheadAndBehindCorrect() {
        // given

        // when

        // then
        assertThat(players.getPlayersInAhead(sb).isEmpty(), is(true));
        assertThat(players.getPlayersInBehind(sb), contains(bb, ep, mp, lp, bu));

        assertThat(players.getPlayersInBehind(bu).isEmpty(), is(true));
        assertThat(players.getPlayersInAhead(bu), contains(sb, bb, ep, mp, lp));

        assertThat(players.getPlayersInAhead(mp), contains(sb, bb, ep));
        assertThat(players.getPlayersInBehind(mp), contains(lp, bu));
    }

    @Test
    public void givenSomePlayersWithFoldsShouldGetPlayersAheadAndBehindCorrect() {
        // given
        bb.foldAndMuck();
        bu.foldAndMuck();

        // when

        // then
        assertThat(players.getPlayersInAhead(sb).isEmpty(), is(true));
        assertThat(players.getPlayersInBehind(sb), contains(ep, mp, lp));

        assertThat(players.getPlayersInBehind(lp).isEmpty(), is(true));
        assertThat(players.getPlayersInAhead(lp), contains(sb, ep, mp));

        assertThat(players.getPlayersInAhead(mp), contains(sb, ep));
        assertThat(players.getPlayersInBehind(mp), contains(lp));
    }

    @Test
    public void givenThatNobodyHasActedThenFirstToActShouldBeSB() {
        // given

        // when

        // then
        assertThat(players.getNextToAct(), is(sb));
    }

    @Test
    public void givenSomePlayersItShouldGetTheNextAndPreviousToAct() {

        // given

        // when / then
        assertThat(players.getNextToAct(), is(sb));

        // when / then
        players.hasActed(sb);
        assertThat(players.getNextToAct(), is(bb));

        // when / then
        players.hasActed(bb);
        assertThat(players.getNextToAct(), is(ep));

        // when / then
        players.hasActed(ep);
        assertThat(players.getNextToAct(), is(mp));

        // when / then
        players.hasActed(mp);
        assertThat(players.getNextToAct(), is(lp));

        // when / then
        players.hasActed(lp);
        assertThat(players.getNextToAct(), is(bu));

        // when / then
        players.hasActed(bu);
        assertThat(players.getNextToAct(), is(sb));
    }

    @Test
    public void givenSomeFoldsNextAndPreviousToActShouldAccountForThis() {
        // given
        sb.foldAndMuck();
        mp.foldAndMuck();

        // when / then
        // when / then
        assertThat(players.getNextToAct(), is(bb));

        // when / then
        players.hasActed(bb);
        assertThat(players.getNextToAct(), is(ep));

        // when / then
        players.hasActed(ep);
        assertThat(players.getNextToAct(), is(lp));

        // when / then
        players.hasActed(lp);
        assertThat(players.getNextToAct(), is(bu));

        // when / then
        players.hasActed(bu);
        assertThat(players.getNextToAct(), is(bb));
    }

    @Test
    public void containsShouldWorkForPlayerName() {
        // given
        Players players = new Players();
        players.add(player("DOOKIE", 1));
        players.add(player("yangyunpeng", 2));
        players.dealInEverybody();
        players.setDealerForRound("DOOKIE");

        // when
        boolean b1 = players.contains(player("DOOKIE", 2));
        boolean b2 = players.contains(player("yangyunpeng", 1));

        // then
        assertThat(b1, is(true));
        assertThat(b2, is(true));
    }

    PokerPlayer player(String playerName, int seat) {
        return new PokerPlayer(playerName, seat);
    }

    @Test
    public void shouldWorkForFourPlayers() {
        // given
        PokerPlayer b  = new PokerPlayer("B");
        PokerPlayer sb = new PokerPlayer("SB");
        PokerPlayer bb = new PokerPlayer("BB");
        PokerPlayer co = new PokerPlayer("CO");

        Players players = new Players(Arrays.asList(b, sb, bb, co));

        // when
        players.dealInEverybody();
        players.setDealerForRound(b.getName());
        Iterator<PokerPlayer> pi = players.iterator();

        // then
        assertThat(players.getSmallBlind(), is(sb));
        assertThat(players.getBigBlind(), is(bb));
        assertThat(players.getButton(), is(b));

        assertThat(players.get(0), is(sb));
        assertThat(players.get(1), is(bb));
        assertThat(players.get(2), is(co));
        assertThat(players.get(3), is(b));

        assertThat(pi.next(), is(sb));
        assertThat(pi.next(), is(bb));
        assertThat(pi.next(), is(co));
        assertThat(pi.next(), is(b));
    }

    @Test
    public void givenOnePlayerToActNextToActShouldReturnToTheSinglePlayer() {
        // given
        PokerPlayer p = new PokerPlayer("A", 1);
        p.dealIn();
        Players players = new Players(Arrays.asList(p));

        // when
        players.hasActed(p);

        // then
        assertThat(players.getNextToAct(), is(p));
    }

    @Test
    public void givenPlayersWithContigSeatNumbersInsertAfterShouldRenumberSeats() {
        // given
        PokerPlayer a = new PokerPlayer("A", 1);
        PokerPlayer b = new PokerPlayer("B", 1);
        PokerPlayer c = new PokerPlayer("C", 1);
        PokerPlayer d = new PokerPlayer("D", 1);
        Players players = new Players(Arrays.asList(a));

        // when / then
        players.insertAfter(a, d);
        assertThat(players.get(0), is(a));
        assertThat(players.get(1), is(d));
        assertThat(a.getSeatNumber(), is(1));
        assertThat(d.getSeatNumber(), is(2));

        players.insertAfter(a, b);
        assertThat(players.get(0), is(a));
        assertThat(players.get(1), is(b));
        assertThat(players.get(2), is(d));
        assertThat(a.getSeatNumber(), is(1));
        assertThat(b.getSeatNumber(), is(2));
        assertThat(d.getSeatNumber(), is(3));

        players.insertAfter(b, c);
        assertThat(players.get(0), is(a));
        assertThat(players.get(1), is(b));
        assertThat(players.get(2), is(c));
        assertThat(players.get(3), is(d));
        assertThat(a.getSeatNumber(), is(1));
        assertThat(b.getSeatNumber(), is(2));
        assertThat(c.getSeatNumber(), is(3));
        assertThat(d.getSeatNumber(), is(4));
    }

    @Test
    public void givenNoPlayersToActNexToActShouldReturnNull() {
        // given
        PokerPlayer p = new PokerPlayer("A", 1);
        p.dealOut();
        Players players = new Players(Arrays.asList(p));

        // when
        players.hasActed(p);

        // then
        assertThat(players.getNextToAct(), nullValue());
    }

    @Test
    public void defaultInsertShouldWorkLikeListAdd() {

        // given
        PokerPlayer a = new PokerPlayer("A", 1);
        PokerPlayer b = new PokerPlayer("B", 1);
        PokerPlayer c = new PokerPlayer("C", 1);
        PokerPlayer d = new PokerPlayer("D", 1);
        a.dealIn();
        b.dealIn();
        c.dealIn();
        d.dealIn();
        Players players = new Players();

        // when / then
        players.insert(a);
        assertThat(players.get(0), is(a));
        assertThat(a.getSeatNumber(), is(1));
        assertThat(players.getNextToAct(), is(a));

        players.insert(b);
        assertThat(players.get(0), is(a));
        assertThat(players.get(1), is(b));
        assertThat(a.getSeatNumber(), is(1));
        assertThat(b.getSeatNumber(), is(2));
        assertThat(players.getNextToAct(), is(a));

        players.insert(c);
        assertThat(players.get(0), is(a));
        assertThat(players.get(1), is(b));
        assertThat(players.get(2), is(c));
        assertThat(a.getSeatNumber(), is(1));
        assertThat(b.getSeatNumber(), is(2));
        assertThat(c.getSeatNumber(), is(3));
        assertThat(players.getNextToAct(), is(a));

        players.insert(d);
        assertThat(players.get(0), is(a));
        assertThat(players.get(1), is(b));
        assertThat(players.get(2), is(c));
        assertThat(players.get(3), is(d));

        assertThat(a.getSeatNumber(), is(1));
        assertThat(b.getSeatNumber(), is(2));
        assertThat(c.getSeatNumber(), is(3));
        assertThat(d.getSeatNumber(), is(4));
        assertThat(players.getNextToAct(), is(a));
    }

    @Test
    public void shouldWorkForThreePlayers() {
        // given
        PokerPlayer b  = new PokerPlayer("B");
        PokerPlayer sb = new PokerPlayer("SB");
        PokerPlayer bb = new PokerPlayer("BB");

        Players players = new Players(Arrays.asList(sb, bb, b));

        // when
        players.dealInEverybody();
        players.setDealerForRound(b.getName());
        Iterator<PokerPlayer> pi = players.iterator();

        // then
        assertThat(players.getSmallBlind(), is(sb));
        assertThat(players.getBigBlind(), is(bb));
        assertThat(players.getButton(), is(b));

        assertThat(players.get(0), is(sb));
        assertThat(players.get(1), is(bb));
        assertThat(players.get(2), is(b));

        assertThat(pi.next(), is(sb));
        assertThat(pi.next(), is(bb));
        assertThat(pi.next(), is(b));
    }

    @Test
    public void shouldWorkForTwoPlayers() {
        // given
        PokerPlayer b = new PokerPlayer("B", 0.0, 3);
        PokerPlayer bb = new PokerPlayer("BB", 0.0, 6);

        Players players = new Players(Arrays.asList(bb, b));

        // when
        players.dealInEverybody();
        players.setDealerForRound(b.getName());

        // then
        assertThat(players.getSmallBlind(), is(b));
        assertThat(players.getButton(), is(b));
        assertThat(players.getBigBlind(), is(bb));
    }
}
