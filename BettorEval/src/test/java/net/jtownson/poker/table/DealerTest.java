/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class DealerTest {

    private PokerPlayer p1 = new PokerPlayer("p1", 200, 1);
    private PokerPlayer p2 = new PokerPlayer("p2", 200, 2);
    private PokerPlayer p3 = new PokerPlayer("p3", 200, 3);
    private PokerPlayer p4 = new PokerPlayer("p4", 150, 3);
    private Players players = new Players(Arrays.asList(p1, p2, p3, p4));
    private PokerTable table = new PokerTable(players);
    private Dealer dealer = new Dealer(table);

    @Before
    public void setup() {
        players.dealInEverybody();
    }

    @Test
    public void shouldCreatePotsAndSidePotsCorrectlyForASimpleBettingScenario() {

        // given
        p1.putIn(100); // open raise
        p2.putIn(100); // call
        p3.putIn(200); // re-raise
        p4.putIn(150); // all-in
        p1.putIn(100); // call
        p2.foldAndMuck();

        // when
        dealer.gatherBets();
        List<Pot> pots = table.getPots();

        // then
        assertThat(pots.size(), is(2));
        assertThat(pots.get(0).getPotValue(), is(550.0));
        assertThat(pots.get(1).getPotValue(), is(100.0));
    }

    @Test
    public void shouldAssignWinningsCorrectly() {

        // given
        p1.setPocketCards("Kc Kd");
        p3.setPocketCards("Qc Qd");
        p4.setPocketCards("Ac Ad");

        // main pot is p1, p3 and p4
        // side pot is p1 and p3
        p1.putIn(100);
        p2.putIn(100);
        p3.putIn(200);
        p4.putIn(150); // all-in
        p1.putIn(100);
        p2.foldAndMuck();

        // when
        dealer.gatherBets();
        table.flopIs("Ah Kh Qh"); // never mind the tableWithTurn or tableWithRiver, hands will evaluate here anyhow.
        dealer.awardPots();

        // then
        assertThat(p1.getStack(), is(100.0));
        assertThat(p2.getStack(), is(100.0));
        assertThat(p3.getStack(), is(000.0));
        assertThat(p4.getStack(), is(550.0));
    }

    @Test
    public void shouldReturnUncalledAllInBetsToPlayer() {

        // given
        // somebody bets 200 and only gets 150 called, he should be refunded 50 from the pot
        p1.foldAndMuck();
        p2.foldAndMuck();
        p3.putIn(200);
        p4.putIn(150);
        p3.setShowDownHand("3h 3c 5s 7d 9c");
        p4.setShowDownHand("3h 3c 4d 4c Ah");

        // when
        dealer.awardPots();

        // then
        assertThat(p4.getStack(), is(300.0));
        assertThat(p3.getStack(), is(50.0));
    }

    @Test
    public void shouldGetCorrectAmountToCall() {

        // given
        p1.putIn(10.0);
        p2.putIn(10.0);
        p3.putIn(25.0);

        // when
        double callAmnt = dealer.getAmountToCall();

        // then
        assertThat(callAmnt, is(25.0));
    }

}
