/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class PokerTableTest {

    @Test
    public void setBoardCardsShouldNotFailForEmptyString() {

        // given
        PokerTable pokerTable = new PokerTable();
        String board = "";

        // when
        pokerTable.setBoardCards(board);

        // then
        assertThat(pokerTable.getBoardCards(), is(""));
    }

    @Test
    public void setBoardCardsShouldNotFailForSingleCard() {

        // given
        PokerTable pokerTable = new PokerTable();
        String board = "Ah";

        // when
        pokerTable.setBoardCards(board);

        // then
        assertThat(pokerTable.getBoardCards(), is("Ah"));

    }

    @Test
    public void setBoardCardsShouldNotFailForFullBoard() {

        // given
        PokerTable pokerTable = new PokerTable();
        String board = "Ah Kd 8s 2d 9h";

        // when
        pokerTable.setBoardCards(board);

        // then
        assertThat(pokerTable.getBoardCards(), is(board));

    }

    @Test
    public void getPlayersWinningsShouldWorkCorrectlyForSidePots() {

        // given
        PokerPlayer p1 = new PokerPlayer("p1", 200.0);
        PokerPlayer p2 = new PokerPlayer("p2", 200.0);
        PokerPlayer p3 = new PokerPlayer("p3", 200.0);
        PokerPlayer p4 = new PokerPlayer("p4", 150.0);
        Players players = new Players(Arrays.asList(p1, p2, p3, p4));
        players.dealInEverybody();
        PokerTable table = new PokerTable(players);

        // when
        p1.foldAndMuck();
        p2.foldAndMuck();
        p3.putIn(200);
        p4.putIn(150);
        p3.setShowDownHand("3h 3c 5s 7d 9c");
        p4.setShowDownHand("3h 3c 4d 4c Ah");
        table.getDealer().awardPots();

        Map<String, Double> winnings = table.getDealer().getWinningsInHand();

        // then
        assertThat(winnings.get("p3"), is(50.0));
        assertThat(winnings.get("p4"), is(300.0));
    }
}
