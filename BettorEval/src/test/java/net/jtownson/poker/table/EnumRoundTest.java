package net.jtownson.poker.table;

import org.junit.Test;

import static net.jtownson.poker.table.EnumRound.*;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 15/01/14.
 */
public class EnumRoundTest {

    @Test
    public void givenIncrementingRoundsNextRoundShouldReturnCorrectly() {

        // given
        EnumRound initialRound = flop;

        // when
        EnumRound r1 = initialRound.next();
        EnumRound r2 = r1.next();
        EnumRound r3 = r2.next();
        EnumRound r4 = r3.next();
        EnumRound r5 = r4.next();

        // then
        assertThat(r1, is(turn));
        assertThat(r2, is(river));
        assertThat(r3, is(blinds));
        assertThat(r4, is(preFlop));
        assertThat(r5, is(flop));
    }

}
