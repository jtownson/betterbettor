/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyDouble;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by jtownson on 21/01/14.
 */

@RunWith(MockitoJUnitRunner.class)
public class CompositeActionControllerTest {

    @Mock
    private ActionController controller1;

    @Mock
    private ActionController controller2;

    private CompositeActionController compositeController;

    @Before
    public void setup() {
        compositeController = new CompositeActionController(controller1, controller2);
    }

    @Test
    public void givenTwoControllersTheCompositeShouldInvokeActionsOnBoth() {

        // given
        PokerTable table = new PokerTable();

        // when
        compositeController.dealsFlop(table, "Ah");

        // then
        verify(controller1).dealsFlop(eq(table), eq("Ah"));
        verify(controller2).dealsFlop(eq(table), eq("Ah"));
    }

    @Test
    public void givenAGameFilterShouldHonourIt() {

        // given
        PokerTable table = new PokerTable();
        PokerPlayer player = new PokerPlayer("Stu", 1);

        when(controller1.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(controller2.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(false);

        when(controller1.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(true);
        when(controller2.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(true);

        // when
        compositeController.acceptGame(table, table.getGameType());
        compositeController.acceptHand(table, "foo");
        compositeController.postSmallBlind(table, player, 10);

        // then
        verify(controller1).postSmallBlind(eq(table), eq(player), anyDouble());
        verify(controller2, never()).postSmallBlind(any(PokerTable.class), any(PokerPlayer.class), anyDouble());
    }

    @Test
    public void givenAHandFilterShouldHonourIt() {

        // given
        PokerTable table = new PokerTable();
        PokerPlayer player = new PokerPlayer("Stu", 1);

        when(controller1.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(controller2.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(false);

        when(controller1.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(false);
        when(controller2.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(true);

        // when
        compositeController.acceptGame(table, table.getGameType());
        compositeController.acceptHand(table, "foo");
        compositeController.postSmallBlind(table, player, 10);

        // then
        verify(controller1, never()).postSmallBlind(any(PokerTable.class), any(PokerPlayer.class), anyDouble());
        verify(controller2, never()).postSmallBlind(any(PokerTable.class), any(PokerPlayer.class), anyDouble());
    }

    @Test
    public void givenGameAndHandFiltersShouldHonourThem() {

        // given
        PokerTable table = new PokerTable();
        PokerPlayer player = new PokerPlayer("Stu", 1);

        when(controller1.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(true);
        when(controller2.acceptGame(any(PokerTable.class), any(GameType.class))).thenReturn(false);

        when(controller1.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(true);
        when(controller2.acceptHand(any(PokerTable.class), any(String.class))).thenReturn(false);

        // when
        boolean g = compositeController.acceptGame(table, table.getGameType());
        boolean h = compositeController.acceptHand(table, "id");

        compositeController.startGame(table);
        compositeController.startHand(table, "id", new Date());

        // then
        assertThat(g, is(true));
        assertThat(h, is(true));

        verify(controller1).startGame(eq(table));
        verify(controller1).startHand(eq(table), eq("id"), any(Date.class));
        verify(controller2, never()).startGame(eq(table));
        verify(controller2, never()).startHand(any(PokerTable.class), any(String.class), any(Date.class));
    }
}
