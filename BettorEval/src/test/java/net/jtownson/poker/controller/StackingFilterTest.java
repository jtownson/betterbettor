/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import org.junit.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 22/01/14.
 */

public class StackingFilterTest {

    @Test
    public void givenOneLevelIncludesShouldWork() {

        // given
        List<String> l = Arrays.asList("A", "B", "C", "D", "E", "F");

        // when
        StackingFilter<String> filter = new StackingFilter<>(l);

        // then
        for (String s : l) {
            assertThat(filter.isIncluded(s), is(true));
            assertThat(filter.isExcluded(s), is(false));
        }

        assertThat(filter.isIncluded("Z"), is(false));
        assertThat(filter.isExcluded("Z"), is(true));
    }

    @Test
    public void exclusionAndInclusionShouldWork() {
        // given
        List<String> l = Arrays.asList("A", "B", "C", "D", "E", "F");

        // when
        StackingFilter<String> filter = new StackingFilter<>(l);
        filter.exclude("B");
        filter.include("Z");

        // then
        for (String s : Arrays.asList("A", "C", "D", "E", "F", "Z")) {
            assertThat(filter.isIncluded(s), is(true));
            assertThat(filter.isExcluded(s), is(false));
        }

        assertThat(filter.isIncluded("B"), is(false));
        assertThat(filter.isExcluded("B"), is(true));
    }

    @Test
    public void nextLevelShouldWork() {
        // given
        List<String> l = Arrays.asList("A", "B", "C", "D", "E", "F");
        StackingFilter<String> filter = new StackingFilter<>(l);

        // when
        filter.pushLevel();
        filter.exclude("B");
        filter.include("Z");

        // then
        for (String s : Arrays.asList("A", "C", "D", "E", "F", "Z")) {
            assertThat(filter.isIncluded(s), is(true));
            assertThat(filter.isExcluded(s), is(false));
        }
        assertThat(filter.isIncluded("B"), is(false));
        assertThat(filter.isExcluded("B"), is(true));

        // and when
        filter.popLevel();

        // then
        for (String s : l) {
            assertThat(filter.isIncluded(s), is(true));
            assertThat(filter.isExcluded(s), is(false));
        }
    }

    @Test
    public void givenAFilterElementOrderingShouldRemainAsClientSetIt() {
        // given
        List<String> l = Arrays.asList("A", "B", "C", "D", "E", "F");
        StackingFilter<String> filter = new StackingFilter<>(l);

        // when
        Set<String> s = filter.included();
        Iterator<String> i = s.iterator();

        // then
        assertThat(i.next(), is("A"));
        assertThat(i.next(), is("B"));
        assertThat(i.next(), is("C"));
        assertThat(i.next(), is("D"));
        assertThat(i.next(), is("E"));
        assertThat(i.next(), is("F"));
    }
}
