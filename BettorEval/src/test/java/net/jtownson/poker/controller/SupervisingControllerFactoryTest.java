/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import net.jtownson.poker.table.PokerTable;
import net.jtownson.poker.view.TableView;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static net.jtownson.poker.controller.SupervisingControllerFactory.SupervisingActionController;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by jtownson on 21/01/14.
 */
@RunWith(MockitoJUnitRunner.class)
public class SupervisingControllerFactoryTest {

    @Mock
    private ModelUpdatingActionController modelUpdatingController;

    @Mock
    private ActionController viewUpdatingController;

    private ActionController supervisingController;

    @Before
    public void setup() {
        supervisingController = SupervisingActionController(modelUpdatingController, viewUpdatingController);
    }

    @Test
    public void givenAControllerRequestShouldInvokeBothControllers() {

        // given
        PokerTable table = new PokerTable();

        // when
        supervisingController.acceptHand(table, "foo");

        // then
        verify(modelUpdatingController).acceptHand(eq(table), eq("foo"));
        verify(viewUpdatingController).acceptHand(eq(table), eq("foo"));
    }

}
