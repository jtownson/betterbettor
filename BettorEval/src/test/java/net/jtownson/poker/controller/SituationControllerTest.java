/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import net.jtownson.poker.table.Players;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import net.jtownson.poker.table.Situation;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jtownson on 26/01/14.
 */
public class SituationControllerTest {


    private ActionController muc = new ModelUpdatingActionController();
    private SituationController sc = new SituationController();
    private ActionController controller = new CompositeActionController(muc, sc);

    private PokerPlayer sb = new PokerPlayer("SB", 1);
    private PokerPlayer bb = new PokerPlayer("BB", 2);
    private PokerPlayer ep = new PokerPlayer("EP", 3);
    private PokerPlayer mp = new PokerPlayer("MP", 4);
    private PokerPlayer lp = new PokerPlayer("LP", 5);
    private PokerPlayer bu  = new PokerPlayer("BU", 6);
    private Players players = new Players(sb, bb, ep, mp, lp, bu);
    private PokerTable table = new PokerTable(players);


    @Before
    public void setup() {
        players.dealInEverybody();
        players.setDealerForRound(bu);
    }

    @Test
    public void givenAWorkADayScenarioShouldGetTheSituationCorrect() {

        // given
        controller.startHand(table, "foo", new Date());
        controller.sitsOut(table, lp);
        controller.postSmallBlind(table, sb, 1);
        controller.postBigBlind(table, bb, 2);
        controller.folds(table, ep);

        // when
        Situation situation = sc.getCurrentSituation();

        // then
        assertThat(situation.getPlayer(), is(mp));
        assertThat(situation.getPotSize(), closeTo(3.0, 0.0001));
        assertThat(situation.getAmountToCall(), closeTo(2, 0.0001));
        assertThat(situation.getPlayersAhead(), is(2));
        assertThat(situation.getPlayersBehind(), is(1));
    }

}
