package ca.ualberta.cs.poker;

import org.junit.Test;

import static ca.ualberta.cs.poker.Card.Card;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 */
public class HandTest {

    @Test
    public void testSorting() {
        // given
        Hand h1 = new Hand("Ac 3s 4d 5c 7d");
        Hand h2 = new Hand("3s Ac 5c 7d 4d");

        // then
        assertThat(h1, equalTo(h2));
    }

    @Test
    public void testSortingAfterAddition() {
        // given
        Hand h1 = new Hand("Ac 3s 4d 5c 7d");
        Hand h2 = new Hand("3s Ac 5c 7d 4d");
        Card c1 = Card("Ah");
        Card c2 = Card("9s");

        // when
        h1.addCard(c1);
        h1.addCard(c2);
        h2.addCard(c2);
        h2.addCard(c1);

        // then
        assertThat(h1, equalTo(h2));
    }

    @Test
    public void testSortingAfterIndexAddition() {
        // given
        Hand h1 = new Hand("Ac 3s 4d 5c 7d");
        Hand h2 = new Hand("3s Ac 5c 7d 4d");

        // when
        h1.addCard(30);
        h1.addCard(31);
        h2.addCard(31);
        h2.addCard(30);

        // then
        assertThat(h1, equalTo(h2));
    }

    @Test
    public void testAddCardsAcceptsSevenCardHand() {

        // given
        Hand h = new Hand("Ac 3s 4d 5c 7d");

        // when
        boolean b1 = h.addCard(Card("2s"), Card("2c"));

        // then
        assertThat(b1, is(true));
    }

    @Test
    public void testAddCardsDoesNotAcceptsEightCardHand() {

        // given
        Hand h = new Hand("Ac 3s 4d 5c 7d");

        // when
        boolean b1 = h.addCard(Card("2s"), Card("2c"), Card("2d"));

        // then
        assertThat(b1, is(false));
    }

}
