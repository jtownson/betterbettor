package ca.ualberta.cs.poker;

/***************************************************************************
 Copyright (c) 2000:
 University of Alberta,
 Deptartment of Computing Science
 Computer Poker Research Group

 See "Liscence.txt"
 ***************************************************************************/

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.Arrays;
import java.util.StringTokenizer;

import static ca.ualberta.cs.poker.Card.Card;

/**
 * Stores a Hand of Cards (up to a maximum of 7)
 *
 * @author Aaron Davidson
 */

public class Hand implements Comparable<Hand> {

    public final static int MAX_CARDS = 7;
    private final int[] cards;
    private int[] cardsSorted;

    public Hand() {
        cards = new int[MAX_CARDS + 1];
        cards[0] = 0;
    }

    public Hand(Card... cardList) {
        this();
        addCard(cardList);
    }

    /**
     * @param cs A string representing a Hand of cards
     */
    public Hand(String cs) {
        cards = new int[MAX_CARDS + 1];
        cards[0] = 0;
        StringTokenizer t = new StringTokenizer(cs, " -");
        while (t.hasMoreTokens()) {
            String s = t.nextToken();
            if (s.length() == 2) {
                Card c = Card(s.charAt(0), s.charAt(1));
                if (c.getIndex() != Card.BAD_CARD)
                    addCard(c);
            }
        }
    }

    /**
     * Duplicate an existing hand.
     *
     * @param h the hand to clone.
     */
    public Hand(Hand h) {
        cards = new int[MAX_CARDS + 1];
        cards[0] = h.size();
        System.arraycopy(h.cards, 1, cards, 1, cards[0]);
    }

    public void setCards(Hand h) {
        cards[0] = h.size();
        System.arraycopy(h.cards, 1, cards, 1, cards[0]);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(cards).toHashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof Hand)) {
            return false;
        }
        Hand lhs = this;
        Hand rhs = (Hand) obj;
        return new EqualsBuilder().append(lhs.getSortedCards(), rhs.getSortedCards()).isEquals();
    }

    @Override
    public int compareTo(Hand rhs) {
        Hand lhs = this;
        return new CompareToBuilder().append(lhs.getSortedCards(), rhs.getSortedCards()).toComparison();
    }

    /**
     * return true iff this hand contains any of the cards in h
     */
    public boolean blocks(Hand h) {
        for (int i = 0; i < h.size(); i++) {
            if (containsCard(h.getCard(i+1))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the size of the hand.
     *
     * @return the number of cards in the hand
     */
    public int size() {
        return cards[0];
    }

    /**
     * Remove the last card in the hand.
     */
    public void removeCard() {
        if (cards[0] > 0) {
            cards[0]--;
            cardsSorted = null;
        }
    }

    /**
     * Remove the all cards from the hand.
     */
    public void makeEmpty() {
        cards[0] = 0;
        cardsSorted = null;
    }

    /**
     * Add a card to the hand. (if there is room)
     *
     * @param cardsToAdd the card to add
     * @return true if the cards were added, false otherwise
     */
    public boolean addCard(Card... cardsToAdd) {

        if (cards[0] == MAX_CARDS || cards[0] + cardsToAdd.length > MAX_CARDS) {
            return false;
        }

        for (Card c : cardsToAdd) {
            if (c == null) {
                return false;
            }
        }

        for (Card c : cardsToAdd) {
            cards[0]++;
            cards[cards[0]] = c.getIndex();
        }

        cardsSorted = null;

        return true;
    }

    /**
     * Add a card to the hand. (if there is room)
     *
     * @param i the index value of the card to add
     * @return true if the card was added, false otherwise
     */
    public boolean addCard(int i) {
        if (cards[0] == MAX_CARDS) {
            return false;
        }
        cards[0]++;
        cards[cards[0]] = i;
        cardsSorted = null;
        return true;
    }

    /**
     * Get the a specified card in the hand
     *
     * @param pos the position (1..n) of the card in the hand
     * @return the card at position pos
     */
    public Card getCard(int pos) {
        if (pos < 1 || pos > cards[0]) {
            return null;
        }
        return Card(cards[pos]);
    }

    public Card[] getCards() {
        int sz = size();
        Card[] c = new Card[sz];
        for (int i = 0; i < sz; i++) {
            c[i] = Card(cards[i + 1]);
        }
        return c;
    }

    /**
     * Add a card to the hand. (if there is room)
     *
     * @param c the card to add
     */
    public void setCard(int pos, Card c) {
        if (cards[0] < pos) {
            return;
        }
        cards[pos] = c.getIndex();
        cardsSorted = null;
    }

    /**
     * Obtain the array of card indexes for this hand.
     * First element contains the size of the hand.
     *
     * @return array of card indexs (size = MAX_CARDS+1)
     */
    public int[] getCardArray() {
        return cards;
    }

    public boolean containsCard(Card c) {
        int ci = c.getIndex();
        for (int i = 1; i <= cards[0]; ++i) {
            if (cards[i] == ci) {
                return true;
            }
        }
        return false;
    }

    /**
     * Bubble Sort the hand to have cards in descending order, but card index.
     * Used for database indexing.
     */
    public void sort() {
        boolean flag = true;
        while (flag) {
            flag = false;
            for (int i = 1; i < cards[0]; i++) {
                if (cards[i] < cards[i + 1]) {
                    flag = true;
                    int t = cards[i];
                    cards[i] = cards[i + 1];
                    cards[i + 1] = t;
                }
            }
        }
    }

    public int[] getSortedCards() {
        if (cardsSorted == null) {
            cardsSorted = new int[cards[0]];
            System.arraycopy(cards, 1, cardsSorted, 0, cardsSorted.length);
            Arrays.sort(cardsSorted);
        }
        return cardsSorted;
    }

    /**
     * Get a string representation of this Hand.
     */
    public String toString() {
        String s = "";
        for (int i = 1; i <= cards[0]; i++) {
            s += " " + getCard(i).toString();
        }
        return s;
    }


    /**
     * Get the specified card id
     *
     * @param pos the position (1..n) of the card in the hand
     * @return the card at position pos
     */
    public int getCardIndex(int pos) {
        return cards[pos];
    }

}

