package ca.ualberta.cs.poker;

/***************************************************************************
Copyright (c) 2000:
      University of Alberta,
      Deptartment of Computing Science
      Computer Poker Research Group

    See "Liscence.txt"
***************************************************************************/

import java.util.Random;

import static ca.ualberta.cs.poker.Card.Card;

/**
*  A Deck of 52 Cards which can be dealt and shuffled
*  @author  Aaron Davidson
*/

public class Deck {
   public static final int NUM_CARDS_DEFAULT = 52;
    private int numCards;
   private Card[] gCards;
   private char position; // top of deck
   private Random r = new Random();
   
   /**
    * Constructor.
    */
   public Deck() {
       this(NUM_CARDS_DEFAULT);
   }

    public Deck(int numCards) {
        this.numCards = numCards;
        position = 0;
        gCards = new Card[numCards];
        for (int i=0;i< numCards;i++) {
            gCards[i] = Card(i);
        }
    }

   /*
    * Constructor w/ shuffle seed.
    * @param seed the seed to use in randomly shuffling the deck.
    */
   /*
   public Deck(long seed) {
      this();
      if (seed == 0) { 
         seed = System.currentTimeMillis();
      }
      r.setSeed(seed);
   } */
   
   public int getNumCards() {
       return numCards;
   }

   /**
    * Places all cards back into the deck.
    * Note: Does not sort the deck.
    */
   public synchronized void reset() { position = 0; }
     
   /**
    * Shuffles the cards in the deck.
    */
   public synchronized void shuffle() {
      Card  tempCard;
      int   i,j;
      for (i=0; i< numCards; i++) {
         j = i + randInt(numCards -i);
         tempCard = gCards[j];
         gCards[j] = gCards[i];
         gCards[i] = tempCard;
      }
      position = 0;
   }
   
   /**
    * Obtain the next card in the deck.
    * If no cards remain, a null card is returned
    * @return the card dealt
    */
   public synchronized Card deal() {
      return (position < numCards ? gCards[position++] : null);
   }
   
   /**
    * Obtain the next card in the deck.
    * If no cards remain, a null card is returned
    * @return the card dealt
    */
   public synchronized Card dealCard() {
      return extractRandomCard();
   }
   
   /**
    * Find position of Card in Deck.
    */
   public synchronized int findCard(Card c) {
      int i = position;
      int n = c.getIndex();
      while (i < numCards && n != gCards[i].getIndex())
         i++;
      return (i < numCards ? i : -1);
   }
   
   private synchronized int findDiscard(Card c) {
      int i = 0;
      int n = c.getIndex();
      while (i < position && n != gCards[i].getIndex())
         i++;  
      return (n == gCards[i].getIndex() ? i : -1);
   }
   
   /**
    * Remove all cards in the given hand from the Deck.
    */
   public synchronized void extractHand(Hand h) {
      for (int i=1;i<=h.size();i++)
         this.extractCard(h.getCard(i));
   }
   
   /**
    * Remove a card from within the deck.
    * @param c the card to remove.
    */
   public synchronized void extractCard(Card c) {
      int i = findCard(c);
      if (i != -1) {
         Card t = gCards[i];
         gCards[i] = gCards[position];
         gCards[position] = t;
         position++;
      } else {
         System.err.println("*** ERROR: could not find card " + c);
         Thread.currentThread().dumpStack();
      }
   }
   
   /**
    * Remove and return a randomly selected card from within the deck.
    */
   public synchronized Card extractRandomCard() {
      int pos = position+randInt(numCards -position);
      Card c = gCards[pos];
      gCards[pos] = gCards[position];
      gCards[position] = c;
      position++;
      return c;
   }
   
   /**
    * Return a randomly selected card from within the deck without removing it.  
    */
   public synchronized Card pickRandomCard() {
      return gCards[position+randInt(numCards -position)];
   }
   
   /**
    * Place a card back into the deck.
    * @param c the card to insert.
    */
   public synchronized void replaceCard(Card c) {
      int i = findDiscard(c);
      if (i != -1) {
         position--;
         Card t = gCards[i];
         gCards[i] = gCards[position];
         gCards[position] = t;
      }
   }
   
   /**
    * Obtain the position of the top card. 
    * (the number of cards dealt from the deck)
    * @return the top card index
    */
   public synchronized int getTopCardIndex() {
      return position;
   }
   
   
   /**
    * Obtain the number of cards left in the deck
    */
   public synchronized int cardsLeft() {
      return numCards -position;
   }
   
   /**
    * Obtain the card at a specific index in the deck.
    * Does not matter if card has been dealt or not.
    * If i < topCardIndex it has been dealt.
    * @param i the index into the deck (0..51)
    * @return the card at position i
    */
   public synchronized Card getCard(int i) {    
      return gCards[i];
   }

    /**
     * Get card i from the deck, removing it by setting to null at that position.
     * @return
     * @author JMT
     */
    public Card dealCard(int i) {
        Card c = gCards[i];
        gCards[i] = null;
        return c;
    }

    public void undealCard(int i, Card c) {
        gCards[i] = c;
    }

    public void deadenCards(Hand h) {
        for (int i = 1; i <= h.size(); i++) {
            int cardIndex = h.getCard(i).getIndex();
            dealCard(cardIndex);
        }
    }

   public String toString() {

       StringBuilder sb = new StringBuilder(numCards * 4);

       for (int i = 0; i < numCards; i++) {
           sb.append(gCards[i]);
           if ((i+1) % 13 == 0) {
               sb.append('\n');
           } else {
            sb.append(' ');
           }
       }
        return sb.toString();
   }
   
   private int randInt(int range) {
      return (int)(r.nextDouble()*range);
   }
    

}
