package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Card;
import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import ca.ualberta.cs.poker.HandEvaluator;
import org.apache.commons.lang.Validate;

import java.util.Arrays;

/**
 * Implementation of Loki's Effective Hand Strength calculation.
 *
 * EHS is a complex equity calculation (complex as in complex number, not complicated)
 * that factors in probabilities of improving from the worst to the best hand and
 * vice versa.
 *
 * Created by jtownson on 30/01/14.
 */
public class EhsEvaluator {

    static final int iAhead = 0;
    static final int iTied = 1;
    static final int iBehind = 2;

    private HandEnumerator handEnumerator = new HandEnumerator();
    private HandEvaluator handEvaluator = new HandEvaluator();

    /**
     * General range-based EHS calculation. This is a monte-carlo based
     * calculation that stops when it converges to the provided error.
     *
     * @param deck a deck
     * @param board flop or turn (or river) that defines dead cards
     * @param herosHand the distribution for which EHS will be calculated.
     * @param oppsHand the distribution against which EHS will be calculated.
     * @return an EHS wrapper containing ehs itself and various intermediate results.
     */

    public Ehs getEhs(Deck deck, Hand board, HandWeights herosHand, HandWeights oppsHand) {

        Validate.isTrue(board.size() == 0 || board.size() == 3 || board.size() == 4 || board.size() == 5);

        throw new UnsupportedOperationException();
    }

    /**
     * Straight implementation of EHS for a single hand, known with 100% certainty vs
     * another range of hands.
     * @param deck a deck
     * @param board flop or turn (or river) that defines dead cards
     * @param hand the hand for which EHS will be calculated.
     * @param oppHandWeights the distribution against which EHS will be calculated.
     * @return an EHS wrapper containing ehs itself and various intermediate results.
     */
    public Ehs getEhs(Deck deck, Hand hand, HandWeights oppHandWeights, Hand board) {

        double [][] hp = new double[3][3];
        double [] hpTotal = new double[3];

        if (board.size() == 3) {

            ehsOnFlop(deck, hand, oppHandWeights, board, hp, hpTotal);

        } else if (board.size() == 4) { // TODO ...and then on the turn
            throw new UnsupportedOperationException("boo");
        }

        //hs: initial hand strength
        double hs = getHandStrength(hpTotal);

        //Ppot: were behind but moved ahead.
        double pPot = getPositivePotential(hp, hpTotal);

        //Npot: were ahead but fell behind.
        double nPot = getNegativePotential(hp, hpTotal);

        double ehsAggressive = hs + (1 - hs) * pPot;
        double ehsPassive = hs + (1 - hs) * pPot - hs * nPot;

        return new Ehs(hs, nPot, pPot, ehsAggressive, ehsPassive, hp, hpTotal);
    }

    public Ehs getEhs(Deck deck, Hand hand, Hand board) {
        return getEhs(deck, hand, new HandWeights(deck), board);
    }

    private double getHandStrength(double[] hpTotal) {
        double ahead = hpTotal[iAhead];
        double tied = hpTotal[iTied];
        double behind = hpTotal[iBehind];
        return (ahead+tied/2.0)/(ahead+tied+behind);
    }

    private double getNegativePotential(double[][] hp, double[] hpTotal) {

        if (hpTotal[iAhead] == 0 && hpTotal[iTied] == 0) { // have the utter worst
            return 0.0;
        } else {
            return (hp[iAhead][iBehind] + hp[iTied][iBehind]/2 + hp[iAhead][iTied]/2)
                    /
                    (hpTotal[iAhead] + hpTotal[iTied]);
        }
    }

    private double getPositivePotential(double[][] hp, double[] hpTotal) {

        if (hpTotal[iBehind] == 0 && hpTotal[iTied] == 0) { // have the absolute nuts
            return 0.0;
        } else {
            return (hp[iBehind][iAhead] + hp[iBehind][iTied]/2 + hp[iTied][iAhead]/2)
                    /
                    (hpTotal[iBehind] + hpTotal[iTied]);
        }
    }

    private void ehsOnFlop(Deck deck, Hand hand, HandWeights oppHandWeights, Hand flop, double[][] hp, double[] hpTotal) {

        Card[] deadCards = new Card[5];
        deadCards[0] = hand.getCard(1);
        deadCards[1] = hand.getCard(2);
        deadCards[2] = flop.getCard(1);
        deadCards[3] = flop.getCard(2);
        deadCards[4] = flop.getCard(3);

        Card[] deadWorking = new Card[4];

        int ourStartingRank = handEvaluator.rankHand(hand.getCard(1), hand.getCard(2), flop);

        handEnumerator.exEnumerate2CardHands(deck, deadCards,
                (Hand oppHand) -> {

                    deadWorking[0] = hand.getCard(1);
                    deadWorking[1] = hand.getCard(2);
                    deadWorking[2] = oppHand.getCard(1);
                    deadWorking[3] = oppHand.getCard(2);

                    int oppStartingRank = handEvaluator.rankHand(oppHand.getCard(1), oppHand.getCard(2), flop);

                    handEnumerator.exEumerateRivers(deck, deadWorking, new Hand(flop),
                            (Hand simBoard) -> {

                                // TODO this is being reduntantly evaluated too many times.
                                int ourFinalRank = handEvaluator.rankHand(hand.getCard(1), hand.getCard(2), simBoard);
                                int oppFinalRank = handEvaluator.rankHand(oppHand.getCard(1), oppHand.getCard(2), simBoard);

                                int initialIndex = indexByRankComparison(ourStartingRank, oppStartingRank);
                                int finalIndex = indexByRankComparison(ourFinalRank, oppFinalRank);

                                double weight = oppHandWeights.weightFor(oppHand.getCard(1), oppHand.getCard(2));

                                hp[initialIndex][finalIndex] += weight;

                                hpTotal[initialIndex] += weight;

                            });
                });
    }

    private int indexByRankComparison(int ourRank, int oppRank) {
        if (ourRank > oppRank) {
            return iAhead;
        } else if (ourRank == oppRank) {
            return iTied;
        }
        return iBehind;
    }
}
