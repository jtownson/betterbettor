package net.jtownson.poker.oddsandequity;

/**
 * Created by jtownson on 07/01/14.
 */
class IntegerRange {

    private final boolean leftInclusive;
    private final boolean rightInclusive;
    private final int min;
    private final int max;

    static IntegerRange inclusive(int min, int max) {
        return new IntegerRange(true, true, min, max);
    }

    static IntegerRange exclusive(int min, int max) {
        return new IntegerRange(min, max);
    }

    IntegerRange(int min, int max) {
        this(false, false, min, max);
    }

    IntegerRange(boolean leftInclusive, boolean rightInclusive, int min, int max) {
        this.leftInclusive = leftInclusive;
        this.rightInclusive = rightInclusive;
        this.min = min;
        this.max = max;
    }

    boolean contains(int i) {
        boolean contains = true;
        if (leftInclusive) {
            contains = contains && min <= i;
        } else {
            contains = contains && min < i;
        }

        if (rightInclusive) {
            contains = contains && i <= max;
        } else {
            contains = contains && i < max;
        }
        return contains;
    }
}
