/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/

package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Card;
import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.mutable.MutableDouble;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.util.List;
import java.util.TreeMap;

import static ca.ualberta.cs.poker.Card.Card;

/**
 * Implementation of a weighted hand distribution.
 * You can use this class like a traditional hand range
 * (such as ATs+) where all hands in the range are equally
 * likely and hands outside the range have zero probability,
 * or you can assign more general weights to specific hands.
 *
 * The use of the <code>sample</code> method will then deal
 * sequences of hands with a distribution in proportion to
 * those weights.
 *
 * Created by jtownson on 08/01/14.
 */
public class HandWeights {

    private Deck deck;
    private Card[] deadCards;
    private double deltaP;
    private double normalization;
    private double[][] weightMatrix;
    private TreeMap<Double, Hand> handSelector;
    private RandomDataGenerator generator = new RandomDataGenerator();

    /**
     * Create a hand matrix over the standard 52 card deck.
     */
    public HandWeights() {
        this(new Deck());
    }

    /**
     * Create a hand matrix over a custom sized deck.
     * (useful for testing with small decks).
     * @param deck a deck
     */
    public HandWeights(Deck deck) {
        this(deck, true);
    }

    /**
     * A factory method to create a weight matrix from a known set of hands
     */
    public static HandWeights handBasedWeights(List<Hand>  hands) {

        return handBasedWeights(new Deck(), hands);
    }

    /**
     * A factory method to create a weight matrix from a known set of hands with a custom deck
     */
    public static HandWeights handBasedWeights(Deck deck, List<Hand> hands) {

        HandWeights matrix = new HandWeights(deck, false);

        initializeFrom(matrix, hands);

        return matrix;
    }

    /**
     * A factory method to create a weight matrix from a hand range.
     * Hands in the range will have a weight of 1 and hands outside
     * will have a weight of zero.
     * @param handRange a string representation of the range (e.g. AJs+, AQo+)
     * @return a set of weights where all hands in the range are equially likely
     */
    public static HandWeights rangeBasedHandWeights(String handRange) {
        return rangeBasedHandWeightsForDeck(new Deck(), handRange);
    }

    /**
     * A factory method to create a weight matrix from a hand range using a custom sized deck.
     * Hands in the range will have a weight of 1 and hands outside
     * will have a weight of zero.
     * @param handRange a string representation of the range (e.g. AJs+, AQo+)
     * @return As for rangeBasedHandWeights
     */
    public static HandWeights rangeBasedHandWeightsForDeck(Deck deck, String handRange) {

        RangeNotationParser notationParser = new RangeNotationParser();

        List<Hand> handsInRange = notationParser.parse(handRange);

        HandWeights matrix = new HandWeights(deck, false);

        initializeFrom(matrix, handsInRange);

        matrix.normalizeTo(1.0);

        return matrix;
    }

    /**
     * A factory method to get an initial set of starting hand weights,
     * with weights calculated using expected all-in equitities against
     * the given number of oppoenents.
     * @param numOpponents a number from 1 to 8
     * @return the weight matrixs
     */
    public static HandWeights startingEquityHandWeights(int numOpponents) {

        HandWeights weightMatrix = new HandWeights(new Deck(), false);

        initializeFrom(weightMatrix, numOpponents);

        return weightMatrix;
    }

    /**
     * Get the weight associated with a 2 card hand.
     * @param hand the hand. Must be of two cards.
     * @return the associated weight.
     */
    public double weightFor(Hand hand) {
        Validate.isTrue(hand.size() == 2);
        return weightFor(hand.getCard(1), hand.getCard(2));
    }

    public void setWeightFor(Hand hand, double value) {
        Validate.isTrue(hand.size() == 2);
        setWeightFor(hand.getCard(1), hand.getCard(2), value);
    }

    /**
     * Get the weight associated with the hand containing card1 and card2
     * @param card1 first card in hand
     * @param card2 second card in hand
     * @return the associated weight
     */
    public double weightFor(Card card1, Card card2) {
        int i = card1.getIndex();
        int j = card2.getIndex();
        return weightFor(i, j);
    }

    public void setWeightFor(Card card1, Card card2, double value) {
        int i = card1.getIndex();
        int j = card2.getIndex();
        setWeightFor(i, j, value);
    }

    /**
     * As for weightFor but taking card indices.
     * @param cardIndex1 first card index
     * @param cardIndex2 second card index
     * @return the associated weight
     */
    public double weightFor(int cardIndex1, int cardIndex2) {
        if (cardIndex2 > cardIndex1) {
            return weightMatrix[cardIndex1][cardIndex2];
        } else if (cardIndex1 > cardIndex2) {
            return weightMatrix[cardIndex2][cardIndex1];
        } else {
            throw new IllegalStateException(
                    "Cannot get hand weight. Hand contains two identical cards: " + Card(cardIndex1));
        }
    }

    /**
     * Normalize all weights to the given value.
     *
     * @param value A value &ge; 1. If set to 1.0 then weights will represent hand probabilities,
     *              however, normalization to larger numbers, such as N, the number of hands
     *              (thus making the average weight = 1) will produce more accurate intermediate
     *              calculations.
     */
    public void normalizeTo(double value) {

        Validate.isTrue(value >= 1.0);

        double sum = sumOfWeights();

        double p = foreachWeight(
                (i, j, d) ->
                {
                    weightMatrix[i][j] = weightMatrix[i][j] * value / sum;
                    return d + weightMatrix[i][j];
                });

        normalization = value;
        setDelta(value - p);
    }

    public double getNormalization() {
        return normalization;
    }

    public double getDeltaP() {
        return deltaP;
    }

    @Override
    public String toString() {
        int deckSize = numCards();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < deckSize; i++) {
            sb.append("  ").append(Card(i)).append("  ");
        }
        sb.append('\n');
        for (int i = 0; i < deckSize; i++) {
            for (int j = 0; j < deckSize; j++) {
                if (j < i+1) {
                    sb.append("  0   ");
                } else {
                    sb.append(String.format("%.3f ", weightMatrix[i][j]));
                }
            }
            sb.append(Card(i)).append('\n');
        }
        return sb.toString();
    }

    /**
     * Eliminate cards on the board as possible starting hands (i.e. set their weights to zero).
     * Redistribute those eliminated probabilities among the other weights (in proportion to their
     * value).
     * @param board a hand containing dead cards
     */
    public void setDeadCards(Hand board) {

        setDeadCards(board.getCards());
    }

    /**
     * Eliminate cards on the board as possible starting hands (i.e. set their weights to zero).
     * Redistribute those eliminated probabilities among the other weights (in proportion to their
     * value).
     * @param cards some cards
     */
    public void setDeadCards(Card[] cards) {
        // first zero the probabilities of all hands that
        // the board is blocking, taking stock of this
        // discarded probability so we can re-distribute it
        // throughout the remaining hands.
        zeroWeightAttributedTo(cards);

        // next through the matrix, redistributing
        // the blocked probabilities so the matrix
        // still sums to 1 overall.
        normalizeTo(normalization);

        setDeadCardArray(cards);
    }

    public int getNumHands() {
        int deckSize = numCards();
        return deckSize * (deckSize - 1) / 2;
    }

    public double sumOfWeights() {
        return foreachWeight((i, j, d) -> d + weightMatrix[i][j]);
    }

    /**
     * Subsequent calls to sample will return a sequence of hands in a distribution
     * that corresponds to their weights.
     * Thus, hands with weight=0.0 will never get returned, hands with high weight
     * values are more probable and everything in between.
     * @return a hand, selected according to the hand weights
     */
    public Hand sample() {

        // random in the range (0, 1] scaled to normalization factor.
        double r = generator.nextUniform(0, normalization, true/*lower inclusive*/);
        double d = normalization - r;

        return getHandSelector().lowerEntry(d).getValue();
    }

    private TreeMap<Double, Hand> getHandSelector() {
        if (handSelector == null) {

            handSelector = new TreeMap<>();

            MutableDouble count = new MutableDouble(0.0);

            foreachWeight((i,j,d) ->{
                double w = weightMatrix[i][j];
                if (w != 0) {
                    handSelector.put(count.doubleValue(), new Hand(Card(i), Card(j)));
                    count.add(w);
                }
                return 0;
            });
        }
        return handSelector;
    }

    private void setWeightFor(int cardIndex1, int cardIndex2, double value) {
        if (cardIndex2 > cardIndex1) {
            weightMatrix[cardIndex1][cardIndex2] = value;
        } else if (cardIndex1 > cardIndex2) {
            weightMatrix[cardIndex2][cardIndex1] = value;
        } else {
            throw new IllegalStateException(
                    "Cannot get hand weight. Hand contains two identical cards: " + Card(cardIndex1));
        }
        handSelector = null;
    }

    private void setDeadCardArray(Card[] cards) {

        if (deadCards.length != cards.length) {
            deadCards = new Card[cards.length];
        }
        System.arraycopy(cards, 0, deadCards, 0, cards.length);
        handSelector = null;
    }

    private double zeroWeightAttributedTo(Card[] cardIndices) {

        int deckSize = numCards();
        double pb = 0;
        for (Card cardIndice : cardIndices) {

            int cardIndex = cardIndice.getIndex();

            for (int i = 0; i < cardIndex; i++) {
                pb += weightMatrix[i][cardIndex];
                weightMatrix[i][cardIndex] = 0;
            }

            for (int j = cardIndex + 1; j < deckSize; j++) {
                pb += weightMatrix[cardIndex][j];
                weightMatrix[cardIndex][j] = 0;
            }
        }
        return pb;
    }

    private void setDelta(double p) {
        deltaP = p;
    }

    private int numCards() {
        return deck.getNumCards();
    }

    private void setAllWeightsTo(double value) {
        foreachWeight((i, j, d) -> weightMatrix[i][j] = value);
    }

    @FunctionalInterface
    public interface WeightHandler {
        double call(int i, int j, double d);
    }

    private double foreachWeight(WeightHandler h) {
        double d = 0;
        int deckSize = numCards();
        for (int i = 0; i < deckSize; i++) {
            for (int j = i+1; j < deckSize; j++) {
                d = h.call(i, j, d);
            }
        }
        return d;
    }

    private static void initializeFrom(HandWeights matrix, List<Hand> handsInRange) {

        Deck deck = matrix.deck;
        double[][] weights = matrix.weightMatrix;

        for (Hand hand : handsInRange) {

            int c1 = hand.getCard(1).getIndex();
            int c2 = hand.getCard(2).getIndex();

            if (c1 > deck.getNumCards() || c2 > deck.getNumCards()) {
                throw new IllegalArgumentException("Hand " + hand + " is not part of the current deck: " + deck);
            }
            if (c1 > c2) {
                weights[c2][c1] = 1;
            } else if (c2 > c1) {
                weights[c1][c2] = 1;
            } else {
                throw new IllegalStateException("Got a hand with two identical cards: " + hand);
            }
        }
        matrix.normalization = matrix.sumOfWeights();
    }

    private static void initializeFrom(HandWeights weightMatrix, int numOpponents) {

        double[][] w = weightMatrix.weightMatrix;

        RangeNotationParser rangeNotationParser = new RangeNotationParser();
        List<HandEquity> equities = new AllInEquities().getEquitiesForNumOpponents(numOpponents);

        for (HandEquity handEquity : equities) {

            List<Hand> hands = rangeNotationParser.parse(handEquity.getStartingHand());

            for (Hand hand : hands) {

                int h1 = hand.getCard(1).getIndex();
                int h2 = hand.getCard(2).getIndex();

                double equity = handEquity.getEquity();

                if (h1 > h2) {
                    w[h2][h1] = equity;
                } else {
                    w[h1][h2] = equity;
                }
            }
        }
        weightMatrix.normalization = weightMatrix.sumOfWeights();
    }

    private HandWeights(Deck deck, boolean initialize) {
        this.deck = deck;
        this.weightMatrix = new double[numCards()][numCards()];
        this.deadCards = new Card[0];
        this.deltaP = 0.0;
        this.normalization = getNumHands();
        if (initialize) {
            setAllWeightsTo(1.0);
        }
    }

}
