/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Card;
import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.math3.exception.NotStrictlyPositiveException;
import org.apache.commons.math3.exception.NumberIsTooLargeException;
import org.apache.commons.math3.exception.util.LocalizedFormats;
import org.apache.commons.math3.random.RandomDataGenerator;
import org.apache.commons.math3.util.Combinations;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * Routines to enumerate hands using monte-carlo or exhaustive
 * enumeration and invoke callback functions.
 *
 * Created by jtownson on 28/01/14.
 */
public class HandEnumerator {

    private RandomDataGenerator generator = new RandomDataGenerator();

    /**
     * Enumerate all 2 card hands that could be dealt, taking account of dead
     * cards.
     * @param deck a deck
     * @param deadCards some dead cards
     * @param callback some code to accept a 2 card hand and do some work on it
     */
    public void exEnumerate2CardHands(Deck deck, Card[] deadCards, Consumer<Hand> callback) {
        exEnumerateCards(deck, deadCards, 2, callback);
    }

    public void mcEnumerate2CardHands(Deck deck, Card[] deadCards, Predicate<Hand> predicate) {
        mcEnumerateCards(deck, deadCards, 2, predicate);
    }

    /**
     * Enumerate all 3-board combinations (i.e. from pre-flop to the flop),
     * taking account of known dead cards in players hands.
     * @param deck Geneally you want <code>new Deck()</code>, however, the param
     *             is used to determine how many cards are in the deck so
     *             <code>new Deck(5)</code> for example allows you to use a small
     *             deck for testing.
     * @param deadCards Those cards known to be in players' hands.
     * @param callback caller code to handle the visited 3 card combinations. The generated
     *                 board will be passed as an argument.
     */
    public void exEnumerateFlops(Deck deck, Card[] deadCards, Consumer<Hand> callback) {
        exEnumerateCards(deck, deadCards, 3, callback);
    }

    /**
     * Enumerate all 5-card board combinations, given that the flop has already been dealt.
     * This effectively adds the flop to dead cards, however, the callback will be invoked
     * with the flop + generated turn and river.
     * @param deck a deck.
     * @param deadCards known cards in players hands.
     * @param flop the flop
     * @param callback a callback that takes a 5-card hand representing the generated board.
     */
    public void exEumerateRivers(Deck deck, Card[] deadCards, Hand flop, Consumer<Hand> callback) {

        Validate.isTrue(flop.size() == 3);

        Card[] effectivelyDead = (Card[])ArrayUtils.addAll(
                deadCards, new Card[]{flop.getCard(1), flop.getCard(2), flop.getCard(3)});

        exEnumerateCards(deck, effectivelyDead, 2,
                (Hand turnRiver) ->
                {
                    flop.addCard(turnRiver.getCard(1));
                    flop.addCard(turnRiver.getCard(2));
                    try {
                        callback.accept(flop);
                    } finally {
                        flop.removeCard();
                        flop.removeCard();
                    }
                });
    }

    public void mcEnumerateRivers(Deck deck, Card[] deadCards, Hand flop, Predicate<Hand> predicate) {

        Validate.isTrue(flop.size() == 3);

        Card[] effectivelyDead = (Card[])ArrayUtils.addAll(
                deadCards, new Card[]{flop.getCard(1), flop.getCard(2), flop.getCard(3)});

        final Hand workingHand = new Hand(flop);

        mcEnumerateCards(deck, effectivelyDead, 2,
                turnRiver ->
                {
                    workingHand.addCard(turnRiver.getCard(1));
                    workingHand.addCard(turnRiver.getCard(2));
                    try {
                        return predicate.test(workingHand);
                    } finally {
                        workingHand.removeCard();
                        workingHand.removeCard();
                    }
                });
    }

    /**
     * Enumerate all 5-card board combinations, given the turn has already been dealt.
     * This effectively makes the flop and turn dead, however, the callback will be
     * invoked with the flop+turn + generated river cards.
     */
    public void exEnumerateRivers(Deck deck, Card[] deadCards, Card[] flop, Card turn, Consumer<Hand> callback) {

        Validate.isTrue(flop.length == 3);

        Card[] effectivelyDead = (Card[])ArrayUtils.addAll(deadCards, flop);
        effectivelyDead = (Card[])ArrayUtils.add(effectivelyDead, turn);

        Hand h = new Hand(flop[0], flop[1], flop[2], turn);

        exEnumerateCards(deck, effectivelyDead, 1,
                (Hand river) ->
                {
                    h.addCard(river.getCard(1));
                    try {
                        callback.accept(h);
                    } finally {
                        h.removeCard();
                    }
                });
    }

    public void mcEnumerateRivers(Deck deck, Card[] deadCards, Card[] flop, Card turn, Predicate<Hand> predicate) {

        Validate.isTrue(flop.length == 3);

        Card[] effectivelyDead = (Card[]) ArrayUtils.addAll(deadCards, flop);
        effectivelyDead = (Card[])ArrayUtils.add(effectivelyDead, turn);

        Hand h = new Hand(flop[0], flop[1], flop[2], turn);

        mcEnumerateCards(deck, effectivelyDead, 1,
                river -> {
                    h.addCard(river.getCard(1));
                    try {
                        return predicate.test(h);
                    } finally {
                        h.removeCard();
                    }
                }
        );
    }

    /**
     * Enumerate all 5-board combinations to the river, taking account of
     * dead hands.
     * @param deck a deck. See previous comments about small decks for testing.
     * @param deadCards Those cards known to be in players' hands.
     * @param callback caller code to handle the visited combinations. The generated
     *                 board will be passed as an argument.
     */
    public void exEnumerate5CardBoards(Deck deck, Card[] deadCards, Consumer<Hand> callback) {
        exEnumerateCards(deck, deadCards, 5, callback);
    }

    public void mcEnumerate5CardBoards(Deck deck, Card[] deadCards, Predicate<Hand> predicate) {
        mcEnumerateCards(deck, deadCards, 5, predicate);
    }

    /**
     * Enumerate boards of size k, taking account of dead cards.
     */
    public void exEnumerateCards(Deck deck, Card[] deadCards, int k, Consumer<Hand> callback) {

        Arrays.sort(deadCards);

        int n = deck.getNumCards() - deadCards.length;

        // we always generate k-size subsets of ints in 0..n-1
        // and then map these ints to card indexes in the deck
        // (only equal if no dead cards)
        int[] deadMap = getDeadMap(deadCards, n);

        exEnumerate(n, k, (int[] iArr) -> {
            // iArr has 5 cards from 0..n
            // which we need to map onto 0..52
            Hand hand = new Hand();

            for (int iDead : iArr) {
                int iFull = deadMap[iDead];
                hand.addCard(iFull);
            }

            callback.accept(hand);
        });
    }

    public void mcEnumerateCards(Deck deck, Card[] deadCards, int k, Predicate<Hand> predicate) {

        Arrays.sort(deadCards);

        int n = deck.getNumCards() - deadCards.length;

        int[] deadMap = getDeadMap(deadCards, n);

        int[] subset = new int[k];

        mcEnumerate(n, k, subset,
                iArr -> {
                    Hand hand = new Hand();
                    for (int i = 0; i < k; i++) {
                        int iDead = iArr[i];
                        int iFull = deadMap[iDead];
                        hand.addCard(iFull);
                    }
                    return predicate.test(hand);
                });
    }

    /**
     * Iterate through all combinations of C(n,k) calling callback
     * with an array of k ints repsenting the 0, 1, ..., k-1 elements
     * in the combination.
     * @param n the n in C(n,k)
     * @param k the k in C(n,k)
     * @param callback a fun that does some work with the combination.
     */
    public void exEnumerate(int n, int k, Consumer<int[]> callback) {
        Combinations c = new Combinations(n, k);
        for (int[] iArr : c) {
            callback.accept(iArr);
        }
    }

    /**
     * Randomly select k sized subsets of integers in (0..n-1).
     * Do this nTrials times.
     * @param n The n in C(n,k)
     * @param k The k in C(n,k)
     * @param predicate a fun that does some work with the combination and returns true iff more combinations are required.
     * @param subset a k-size array for holding randomly generated subsets of ints in the range 0..n-1
     */
    public void mcEnumerate(int n, int k, int[] subset, Predicate<int[]> predicate) {
        boolean morePerms = true;
        while (morePerms) {
            nextPermutation(n, k, subset);
            morePerms = predicate.test(subset);
        }
    }

    public void nextPermutation(int n, int k, int[] result)
            throws NumberIsTooLargeException, NotStrictlyPositiveException {
        if (k > n) {
            throw new NumberIsTooLargeException(LocalizedFormats.PERMUTATION_EXCEEDS_N, k, n, true);
        }
        if (k <= 0) {
            throw new NotStrictlyPositiveException(LocalizedFormats.PERMUTATION_SIZE, k);
        }

        int[] index = getNatural(n);
        shuffle(index, n - k);
        for (int i = 0; i < k; i++) {
            result[i] = index[n - i - 1];
        }
    }

    // essentially a left-shifting operation
    private int[] getDeadMap(Card[] deadCards, int n) {

        int[] deadMap = new int[n];

        int lag = 0;
        for (int i48 = 0, i52 = 0; i48 < n; i48++, i52++) {
            while (lag < deadCards.length && i52 >= deadCards[lag].getIndex()) {
                lag++;
                i52++;
            }
            deadMap[i48] = i52;
        }
        return deadMap;
    }

    private void shuffle(int[] list, int end) {
        int target;
        for (int i = list.length - 1; i >= end; i--) {
            if (i == 0) {
                target = 0;
            } else {
                // NumberIsTooLargeException cannot occur
                target = generator.nextInt(0, i);
            }
            int temp = list[target];
            list[target] = list[i];
            list[i] = temp;
        }
    }

    private int[] getNatural(int n) {
        int[] natural = new int[n];
        for (int i = 0; i < n; i++) {
            natural[i] = i;
        }
        return natural;
    }
}
