/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Hand;
import ca.ualberta.cs.poker.HandEvaluator;
import org.apache.commons.lang.builder.EqualsBuilder;

import java.util.HashSet;
import java.util.Set;

/**
 * Represents a class of equally ranked hands, such as "Three of a Kind, Fours".
 */
public class HandCategory implements Comparable<HandCategory> {

    private final Set<Hand> contributingHands = new HashSet<>();
    private final String handDescription;
    double weight;
    double equity;

    HandCategory(Hand initialHand, int rank) {
        this.handDescription = HandEvaluator.nameHand(rank);
        contributingHands.add(initialHand);
    }

    public int getHandCount() {
        return contributingHands.size();
    }

    public boolean containsHand(Hand h) {
        return contributingHands.contains(h);
    }

    public void addHand(Hand h) {
        contributingHands.add(h);
    }

    public String getHandDescription() {
        return handDescription;
    }

    public double getWeight() {
        return weight;
    }

    public double getEquity() {
        return equity;
    }

    @Override
    public int hashCode() {
        return handDescription.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || ! (obj instanceof HandCategory)) {
            return false;
        }
        HandCategory rhs = (HandCategory)obj;

        return new EqualsBuilder().append(this.handDescription, rhs.handDescription).isEquals();
    }

    @Override
    public String toString() {
        return String.format(
                "%s: %s hands, weight = %.2f, equity = %.2f",
                handDescription, contributingHands.size(), weight, equity);
    }

    @Override
    public int compareTo(HandCategory rhs) {
        return this.handDescription.compareTo(rhs.handDescription);
    }
}
