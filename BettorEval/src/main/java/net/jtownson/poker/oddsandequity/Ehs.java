/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

/**
 * Created by jtownson on 30/01/14.
 */
public class Ehs {

    public final double hs;
    public final double pPot;
    public final double nPot;
    public final double ehsAggressive;
    public final double ehsPassive;
    double[][] hpd;
    double[] hpdTotal;

    public Ehs(double hs, double nPot, double pPot, double ehsAggressive, double ehsPassive, double[][] hpd, double[] hpdTotal) {
        this.hs = hs;
        this.pPot = pPot;
        this.nPot = nPot;
        this.ehsAggressive = ehsAggressive;
        this.ehsPassive = ehsPassive;
        this.hpd = hpd;
        this.hpdTotal = hpdTotal;
    }
}
