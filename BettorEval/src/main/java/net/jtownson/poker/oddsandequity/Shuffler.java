package net.jtownson.poker.oddsandequity;

import org.apache.commons.math3.random.RandomDataGenerator;

/**
 * Little class to randomly jiggle the elements of an array.
 * Created by jtownson on 03/02/14.
 */
public class Shuffler {

    private static RandomDataGenerator generator = new RandomDataGenerator();

    public static void shuffle(int[] list) {
        int end = 0;
        int target;
        for (int i = list.length - 1; i >= end; i--) {
            if (i == 0) {
                target = 0;
            } else {
                // NumberIsTooLargeException cannot occur
                target = generator.nextInt(0, i);
            }
            int temp = list[target];
            list[target] = list[i];
            list[i] = temp;
        }
    }


}
