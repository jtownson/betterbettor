/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Hand;
import org.apache.commons.lang.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class RangeNotationParser {

    private static final String twoCards = "([23456789TJQKA])([23456789TJQKA])";
    private static final String runningToken = "\\s?-\\s?";
    private static final String suitedToken = "([so]?)";
    private static final String plusToken = "(\\+?)";
    private static final String twoCardsWithSuits = "([23456789TJQKA][hcds])([23456789TJQKA][hcds])";

    private static final Pattern dashPattern = Pattern.compile(twoCards + runningToken + twoCards + suitedToken);
    private static final Pattern runningPattern = Pattern.compile(twoCards + suitedToken + plusToken);
    private static final Pattern suitedCardPattern = Pattern.compile(twoCardsWithSuits);

    private static final String [] suits = {"h", "c", "d", "s"};
    private static final String [] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"};


    public List<Hand> parse(String hand) {

        String [] handRanges = hand.trim().split("\\s?,\\s?");

        Set<Hand> handSet = new HashSet<>();

        for (String handRange : handRanges) {

            parseSingleRange(handRange, handSet);
        }

        return new ArrayList<>(new TreeSet<>(handSet));
    }

    private void parseSingleRange(String handRange, Set<Hand> handSet) {

        Matcher dashMatcher = dashPattern.matcher(handRange);
        Matcher runningMatcher = runningPattern.matcher(handRange);
        Matcher suitedCardMatcher = suitedCardPattern.matcher(handRange);

        if (dashMatcher.matches()) {

            String card11 = dashMatcher.group(1);
            String card12 = dashMatcher.group(2);

            String card21 = dashMatcher.group(3);
            String card22 = dashMatcher.group(4);

            String suitedness = dashMatcher.group(5);
            if (StringUtils.isBlank(suitedness)) {
                suitedness = "o";
            }

            if (card11.equals(card12) && card21.equals(card22)) {

                evaluateRangedPair(card11, card21, handSet);

            } else if ( ! card11.equals(card21)) {
                throw new IllegalArgumentException(
                        "Parse error for " + handRange +
                        ". Hyphenated ranges need to have a common first card. " +
                        "For example, AT-AKo will work, but KT-AKo will not.");

            } else {
                evaluateDashPattern(card11, card12, card22, suitedness, handSet);
            }

        } else if (runningMatcher.matches()) {

            String card1 = runningMatcher.group(1);
            String card2 = runningMatcher.group(2);

            String suitedness = runningMatcher.group(3);

            String plus = runningMatcher.group(4);

            evaluateRunningPattern(card1, card2, suitedness, plus, handSet);

        } else if (suitedCardMatcher.matches()) {

            String card1 = suitedCardMatcher.group(1);
            String card2 = suitedCardMatcher.group(2);

            handSet.add(new Hand(card1 + ' ' + card2));

        } else {
            throw new IllegalArgumentException(
                    "Invalid syntax in hand expression: " + handRange);
        }
    }

    // AK-ATo
    private void evaluateDashPattern(
            String leadingCard, String card12, String card22, String suitedness, Set<Hand> handSet) {

        if ("s".equals(suitedness)) {

            int ic1 = firstIndexOf(card12, ranks);
            int ic2 = firstIndexOf(card22, ranks);

            for (int ic = ic1; ic < ic2+1; ++ic) {

                String nextCard = ranks[ic];

                if ( ! nextCard.equals(leadingCard)) {
                    for(String suit : suits) {

                        handSet.add(new Hand(leadingCard + suit + " " + nextCard + suit));
                    }
                }
            }
        } else {

            int ic1 = firstIndexOf(card12, ranks);
            int ic2 = firstIndexOf(card22, ranks);

            for (int ic = ic1; ic < ic2+1; ++ic) {

                String nextCard = ranks[ic];

                if ( ! nextCard.equals(leadingCard)) {

                    for(String suit1 : suits) {

                        for(String suit2: suits) {

                            if ( ! suit1.equals(suit2)) {

                                handSet.add(new Hand(leadingCard + suit1 + " " + nextCard + suit2));
                            }
                        }
                    }
                }
            }
        }
    }

    // A2s+ or 77+
    private void evaluateRunningPattern(
            String card1, String card2, String suitedness, String plus, Set<Hand> handSet) {


        if (card1.equals(card2)) {
            if ("s".equals(suitedness)) {
                throw new IllegalArgumentException(
                        String.format(
                        "Illegal range syntax for %s%s%s. Pairs cannot be suited.", card1, card2, suitedness));
            }
            evaluatePair(card1, plus, handSet);
        } else {
            evaluateGapConnector(card1, card2, suitedness, plus, handSet);
        }
    }

    private void evaluatePair(String card, String plus, Set<Hand> handSet) {

        if ("+".equals(plus)) {

            evaluateRangedPair(card, "A", handSet);

        } else {

            evaluateRangedPair(card, card, handSet);
        }
    }

    private static <T> int firstIndexOf(T item, T[] arr) {

        for (int i = 0; i < arr.length; ++i) {
            if (item.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    // A2s+ or 9To+
    private void evaluateGapConnector(String card1, String card2, String suitedness, String plus, Set<Hand> handSet) {

        if ("+".equals(plus)) {

            if ("s".equals(suitedness)) {

                evaluateSuitedGapConnectorRange(card1, card2, handSet);

            } else {

                evaluateUnsuitedGapConnectorRange(card1, card2, handSet);
            }
        } else {

            if ("s".equals(suitedness)) {
                evaluateSuitedGapConnector(card1, card2, handSet);
            } else {

                evaluateUnsuitedGapConnector(card1, card2, handSet);
            }
        }
    }

    private void evaluateUnsuitedGapConnector(String card1, String card2, Set<Hand> handSet) {
        for (String suit1 : suits) {

            for (String suit2 : suits) {

                if ( ! suit1.equals(suit2)) {
                    handSet.add(new Hand(card1 + suit1 + " " + card2 + suit2));
                }
            }
        }
    }

    private void evaluateSuitedGapConnector(String card1, String card2, Set<Hand> handSet) {
        for (String suit : suits) {
            handSet.add(new Hand(card1 + suit + " " + card2 + suit));
        }
    }

    private void evaluateUnsuitedGapConnectorRange(String card1, String card2, Set<Hand> handSet) {

        int i1 = firstIndexOf(card2, ranks);
        int i2 = firstIndexOf("A", ranks);

        for (int iRank = i1; iRank < i2+1; ++iRank) {

            String nextCard = ranks[iRank];

            if ( ! card1.equals(nextCard)) {

                evaluateUnsuitedGapConnector(card1, nextCard, handSet);
            }
        }
    }

    private void evaluateSuitedGapConnectorRange(String card1, String card2, Set<Hand> handSet) {

        int i1 = firstIndexOf(card2, ranks);
        int i2 = firstIndexOf("A", ranks);

        for (int iRank = i1; iRank < i2+1; ++iRank) {

            String nextCard = ranks[iRank];

            if ( ! card1.equals(nextCard)) {

                evaluateSuitedGapConnector(card1, nextCard, handSet);
            }
        }
    }

    private void evaluateRangedPair(String card1, String card2, Set<Hand> handSet) {

        int i1 = firstIndexOf(card1, ranks);
        int i2 = firstIndexOf(card2, ranks);

        for (int iRank = i1; iRank < i2+1; ++iRank) {

            String card = ranks[iRank];

            for (int iSuit1 = 0; iSuit1 < suits.length; ++iSuit1) {

                for (int iSuit2 = 0; iSuit2 < suits.length; ++iSuit2) {

                    if (iSuit1 > iSuit2) {
                        String suit1 = suits[iSuit1];
                        String suit2 = suits[iSuit2];

                        if ( ! suit1.equals(suit2)) {
                            handSet.add(new Hand(card + suit1 + " " + card + suit2));
                        }
                    }
                }
            }
        }
    }
}
