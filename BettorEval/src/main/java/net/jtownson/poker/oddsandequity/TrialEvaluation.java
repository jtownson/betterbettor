/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Card;
import ca.ualberta.cs.poker.Hand;
import ca.ualberta.cs.poker.HandEvaluator;
import org.apache.commons.lang.Validate;
import org.apache.commons.math3.util.Combinations;

import java.util.*;
import java.util.function.Predicate;

import static ca.ualberta.cs.poker.Card.Card;

/**
 * Evaluates and totals the player equity and hand combinations in a number
 * of trial hands.
 */
public class TrialEvaluation implements Predicate<Hand> {

    private final Set<String> trialIds = new HashSet<>();
    private final Equities equities;
    private final Hand[] dealtHands;
    private final Card[] dealtCards;
    private final int[] handRanks;
    private final boolean[] winners;
    private final HandHeaps handHeaps;
    private final List<HandWeights> handWeights;
    private final HandEvaluator handEvaluator = new HandEvaluator();
    private final int maxTrialsPerDeal;
    private double msErr;
    private int numTrials;
    private int numTrialsThisDeal;
    private Hand workingBoard = new Hand();

    /**
     * Create a simple trial, given a list of hands known with 100% certainty
     * (e.g. after an all-in bet). <code>test</code> can then be used to
     * simulate their equity on a given board.
     * @param hands some hands
     */
    public TrialEvaluation(List<Hand> hands) {
        this(new Hand(), hands2Weights(hands), 1);
    }

    /**
     * Create a simulation trial with some known dead cards and weighted hand distributions.
     * @param knownBoard some dead cards
     * @param handWeights weighted hand distributions
     * @param maxTrialsPerDeal A parameter indicating how mahy simulated boards you would like to
     *                         run for each deal of the players' cards. Usually 1.
     */
    public TrialEvaluation(Hand knownBoard, List<HandWeights> handWeights, int maxTrialsPerDeal) {

        this.maxTrialsPerDeal = maxTrialsPerDeal;
        this.handWeights = handWeights;

        int numPlayers = handWeights.size();

        winners = new boolean[numPlayers];
        equities = new Equities(numPlayers);
        dealtHands = new Hand[numPlayers];
        dealtCards = new Card[2*numPlayers];
        handRanks = new int[numPlayers];
        handHeaps = new HandHeaps(numPlayers);

        for(HandWeights weights : handWeights) {
            weights.setDeadCards(knownBoard);
        }

    }

    public double getMeanSquaredError() {
        return msErr;
    }

    public Equities getEquities() {
        return equities;
    }

    public double[] getFlopEquities() {
        return equities.flopEquities;
    }

    public double[] getTurnEquities() {
        return equities.turnEquities;
    }

    public double[] getRiverEquities() {
        return equities.riverEquities;
    }

    public HandHeaps getHandHeaps() {
        return handHeaps;
    }

    public HandHeap[] getFlopCategories() {
        return handHeaps.flopHandHeap;
    }

    public HandHeap[] getTurnCategories() {
        return handHeaps.turnHandHeap;
    }

    public HandHeap[] getRiverCategories() {
        return handHeaps.riverHandHeap;
    }

    public Card[] dealtCards() {
        return dealtCards;
    }

    public void dealHands() {

        Arrays.fill(dealtHands, null);

        int[] handIndices = Combinations.natural(dealtHands.length);
        Shuffler.shuffle(handIndices);

        for (int i = 0; i < dealtHands.length; i++) {

            int j = handIndices[i];

            dealtHands[j] = getSampleHand(j);
        }

        Arrays.fill(dealtCards, null);
        for (int i = 0; i < dealtHands.length; i++) {
            int j = 2*i;
            dealtCards[j] = dealtHands[i].getCard(1);
            dealtCards[j+1] = dealtHands[i].getCard(2);
        }
    }



    @Override
    public boolean test(Hand simulatedBoard) {

        Validate.isTrue(simulatedBoard.size() == 5);

        numTrials++;
        numTrialsThisDeal++;

        workingBoard.setCards(simulatedBoard);

        int nBest = rankAndSetWinners(workingBoard);
        assignEquities(nBest, equities.riverEquities, true);
        compileHands(nBest, handHeaps.riverHandHeap);

        workingBoard.removeCard();
        nBest = rankAndSetWinners(workingBoard);
        assignEquities(nBest, equities.turnEquities, false);
        compileHands(nBest, handHeaps.turnHandHeap);

        workingBoard.removeCard();
        nBest = rankAndSetWinners(workingBoard);
        assignEquities(nBest, equities.flopEquities, false);
        compileHands(nBest, handHeaps.flopHandHeap);

        return numTrialsThisDeal < maxTrialsPerDeal;
    }

    public void complete() {

        for (HandHeap[] handHeapArr : handHeaps.handHeaps) {
            for (HandHeap handHeap : handHeapArr) {
                handHeap.normalizeEquities();
            }
        }

        trialIds.clear();
    }

    private String getTrialId(Hand simulatedBoard) {

        // e.g. AcKh vs 5c4h on 2s3s4s5h6h board -> AcKh,5c4h,2s3s4s5h6h
        StringBuilder sb = new StringBuilder(2*dealtCards.length + 12);

        for (Hand dealtHand : dealtHands) {
            Card c1 = dealtHand.getCard(1);
            Card c2 = dealtHand.getCard(2);
            if (c1.getIndex() > c2.getIndex()) {
                sb.append(c1);
                sb.append(c2);
            } else {
                sb.append(c2);
                sb.append(c1);
            }
            sb.append(',');
        }

        int[] sc = simulatedBoard.getSortedCards();
        sb.append(Card(sc[0]));
        for (int i = 1; i < sc.length; i++) {
            sb.append(',');
            sb.append(Card(i));
        }

        return sb.toString();
    }

    private void assignEquities(double nBest, double[] equityArr, boolean tallyErrors) {

        double msErr_n_1 = msErr;

        double msErr_n = 0.0;

        for (int i = 0; i < dealtHands.length; i++) {

            // calculate running equity average using
            // En = (n-1)En-1 + En
            //       -------------
            //            N

            double En = winners[i] ? 1.0/nBest : 0.0;
            double En_1 = equityArr[i];
            En = ((numTrials-1) * En_1 + En) / numTrials;

            equityArr[i] = En;

            msErr_n += (En - En_1) * (En - En_1);
        }

        msErr_n = ((numTrials-1) * msErr_n_1 + msErr_n) / numTrials;

        // generally tally equity errors on the river since these converge the slowest
        if (tallyErrors) {
            msErr = msErr_n;
        }
    }

    private int rankAndSetWinners(Hand board) {

        int bestRank = 0;
        int nBest = 0;

        for (int h = 0; h < dealtHands.length; h++) {

            handRanks[h] = handEvaluator.rankHand(dealtHands[h].getCard(1), dealtHands[h].getCard(2), board);

            if (handRanks[h] > bestRank) {

                reset(winners);
                winners[h] = true;
                nBest = 1;
                bestRank = handRanks[h];

            } else if (handRanks[h] == bestRank) {

                winners[h] = true;
                nBest += 1;
            }
        }
        return nBest;
    }

    private void compileHands(int nBest, HandHeap[] categories) {

        for (int i = 0; i < dealtHands.length; i++) {

            int rank = handRanks[i];
            double weight = handWeights.get(i).weightFor(dealtHands[i]);
            double equity = winners[i] ? 1.0 / nBest : 0.0;

            //categories[i].push(madeHands[i], rank, weight, equity);
            categories[i].push(dealtHands[i], rank, weight, equity);
        }
    }

    private Hand getSampleHand(int j) {

        Hand h;
        do {

            h = handWeights.get(j).sample();

        } while (isBlocked(h));

        return h;
    }

    private void reset(boolean [] arr) {
        Arrays.fill(arr, false);
    }

    private boolean isBlocked(Hand h) {

        // NB the weight distributions have already
        // had the board declared as dead, so we
        // only check other dealt hands.

        for (Hand hand : dealtHands) {
            if (hand != null && hand.blocks(h)) {
                return true;
            }
        }

        return false;
    }

    private static List<HandWeights> hands2Weights(List<Hand> hands) {
        List<HandWeights> handWeights = new ArrayList<>(hands.size());
        for (Hand hand : hands) {
            handWeights.add(HandWeights.handBasedWeights(Arrays.asList(hand)));
        }
        return handWeights;
    }
}
