/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

/**
 * Wrapper around flop turn and river equities
 */
public class Equities {

    final double[][] equities;
    final double[] flopEquities;
    final double[] turnEquities;
    final double[] riverEquities;

    public Equities(int numPlayers) {
        flopEquities = new double[numPlayers];
        turnEquities = new double[numPlayers];
        riverEquities = new double[numPlayers];
        equities = new double[][]{riverEquities, turnEquities, flopEquities};
    }

    public double[] getFlopEquities() {
        return flopEquities;
    }

    public double[] getTurnEquities() {
        return turnEquities;
    }

    public double[] getRiverEquities() {
        return riverEquities;
    }
}
