package net.jtownson.poker.oddsandequity;

/**
 * A collection of hand heaps for several players, on each street.
 */
public class HandHeaps {

    final HandHeap[][] handHeaps;
    final HandHeap[] flopHandHeap;
    final HandHeap[] turnHandHeap;
    final HandHeap[] riverHandHeap;

    public HandHeaps(int numPlayers) {
        flopHandHeap = new HandHeap[numPlayers];
        turnHandHeap = new HandHeap[numPlayers];
        riverHandHeap = new HandHeap[numPlayers];
        handHeaps = new HandHeap[][]{riverHandHeap, turnHandHeap, flopHandHeap};

        for(int i = 0; i < numPlayers; i++) {
            flopHandHeap[i] = new HandHeap();
            turnHandHeap[i] = new HandHeap();
            riverHandHeap[i] = new HandHeap();
        }
    }

    public HandHeap[] getFlopHandHeap() {
        return flopHandHeap;
    }

    public HandHeap[] getTurnHandHeap() {
        return turnHandHeap;
    }

    public HandHeap[] getRiverHandHeap() {
        return riverHandHeap;
    }
}
