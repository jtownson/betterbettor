/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import au.com.bytecode.opencsv.CSVReader;
import ca.ualberta.cs.poker.Card;
import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Uses the "all in before the flop" measure of starting hand equity.
 */
public class AllInEquities implements StartingHandEquities {

    public static final int DISTINCT_STARTING_HANDS = 169;
    private static Map<Integer, List<HandEquity>> allInEquityMap;

    static {
        initializeEquities();
    }

    public List<HandEquity> getEquitiesForNumOpponents(int numOpponents) {
        return allInEquityMap.get(numOpponents);
    }

    private static synchronized void initializeEquities() {
        if (allInEquityMap == null) {

            Map<Integer, List<HandEquity>> m = new LinkedHashMap<Integer, List<HandEquity>>(8);

            for (int numOpponents = 1; numOpponents <= 8; numOpponents++) {
                m.put(numOpponents, getHandEquityForNumOpponents(numOpponents));
            }

            allInEquityMap = Collections.unmodifiableMap(m);
        }
    }

    private static List<HandEquity> getHandEquityForNumOpponents(int numOpponents) {

        List<HandEquity> handEquities = new ArrayList<HandEquity>(DISTINCT_STARTING_HANDS);

        String equityFile = equityFileNameForNumOppoenents(numOpponents);

        InputStreamReader is =
                new InputStreamReader(
                        AllInEquities.class.getClassLoader().getResourceAsStream(equityFile));

        try {
            CSVReader reader = new CSVReader(is);
            String [] nextLine;
            while ((nextLine = reader.readNext()) != null) {

                if (nextLine.length == 4) {

                    int rank = Integer.parseInt(nextLine[0]);
                    double range = Double.parseDouble(nextLine[1]);
                    String startingHand = nextLine[2];
                    double equity = Double.parseDouble(nextLine[3]) / 100.0;

                    handEquities.add(new HandEquity(rank, range, startingHand, equity));
                }
            }
            return Collections.unmodifiableList(handEquities);

        } catch(IOException e) {
            throw new IllegalStateException(
                    "IOException initializing from equity csv file " + equityFile, e);
        } finally {
            try {is.close();} catch(Exception e) {}
        }
    }

    private static String equityFileNameForNumOppoenents(int numOppoenents) {
        return numOppoenents + "op.csv";
    }
}
