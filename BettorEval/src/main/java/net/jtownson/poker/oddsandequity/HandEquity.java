/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

/**
 *
 */
public class HandEquity {
    private final int rank;
    private final double range;
    private final String startingHand;
    private final double equity;

    public HandEquity(int rank, double range, String startingHand, double equity) {
        this.rank = rank;
        this.range = range;
        this.startingHand = startingHand;
        this.equity = equity;
    }

    public int getRank() {
        return rank;
    }

    public double getRange() {
        return range;
    }

    public String getStartingHand() {
        return startingHand;
    }

    public double getEquity() {
        return equity;
    }

    public double getIncomeRate() {
        return equity - 0.5;
    }
}
