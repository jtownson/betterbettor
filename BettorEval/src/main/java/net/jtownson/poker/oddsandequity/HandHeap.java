/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Hand;

import java.util.*;

/**
 * The simulator dumps winning and losing hands onto the hand heap
 * which composts them into hand categories (i.e. something comprehensible
 * to a person).
 */
public class HandHeap {

    private Map<String, HandCategory> categories = new HashMap<>();

    private int nPushes = 0;

    public HandCategory push(Hand hand, int handRank, double weight, double equity) {

        HandCategory newCategory = new HandCategory(hand, handRank);

        HandCategory currentCategory = categories.get(newCategory.getHandDescription());

        nPushes += 1;

        // we assume here that this is not a duplicate trial
        // and thus we are not double counting the equity
        // attributable to this hand.
        if (currentCategory != null) {

            if ( ! currentCategory.containsHand(hand)) {
                currentCategory.addHand(hand);
                currentCategory.weight += weight;
            }

            currentCategory.equity += equity;

            return currentCategory;

        } else {

            newCategory.weight = weight;
            newCategory.equity = equity;
            newCategory.addHand(hand);

            categories.put(newCategory.getHandDescription(), newCategory);

            return newCategory;
        }
    }

    public void normalizeEquities() {
        for (HandCategory category : categories.values()) {
            category.equity /= nPushes;
        }
    }

    public double getTotalEquity() {
        double equity = 0.0;
        for (HandCategory category : categories.values()) {
            equity += category.getEquity();
        }
        return equity;
    }

    public Collection<HandCategory> getHandCategories() {
        return categories.values();
    }

    public List<HandCategory> getHandCategoriesByWeight() {
        List<HandCategory> ls = new ArrayList<>(categories.values());
        Collections.sort(ls, (hc1, hc2) -> Double.compare(hc2.weight, hc1.weight));
        return ls;
    }

    public List<HandCategory> getHandCategoriesByEquity() {
        List<HandCategory> ls = new ArrayList<>(categories.values());
        Collections.sort(ls, (hc1, hc2) -> Double.compare(hc2.equity, hc1.equity));
        return ls;
    }
}
