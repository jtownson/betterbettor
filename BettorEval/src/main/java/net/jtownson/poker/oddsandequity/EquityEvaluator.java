/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/

package net.jtownson.poker.oddsandequity;

import ca.ualberta.cs.poker.Card;
import ca.ualberta.cs.poker.Deck;
import ca.ualberta.cs.poker.Hand;
import org.apache.commons.lang.Validate;

import java.util.List;

/**
 * <p>Calculates equities on all streets for arbitrary boards.</p>
 * <p>The calculation is non-isometric, supporting hand distributions with arbitrary weights.</p>
 * <p>The algorithm uses monte-carlo simulation to reach a user-specified error tolerance.</p>
 */
public class EquityEvaluator {

    private HandEnumerator handEnumerator = new HandEnumerator();

    /**
     * Calculates starting hand equities (i.e. pre-flop all-in equities).
     *
     * @param deck a deck
     * @param handWeights hand distributions for which to calculate equities
     * @param maxMeanSquaredError convergence factor (e.g. to get accuracy to 1pct, specify 0.01*0.01, etc).
     * @return equities on flop turn and river. Traditional all-in equities are are obviously the equity on the river.
     */
    public TrialEvaluation getStartingHandEquities(Deck deck, List<HandWeights> handWeights, double maxMeanSquaredError) {
        return getEquity(deck, new Hand(), handWeights, maxMeanSquaredError);
    }

    /**
     * Calculates equities when board cards have been dealt (i.e. post-flop all-in equities).
     *
     * @param handWeights hand distributions for which to calculate equities
     * @param maxMeanSquaredError convergence factor (e.g. to get accuracy to 1pct, specify 0.01*0.01, etc).
     * @return equities on flop turn and river. Traditional all-in equities are are obviously the equity on the river.
     */
    public TrialEvaluation getEquity(Hand board, List<HandWeights> handWeights, double maxMeanSquaredError) {
        return getEquity(new Deck(), board, handWeights, maxMeanSquaredError);
    }

    /**
     * Calculates equities when board cards have been dealt (i.e. post-flop all-in equities).
     *
     * @param deck a deck
     * @param handWeights hand distributions for which to calculate equities
     * @param maxMeanSquaredError convergence factor (e.g. to get accuracy to 1pct, specify 0.01*0.01, etc).
     * @return equities on flop turn and river. Traditional all-in equities are are obviously the equity on the river.
     */
    public TrialEvaluation getEquity(Deck deck, Hand board, List<HandWeights> handWeights, double maxMeanSquaredError) {

        Validate.isTrue(board.size() == 0 || board.size() == 3 || board.size() == 4 || board.size() == 5);

        int trialsPerDeal = 1;
        int maxTrials = 100000;
        int numTrials = 0;
        double meanSquaredError;

        TrialEvaluation trial = new TrialEvaluation(board, handWeights, trialsPerDeal);

        do {
            numTrials++;

            trial.dealHands();

            if (board.size() == 0) {

                handEnumerator.mcEnumerate5CardBoards(deck, trial.dealtCards(), trial::test);

            } else if (board.size() == 3) {


                handEnumerator.mcEnumerateRivers(deck, trial.dealtCards(), board, trial::test);

            } else if (board.size() == 4) {

                Card[] flop = new Card[]{board.getCard(1), board.getCard(2), board.getCard(3)};
                Card turn = board.getCard(4);

                handEnumerator.mcEnumerateRivers(deck, trial.dealtCards(), flop, turn, trial::test);

            } else if (board.size() == 5) {

                trial.test(board);
            }

            meanSquaredError = trial.getMeanSquaredError();

        } while (numTrials < maxTrials && meanSquaredError > maxMeanSquaredError);

        trial.complete();

        return trial;
    }

}
