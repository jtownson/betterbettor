/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import net.jtownson.poker.table.*;

import java.util.Date;

import static net.jtownson.poker.table.EnumRound.blinds;

/**
 *
 */
public class ModelUpdatingActionController extends DefaultActionController {

    public ModelUpdatingActionController() {
        super(anyGameType, anyHand);
    }

    public ModelUpdatingActionController(GameTypeFilter gameTypeFilter) {
        super(gameTypeFilter, anyHand);
    }

    public ModelUpdatingActionController(HandFilter handFilter) {
        super(anyGameType, handFilter);
    }

    public ModelUpdatingActionController(GameTypeFilter gameTypeFilter, HandFilter handFilter) {
        super(gameTypeFilter, handFilter);
    }

    @Override
    public void errorCondition(PokerTable table, String userMessage, ErrorType errorType) {

    }

    @Override
    public void startGame(PokerTable table) {

    }

    @Override
    public void endGame(PokerTable table) {

    }

    @Override
    public boolean acceptGame(PokerTable table, GameType gameType) {
        boolean isAcceptable = getGameTypeFilter().acceptGameType(gameType);
        if (isAcceptable) {
            table.setGameType(gameType);
        }
        return isAcceptable;
    }

    @Override
    public boolean acceptHand(PokerTable table, String handId) {
        return getHandFilter().acceptHand(table, handId);
    }

    @Override
    public void startHand(PokerTable table, String handId, Date startDateTime) {
        clearAnyPreviousRound(table, handId);
        table.riverIs("");
        table.turnIs("");
        table.flopIs("");
        table.getPots().clear();
        table.getPots().add(new Pot());
        table.getPlayers().setReadyForNextHand();
        table.setHandId(handId);
        table.setCurrentRound(blinds);
    }

    private void clearAnyPreviousRound(PokerTable table, String handId) {
        if (table.getHandId() != null && ! table.getHandId().equals(handId)) {
            endHand(table);
        }
    }

    @Override
    public void endHand(PokerTable table) {
        if (table.getHandId() != null) {
            table.getDealer().awardPots();
            table.setHandId(null);
        }
    }

    @Override
    public void startRound(PokerTable table) {

    }

    @Override
    public void endRound(PokerTable table) {
        EnumRound round = table.getCurrentRound();
        if (round != blinds) {
            table.getDealer().gatherBets();
        }
        table.nextRound();
    }

    @Override
    public void postSmallBlind(PokerTable table, PokerPlayer player, double amount) {
        player.putIn(amount);
        table.getPlayers().hasActed(player);
    }

    @Override
    public void postBigBlind(PokerTable table, PokerPlayer player, double amount) {
        player.putIn(amount);
        table.getPlayers().hasActed(player);
    }

    @Override
    public void folds(PokerTable table, PokerPlayer player) {
        player.foldAndMuck();
        table.getPlayers().hasActed(player);
    }

    @Override
    public void checks(PokerTable table, PokerPlayer player) {
        player.putIn(0.0);
        table.getPlayers().hasActed(player);
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player) {
        double amountToGo = table.getDealer().getAmountToCall();
        calls(table, player, amountToGo);
        table.getPlayers().hasActed(player);
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player, double amount) {
        player.putIn(amount);
        table.getPlayers().hasActed(player);
    }

    @Override
    public void bets(PokerTable table, PokerPlayer player, double amount) {
        player.putIn(amount);
        table.getPlayers().hasActed(player);
    }

    @Override
    public void raises(PokerTable table, PokerPlayer player, double raiseTo) {
        double alreadyIn = player.getBetsMade();
        player.putIn(raiseTo - alreadyIn);
        table.getPlayers().hasActed(player);
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player, double betAmount) {
        player.setStack(betAmount);
        player.setStack(betAmount);
        allIn(table, player);
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player) {

        double betAmount = player.getStack();
        double callAmount = table.getDealer().getAmountToCall();
        double alreadyIn = player.getBetsMade();
        // Poss there is a nuance here because
        // An 'all-in' bet of less than a full raise does not reopen the betting to a player who has already acted.
        // (from http://www.fidpa.com/international-poker-rules/section-2/all-in/#2-60)
        if (betAmount <= callAmount - alreadyIn) {
            calls(table, player, betAmount);
        } else {
            raises(table, player, betAmount - callAmount + alreadyIn);
        }
        player.allIn();
        table.getPlayers().hasActed(player);
    }

    @Override
    public void takesSeat(PokerTable table, PokerPlayer player) {
        if (table.getPlayers().getByName(player.getName()) == null) {
            table.getPlayers().insert(player);
        }
    }

    @Override
    public void dealIn(PokerTable table, PokerPlayer player) {
        player.dealIn();
    }

    @Override
    public void sitsOut(PokerTable table, PokerPlayer player) {
        player.sitOut();
    }

    @Override
    public void dealsFlop(PokerTable table, String flop) {
        table.flopIs(flop);
    }

    @Override
    public void dealsTurn(PokerTable table, String turn) {
        table.turnIs(turn);
    }

    @Override
    public void dealsRiver(PokerTable table, String river) {
        table.riverIs(river);
    }

    @Override
    public void handSeen(PokerTable table, PokerPlayer player, String cards) {
        player.setPocketCards(cards);
    }

    @Override
    public void shows(PokerTable table, PokerPlayer player, String hand) {
        player.setShowDownHand(hand);
        table.getPlayers().hasActed(player);
    }

}
