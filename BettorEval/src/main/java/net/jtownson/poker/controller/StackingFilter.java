/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import java.util.*;

/**
 * Each time you push a new filter level,
 * all objects are included from the previous level.
 * This allows implementation of a filter that works
 * in several passes/levels, without losing state from each
 * pass.
 *
 * Created by jtownson on 22/01/14.
 */
public class StackingFilter<T> {

    private List<Set<T>> filters = new ArrayList<>();

    public StackingFilter(T... initialObjects) {
        this(Arrays.asList(initialObjects));
    }

    public StackingFilter(List<T> initialObjects) {
        filters.add(orderedSet(initialObjects));
    }

    public void pushLevel() {
        ensureLevel(topIndex() + 1);
    }

    public Set<T> popLevel() {
        if (topIndex() < 1) {
            return null;
        }
        return filters.remove(topIndex());
    }

    public boolean isIncluded(T t) {
        return topLevel().contains(t);
    }

    public boolean isExcluded(T t) {
        return ! isIncluded(t);
    }

    public boolean include(T t) {
        return topLevel().add(t);
    }

    public boolean exclude(T t) {
        return topLevel().remove(t);
    }

    public Set<T> included() {
        return topLevel();
    }

    private void ensureLevel(int level) {
        if (level > topIndex()) {
            if (topIndex() == -1) {
                filters.add(orderedSet(Collections.EMPTY_SET));
            } else {
                filters.add(orderedSet(topLevel()));
            }
            ensureLevel(level);
        }
    }

    private Set<T> topLevel() {
        return filters.get(topIndex());
    }

    private int topIndex() {
        return filters.size() - 1;
    }

    private <T> Set<T> orderedSet(Collection<? extends T> c) {
        return new LinkedHashSet<>(c);
    }
}
