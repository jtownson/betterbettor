/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;

import java.util.Date;

/**
 * Allows hand-history players and interactive UIs to send hand events
 * to a data model.
 */
public interface ActionController {

    public enum ErrorType { parsingOrFormatError }

    /**
     * Called to indicate an error in game playback.
     */
    void errorCondition(PokerTable table, String userMessage, ErrorType errorType);

    /**
     * A chance for implementations to filter the hands they receive.
     * @param table the table
     * @param gameType holdem NL, omaha PL, etc
     * @return false if the player should skip this hand
     */
    boolean acceptGame(PokerTable table, GameType gameType);

    /**
     * Start a new poker game/session
     */
    void startGame(PokerTable table);

    /**
     * End the poker game/session started via startGame
     */
    void endGame(PokerTable table);

    /**
     * Add a player to the table.
     */
    void takesSeat(PokerTable table, PokerPlayer player);

    /**
     * Deal in a player for the hand. This distinguishes players
     * at the table (e.g. waiting for the big blind)
     * from those actually playing
     */
    void dealIn(PokerTable table, PokerPlayer player);

    /**
     * Callbacks can be sure that the players sit out status will have been set.
     * @param player
     */
    void sitsOut(PokerTable table, PokerPlayer player);

    /**
     * A change for implementations to filter specific hands.
     */
    boolean acceptHand(PokerTable table, String handId);

    /**
     * Lets callbacks get the hand start time (mainly applies to hand histories)
     * @param table POJO with the table state.
     * @param handId the hand id
     * @param startDateTime hand timestamp (perhaps useful if playing from a history file)
     */
    void startHand(PokerTable table, String handId, Date startDateTime);

    /**
     * Called at the end of the end. Callbacks can be sure that bets will have been allocated
     * and stacks adjusted, etc.
     * @param table
     */
    void endHand(PokerTable table);

    /**
     * Start a new round of betting.
     * @param table the table on which the action occurred
     */
    void startRound(PokerTable table);

    /**
     * End a round of betting.
     */
    void endRound(PokerTable table);

    /**
     * Callbacks can be sure that the players putIn in front is consistent with the small blind size.
     * @param player
     * @param amount
     */
    void postSmallBlind(PokerTable table, PokerPlayer player, double amount);

    /**
     * Callbacks can be sure that the players bet in front to be consistent with the big blind size.
     * @param player
     * @param amount
     */
    void postBigBlind(PokerTable table, PokerPlayer player, double amount);

    /**
     * Callbacks can be sure that the players fold status will have been set.
     * @param player
     */
    void folds(PokerTable table, PokerPlayer player);

    /**
     * Callbacks can be sure that the players bets in front are zero.
     * @param player
     */
    void checks(PokerTable table, PokerPlayer player);

    /**
     * Callbacks can be sure that the players bets in front to be consistent with the current value to go.
     * @param player
     */
    void calls(PokerTable table, PokerPlayer player);

    /**
     * For calls where the call amount is not necessarily the amount to go
     * and so callers wish to specify this value
     * (e.g. call all in for a smaller amount)
     * @param table the table
     * @param player the player
     * @param amount the call amount. This should be the amount upto which the player
     *               has called (not the bet amount, which will be the amount called
     *               minus any money the player has already put in).
     */
    void calls(PokerTable table, PokerPlayer player, double amount);

    /**
     * Callbacks can be sure that player's bets in front is set accordingly.
     * @param player
     * @param amount
     */
    void bets(PokerTable table, PokerPlayer player, double amount);

    /**
     * Callbacks can be sure that player's bets in front is set accordingly.
     * @param player
     * @param raiseAmount
     */
    void raises(PokerTable table, PokerPlayer player, double raiseAmount);

    /**
     * This is a hack to get around the fact that in chat parsing,
     * the players stacks are not known so we require a hint as
     * to the bet amount.
     * @param table the table
     * @param player the player
     * @param betAmount the amounts of the bet, assumed to be the player's entire stack.
     */
    void allIn(PokerTable table, PokerPlayer player, double betAmount);

    /**
     * Either calling or raising all-in.
     * @param player the player
     */
    void allIn(PokerTable table, PokerPlayer player);

    /**
     * Callbacks can be sure that all bets will be transferred to the pot(s)
     * @param table the table's current state.
     */
    void dealsFlop(PokerTable table, String flop);

    /**
     * Callbacks can be sure that all bets will be transferred to the pot(s)
     */
    void dealsTurn(PokerTable table, String turn);

    /**
     * Callbacks can be sure that all bets will be transferred to the pot(s)
     */
    void dealsRiver(PokerTable table, String river);

    /**
     * In hand histories, hands that go to show down can
     * result in pocket cards being declared at the start
     * of the hand, as though we had snuck a look at somebody's
     * cards
     */
    void handSeen(PokerTable table, PokerPlayer player, String cards);

    /**
     * Callbacks can be sure that all bets will be transferred to the pot(s)
     */
    void shows(PokerTable table, PokerPlayer player, String hand);

}
