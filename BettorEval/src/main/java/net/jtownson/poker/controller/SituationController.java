/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import ca.ualberta.cs.poker.Hand;
import net.jtownson.poker.oddsandequity.EquityEvaluator;
import net.jtownson.poker.oddsandequity.HandWeights;
import net.jtownson.poker.oddsandequity.TrialEvaluation;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import net.jtownson.poker.table.Situation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Extracts parameters for player situation analysis
 * (their pot odds, position, etc).
 *
 * Created by jtownson on 27/01/14.
 */
public class SituationController extends DefaultActionController {

    private EquityEvaluator equityEvaluator = new EquityEvaluator();
    private Situation currentSituation;

    public Situation getCurrentSituation() {
        return currentSituation;
    }

    public SituationController() {
        super(anyGameType, anyHand);
    }

    public SituationController(HandFilter handFilter) {
        super(anyGameType, handFilter);
    }

    public SituationController(GameTypeFilter gameTypeFilter, HandFilter handFilter) {
        super(gameTypeFilter, handFilter);
    }

    @Override
    public void startHand(PokerTable table, String handId, Date startDateTime) {
        currentSituation = null;
    }

    @Override
    public void endHand(PokerTable table) {
        currentSituation = null;
    }

    private void refreshSituation(PokerTable table, PokerPlayer player) {

        PokerPlayer nextUp = table.getPlayers().getNextToAct();

        //if ( ! nextUp.getName().equals(table.getHerosName())) {
        //    currentSituation = null;
        //    return;
        //}

        List<PokerPlayer> actedAhead = table.getPlayers().getPlayersInAhead(nextUp);
        List<PokerPlayer> toActBehind = table.getPlayers().getPlayersInBehind(nextUp);

        double potSize = table.getDealer().getPotValueForPlayer(nextUp);
        double amountToCall = table.getDealer().getAmountToCall() - nextUp.getBetsMade();

        Hand board = new Hand(table.getBoardCards());
        List<PokerPlayer> playersIn = table.getPlayers().getUnfolded();
        List<HandWeights> handWeights = compileHandWeights(playersIn);
        TrialEvaluation trialEvaluation = equityEvaluator.getEquity(board, handWeights, 0.001);

        double streetEx = expectationForStreet(trialEvaluation, playersIn, board, nextUp, potSize, amountToCall);

        currentSituation = new Situation(
                nextUp, actedAhead.size(), toActBehind.size(), potSize, amountToCall, streetEx);

        System.out.println("*** Game state ***");
        System.out.println(table.getPlayers());
        System.out.println(currentSituation);
    }


    private double expectationForStreet(
            TrialEvaluation trialEvaluation, List<PokerPlayer> playersIn, Hand board, PokerPlayer nextUp, double potSize, double amountToCall) {

        int playerIndex = playersIn.indexOf(nextUp);

        double pot = potSize + amountToCall;

        double potEq = amountToCall / pot;

        double flopEq = trialEvaluation.getFlopEquities()[playerIndex];
        double flopEx = pot * (flopEq - potEq);

        double turnEq = trialEvaluation.getTurnEquities()[playerIndex];
        double turnEx = pot * (turnEq - potEq);

        double riverEq = trialEvaluation.getRiverEquities()[playerIndex];
        double riverEx = pot * (riverEq - potEq);

        double streetEx = riverEx;

        if (board.size() == 0) {

            streetEx = riverEx;

        } else if (board.size() == 3) {

            streetEx = flopEx;

        } else if (board.size() == 4) {

            streetEx = turnEx;

        } else if (board.size() == 5) {

            streetEx = riverEx;
        }
        return streetEx;
    }


    private List<HandWeights> compileHandWeights(List<PokerPlayer> players) {
        List<HandWeights> weights = new ArrayList<>(players.size());
        players.forEach(p -> weights.add(p.getHandWeights()));
        return weights;
    }

    @Override
    public void postSmallBlind(PokerTable table, PokerPlayer player, double amount) {
        //refreshSituation(table, player);
    }

    @Override
    public void postBigBlind(PokerTable table, PokerPlayer player, double amount) {
        refreshSituation(table, player);
    }

    @Override
    public void folds(PokerTable table, PokerPlayer player) {
        refreshSituation(table, player);
    }

    @Override
    public void checks(PokerTable table, PokerPlayer player) {
        refreshSituation(table, player);
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player) {
        refreshSituation(table, player);
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player, double amount) {
        refreshSituation(table, player);
    }

    @Override
    public void bets(PokerTable table, PokerPlayer player, double amount) {
        refreshSituation(table, player);
    }

    @Override
    public void raises(PokerTable table, PokerPlayer player, double raiseAmount) {
        refreshSituation(table, player);
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player, double betAmount) {
        refreshSituation(table, player);
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player) {
        refreshSituation(table, player);
    }

    @Override
    public void handSeen(PokerTable table, PokerPlayer player, String cards) {
        refreshSituation(table, player);
    }
}
