/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

/**
 * Provides a common interface for replaying game actions,
 * whether they are from hand histories, screen scrapers,
 * manual input, etc.
 */
public interface GameReplayer {

    void start(ActionController actionCallback);
    void stop();
    void rewind(int numberOfHands);
    void forwind(int numberOfHands);

}
