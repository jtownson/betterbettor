/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;

import java.util.Date;
import java.util.Map;

import static java.lang.System.*;

/**
 * Created by jtownson on 22/01/14.
 */
public class PrintlnActionController extends DefaultActionController {

    public PrintlnActionController() {
        super(anyHand);
    }

    public PrintlnActionController(HandFilter handFilter) {
        super(handFilter);
    }

    @Override
    public void startGame(PokerTable table) {
        out.println("Starting a new game " + table.getGameType() + " " + table.getGameType());
    }

    @Override
    public void startHand(PokerTable table, String handId, Date startDateTime) {
        out.println("Starting hand " + handId + ". Game is " + table.getGameType());
    }

    @Override
    public void postSmallBlind(PokerTable table, PokerPlayer player, double amount) {
        out.println(
                player.getName() + " posts the small blind " + table.getGameType().getCurrencySymbol() + amount);
    }

    @Override
    public void postBigBlind(PokerTable table, PokerPlayer player, double amount) {
        out.println(
                player.getName() + " posts the big blind " + table.getGameType().getCurrencySymbol() + amount);
    }

    @Override
    public void folds(PokerTable table, PokerPlayer player) {
        out.println(
                player.getName() + " folds");
    }

    @Override
    public void takesSeat(PokerTable table, PokerPlayer player) {
        out.println(player.getName() + " joins the table.");
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player) {
        out.println(player.getName() + " calls " + table.getDealer().getAmountToCall());
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player, double betAmount) {
        out.println(player.getName() + " goes all-in for " + table.getGameType().getCurrencySymbol() + betAmount);
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player) {
        out.println(player.getName() + " goes all-in for " + table.getGameType().getCurrencySymbol() + player.getStack());
    }

    @Override
    public void handSeen(PokerTable table, PokerPlayer player, String cards) {
        if (player != null) {
            out.println(player.getName() + " has " + cards + " in the hole.");
        }
    }

    @Override
    public void shows(PokerTable table, PokerPlayer player, String hand) {
        out.println(player.getName() + " shows " + hand);
    }

    @Override
    public void checks(PokerTable table, PokerPlayer player) {
        out.println(player.getName() + " checks");
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player, double amount) {
        out.println(
                player.getName() + " calls");
    }

    @Override
    public void bets(PokerTable table, PokerPlayer player, double amount) {
        out.println(
                player.getName() + " bets " + table.getGameType().getCurrencySymbol() + amount);
    }

    @Override
    public void raises(PokerTable table, PokerPlayer player, double raiseAmount) {

        String currency = table.getGameType().getCurrencySymbol();
        double amountToGo = table.getDealer().getAmountToCall();

        out.println(
                player.getName() + " raises by " + currency + raiseAmount + " making it " + currency + amountToGo + " to go");
    }

    @Override
    public void sitsOut(PokerTable table, PokerPlayer player) {
        out.println(
                player.getName() + " has sat out");
    }

    @Override
    public void dealsFlop(PokerTable table, String flop) {
        out.println(
                "Flop comes " + table.getFlop());
    }

    @Override
    public void dealsTurn(PokerTable table, String turn) {
        out.println(
                "Turn comes " + table.getTurn());
    }

    @Override
    public void dealsRiver(PokerTable table, String river) {
        out.println(
                "River comes " + table.getRiver());

    }

    @Override
    public void endHand(PokerTable table) {
        Map<String, Double> winnings = table.getPlayersWinnings();
        out.print("Hand finished. ");
        for (String player : winnings.keySet()) {
            double amnt = winnings.get(player);
            if (amnt != 0) {
                out.print(player + " wins/takes back " + table.getGameType().getCurrencySymbol() + amnt + ". ");
            }
        }
        out.println();
    }
}
