/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import static net.jtownson.poker.controller.GameType.LimitType.*;
import static net.jtownson.poker.controller.GameType.Name.Holdem;
import static net.jtownson.poker.controller.GameType.Name.Omaha;

/**
 *
 */
public class GameType {

    private Name name;
    private Currency tableCurrency;
    private CurrencySymbol currencySymbol;
    private LimitType limitType;
    private double smallBlind;
    private double bigBlind;

    public enum Name {
        Holdem, Omaha
    }

    public enum Currency {
        GBP, USD, EUR, FUN
    }

    public enum CurrencySymbol {
        $("$"), £("£"), €("€"), FUN("F");
        private String symbol;

        private CurrencySymbol(String symbol) {
            this.symbol = symbol;
        }

        @Override
        public String toString() {
            return symbol;
        }
    }

    public enum LimitType {
        NL, PL, L
    }


    public GameType(Name name, Currency tableCurrency, CurrencySymbol currencySymbol, LimitType limitType, double smallBlind, double bigBlind) {
        this.name = name;
        this.tableCurrency = tableCurrency;
        this.currencySymbol = currencySymbol;
        this.limitType = limitType;
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
    }

    public GameType(String name, String tableCurrency, String currencySymbol, String limitType, double smallBlind, double bigBlind) {

        this.name = Name.valueOf(name);
        this.limitType = LimitType.valueOf(limitType);
        this.smallBlind = smallBlind;
        this.bigBlind = bigBlind;
        setCurrencyOrUseNull(tableCurrency);
        setSymbolOrUseDefault(currencySymbol);
    }

    private void setCurrencyOrUseNull(String tableCurrency) {
        this.tableCurrency = StringUtils.isBlank(tableCurrency) ? null : Currency.valueOf(tableCurrency);
    }

    public GameType(GameType gt) {
        this.name = gt.name;
        this.tableCurrency = gt.tableCurrency;
        this.limitType = gt.limitType;
        this.smallBlind = gt.smallBlind;
        this.bigBlind = gt.bigBlind;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(name).append(tableCurrency).append(limitType).toHashCode();
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GameType lhs = this;
        GameType rhs = (GameType) o;

        return new EqualsBuilder()
                .append(lhs.name, rhs.name)
                .append(lhs.tableCurrency, rhs.tableCurrency)
                .append(lhs.limitType, rhs.limitType)
                .append(lhs.smallBlind, rhs.smallBlind)
                .append(lhs.bigBlind, rhs.bigBlind).isEquals();

    }

    @Override
    public String toString() {
        return name + " " + limitType + " " + smallBlind + '/' + bigBlind + " (" + tableCurrency + ')';
    }

    public boolean isNoLimit() {
        return NL == limitType;
    }


    public boolean isLimit() {
        return L == limitType;
    }

    public boolean isPotLimit() {
        return PL == limitType;
    }

    public boolean isHoldem() {
        return Holdem == name;
    }

    public boolean isOmaha() {
        return Omaha == name;
    }

    public double getSmallBlind() {
        return smallBlind;
    }

    public double getBigBlind() {
        return bigBlind;
    }

    public String getTableCurrency() {
        return tableCurrency.toString();
    }

    public void setTableCurrency(String tableCurrency) {
        setCurrencyOrUseNull(tableCurrency);
    }

    public void setSmallBlind(double smallBlind) {
        this.smallBlind = smallBlind;
    }

    public void setBigBlind(double bigBlind) {
        this.bigBlind = bigBlind;
    }

    public String getCurrencySymbol() {
        return currencySymbol.toString();
    }

    public void setCurrencySymbol(String currencySymbol) {
        setSymbolOrUseDefault(currencySymbol);
    }

    private void setSymbolOrUseDefault(String currencySymbol) {
        this.currencySymbol = StringUtils.isBlank(currencySymbol) ? CurrencySymbol.FUN : CurrencySymbol.valueOf(currencySymbol);
    }
}