/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;

import java.util.*;

/**
 *
 *
 */

public class CompositeActionController implements ActionController {

    private Map<PokerTable, StackingFilter<ActionController>> tableFilters = new HashMap<>();

    private List<ActionController> allControllers = new ArrayList<>();

    public CompositeActionController(ActionController... controllers) {
        allControllers.addAll(Arrays.asList(controllers));
    }

    private Set<ActionController> included(PokerTable table) {
        return filter(table).included();
    }

    private StackingFilter<ActionController> filter(PokerTable table) {
        StackingFilter<ActionController> filter = tableFilters.get(table);
        if (filter == null) {
            filter = new StackingFilter<>(allControllers);
            tableFilters.put(table, filter);
        }
        return filter;
    }

    @Override
    public boolean acceptGame(PokerTable table, GameType gameType) {

        StackingFilter<ActionController> filter = filter(table);
        filter.pushLevel();

        for (ActionController controller : allControllers) {
            if ( ! controller.acceptGame(table, gameType)) {
                filter.exclude(controller);
            }
        }
        return filter.included().size() > 0;
    }

    @Override
    public void startGame(PokerTable table) {
        for(ActionController controller : included(table)) {
            controller.startGame(table);
        }
    }

    @Override
    public void endGame(PokerTable table) {
        try {
            for(ActionController controller : included(table)) {
                controller.endGame(table);
            }
        } finally {
            filter(table).popLevel();
        }
    }

    @Override
    public boolean acceptHand(PokerTable table, String handId) {

        StackingFilter<ActionController> filter = filter(table);
        filter.pushLevel();
        Set<ActionController> toExclude = new HashSet<>();
        for(ActionController controller : filter.included()) {
            if ( ! controller.acceptHand(table, handId)) {
                toExclude.add(controller);
            }
        }
        for (ActionController controller : toExclude) {
            filter.exclude(controller);
        }

        return filter.included().size() > 0;
    }

    @Override
    public void startHand(PokerTable table, String handId, Date startDateTime) {
        for(ActionController callback : filter(table).included()) {
            callback.startHand(table, handId, startDateTime);
        }
    }

    @Override
    public void endHand(PokerTable table) {
        try {
            for(ActionController callback : included(table)) {
                callback.endHand(table);
            }
        } finally {
            filter(table).popLevel();
        }
    }


    @Override
    public void takesSeat(PokerTable table, PokerPlayer player) {
        for(ActionController controller : included(table)) {
            controller.takesSeat(table, player);
        }
    }

    @Override
    public void dealIn(PokerTable table, PokerPlayer player) {
        for (ActionController controller : included(table)) {
            controller.dealIn(table, player);
        }
    }

    @Override
    public void errorCondition(PokerTable table, String userMessage, ErrorType errorType) {
        for(ActionController controller : included(table)) {
            controller.errorCondition(table, userMessage, errorType);
        }
    }

    @Override
    public void postSmallBlind(PokerTable table, PokerPlayer player, double amount) {
        for(ActionController callback : included(table)) {
            callback.postSmallBlind(table, player, amount);
        }
    }

    @Override
    public void postBigBlind(PokerTable table, PokerPlayer player, double amount) {
        for(ActionController callback : included(table)) {
            callback.postBigBlind(table, player, amount);
        }
    }

    @Override
    public void folds(PokerTable table, PokerPlayer player) {
        for(ActionController callback : included(table)) {
            callback.folds(table, player);
        }
    }

    @Override
    public void checks(PokerTable table, PokerPlayer player) {
        for(ActionController callback : included(table)) {
            callback.checks(table, player);
        }
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player) {
        for(ActionController controller : included(table)) {
            controller.calls(table, player);
        }
    }

    @Override
    public void calls(PokerTable table, PokerPlayer player, double amount) {
        for(ActionController callback : included(table)) {
            callback.calls(table, player, amount);
        }

    }

    @Override
    public void bets(PokerTable table, PokerPlayer player, double amount) {
        for(ActionController callback : included(table)) {
            callback.bets(table, player, amount);
        }
    }

    @Override
    public void raises(PokerTable table, PokerPlayer player, double raiseAmount) {
        for(ActionController callback : included(table)) {
            callback.raises(table, player, raiseAmount);
        }
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player, double betAmount) {
        for (ActionController controller : included(table)) {
            controller.allIn(table, player, betAmount);
        }
    }

    @Override
    public void allIn(PokerTable table, PokerPlayer player) {
        for (ActionController controller : included(table)) {
            controller.allIn(table, player);
        }
    }

    @Override
    public void sitsOut(PokerTable table, PokerPlayer player) {
        for(ActionController callback : included(table)) {
            callback.sitsOut(table, player);
        }
    }

    @Override
    public void dealsFlop(PokerTable table, String flop) {
        for(ActionController callback : included(table)) {
            callback.dealsFlop(table, flop);
        }
    }

    @Override
    public void dealsTurn(PokerTable table, String turn) {
        for(ActionController callback : included(table)) {
            callback.dealsTurn(table, turn);
        }
    }

    @Override
    public void dealsRiver(PokerTable table, String river) {
        for(ActionController callback : included(table)) {
            callback.dealsRiver(table, river);
        }
    }

    @Override
    public void handSeen(PokerTable table, PokerPlayer player, String cards) {
        for (ActionController controller : included(table)) {
            controller.handSeen(table, player, cards);
        }
    }

    @Override
    public void shows(PokerTable table, PokerPlayer player, String hand) {
        for(ActionController callback : included(table)) {
            callback.shows(table, player, hand);
        }
    }

    @Override
    public void startRound(PokerTable table) {
        for (ActionController controller : included(table)) {
            controller.startRound(table);
        }
    }

    @Override
    public void endRound(PokerTable table) {
        for(ActionController controller : included(table)) {
            controller.endRound(table);
        }
    }

}
