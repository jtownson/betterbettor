package net.jtownson.poker.controller;

import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;

import java.util.Date;

/**
 *
 */
public class DefaultActionController implements ActionController {

    private GameTypeFilter gameTypeFilter;
    private HandFilter handFilter;

    public static GameTypeFilter anyGameType = gameType -> true;

    public static HandFilter anyHand = (table, handId) -> true;

    public DefaultActionController() {
        this(anyGameType, anyHand);
    }

    public DefaultActionController(GameTypeFilter gameTypeFilter) {
        this(gameTypeFilter, anyHand);
    }

    public DefaultActionController(HandFilter handFilter) {
        this(anyGameType, handFilter);
    }

    public DefaultActionController(GameTypeFilter gameTypeFilter, HandFilter handFilter) {
        this.gameTypeFilter = gameTypeFilter;
        this.handFilter = handFilter;
    }

    GameTypeFilter getGameTypeFilter() {
        return gameTypeFilter;
    }

    HandFilter getHandFilter() {
        return handFilter;
    }

    @Override
    public boolean acceptGame(PokerTable table, GameType gameType) {
        return gameTypeFilter.acceptGameType(gameType);
    }

    @Override
    public boolean acceptHand(PokerTable table, String handId) {
        return handFilter.acceptHand(table, handId);
    }

    @Override
    public void errorCondition(PokerTable table, String userMessage, ErrorType errorType) {}

    @Override
    public void startGame(PokerTable table) {}

    @Override
    public void endGame(PokerTable table) {}

    @Override
    public void takesSeat(PokerTable table, PokerPlayer player) {}

    @Override
    public void dealIn(PokerTable table, PokerPlayer player) {}

    @Override
    public void sitsOut(PokerTable table, PokerPlayer player) {}

    @Override
    public void startHand(PokerTable table, String handId, Date startDateTime) {}

    @Override
    public void endHand(PokerTable table) {}

    @Override
    public void startRound(PokerTable table) {}

    @Override
    public void endRound(PokerTable table) {}

    @Override
    public void postSmallBlind(PokerTable table, PokerPlayer player, double amount) {}

    @Override
    public void postBigBlind(PokerTable table, PokerPlayer player, double amount) {}

    @Override
    public void folds(PokerTable table, PokerPlayer player) {}

    @Override
    public void checks(PokerTable table, PokerPlayer player) {}

    @Override
    public void calls(PokerTable table, PokerPlayer player) {}

    @Override
    public void calls(PokerTable table, PokerPlayer player, double amount) {}

    @Override
    public void bets(PokerTable table, PokerPlayer player, double amount) {}

    @Override
    public void raises(PokerTable table, PokerPlayer player, double raiseAmount) {}

    @Override
    public void allIn(PokerTable table, PokerPlayer player, double betAmount) {}

    @Override
    public void allIn(PokerTable table, PokerPlayer player) {}

    @Override
    public void dealsFlop(PokerTable table, String flop) {}

    @Override
    public void dealsTurn(PokerTable table, String turn) {}

    @Override
    public void dealsRiver(PokerTable table, String river) {}

    @Override
    public void handSeen(PokerTable table, PokerPlayer player, String cards) {}

    @Override
    public void shows(PokerTable table, PokerPlayer player, String hand) {}
}
