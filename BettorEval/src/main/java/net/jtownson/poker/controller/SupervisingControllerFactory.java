/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.controller;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import static java.lang.reflect.Proxy.newProxyInstance;

/**
 * Apply all game actions to the model
 * then pass those actions onto the view.
 *
 * The view updating action is loosely defined and could
 * in practice be any ActionController so, in theory, the
 * view could be a database, log file or some other fairly
 * abstract representation of the action.
 *
 * Impl uses a JDK dynamic proxy, just to avoid duplicating
 * <code>
 *     try {
    *     modelController.action
 *     } finally {
 *         viewController.action
 *     }
 * </code>
 * for every action, which would be dull.
 *
 * Created by jtownson on 21/01/14.
 */
public class SupervisingControllerFactory implements InvocationHandler {

    private final ModelUpdatingActionController modelUpdatingController;
    private final ActionController viewUpdatingController;

    public SupervisingControllerFactory(
            ModelUpdatingActionController modelUpdatingController,
            ActionController viewUpdatingController) {
        this.modelUpdatingController = modelUpdatingController;
        this.viewUpdatingController = viewUpdatingController;
    }

    public static ActionController SupervisingActionController(
            ModelUpdatingActionController modelUpdatingController,
            ActionController viewUpdatingController) {

        return (ActionController)newProxyInstance(
                ActionController.class.getClassLoader(),
                new Class<?>[]{ActionController.class},
                new SupervisingControllerFactory(modelUpdatingController, viewUpdatingController));
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        try {
            return method.invoke(modelUpdatingController, args);
        } finally {
            method.invoke(viewUpdatingController, args);
        }
    }
}
