/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

import net.jtownson.poker.controller.GameType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 *
 */
public class PokerTable {

    private String tableId;
    private String handId;
    private String herosName;
    private GameType gameType;
    private final List<Pot> pots = new ArrayList<>();
    private Dealer dealer;
    private Players players;
    private String flop;
    private String turn;
    private String river;
    private String boardCards;
    private EnumRound currentRound;

    public PokerTable() {
        this(new Players(), null);
    }

    public PokerTable(String herosName) {
        this(new Players(), null);
        this.herosName = herosName;
    }

    public PokerTable(Players players) {
        this(players, null);
    }

    private PokerTable(Players players, GameType gameType) {
        this.players = players;
        this.gameType = gameType;
        dealer = new Dealer(this);
        tableId = String.valueOf(UUID.randomUUID());
    }

    public String getTableId() {
        return tableId;
    }

    public void setTableId(String tableId) {
        this.tableId = tableId;
    }

    public String getHandId() {
        return handId;
    }

    public void setHandId(String handId) {
        this.handId = handId;
    }

    public String getHerosName() {
        return herosName;
    }

    public void setHerosName(String herosName) {
        this.herosName = herosName;
    }

    public void nextRound() {
        currentRound = currentRound.next();
    }

    public EnumRound getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(EnumRound round) {
        this.currentRound = round;
    }

    public void flopIs(String flop) {
        this.flop = flop.trim();
        boardCards = null;
    }

    public String getFlop() {
        return flop;
    }

    public void turnIs(String turn) {
        this.turn = turn.trim();
        boardCards = null;
    }

    public String getTurn() {
        return turn;
    }

    public void riverIs(String river) {
        this.river = river.trim();
        boardCards = null;
    }

    public String getRiver() {
        return river;
    }

    public String getBoardCards() {

        if (boardCards == null) {
            StringBuilder builder = new StringBuilder();
            if (StringUtils.isNotBlank(flop)) {
                builder.append(flop);

                if (StringUtils.isNotBlank(turn)) {
                    builder.append(' ');
                    builder.append(turn);

                    if (StringUtils.isNotBlank(river)) {
                        builder.append(' ');
                        builder.append(river);
                    }
                }
            }
            boardCards = builder.toString().trim();
        }
        return boardCards;
    }

    public void setBoardCards(String boardCards) {

        String flop = "";
        String turn = "";
        String river = "";
        String[] cards = boardCards.split("\\s+");
        for (int i = 0; i < cards.length; i++) {
            if ("".equals(cards[i].trim())) {
                continue;
            }
            if (i < 3) {
                flop += " " + cards[i];
            } else if (i == 3) {
                turn = cards[i];
            } else if (i == 4) {
                river = cards[i];
            }
        }
        this.flop = flop;
        this.turn = turn;
        this.river = river;
    }

    public List<Pot> getPots() {
        return pots;
    }

    public Pot getMainPot() {
        return pots.get(0);
    }

    public Players getPlayers() {
        return players;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public Dealer getDealer() {
        return dealer;
    }

    /**
     * Provide a players overall winnings in a hand.
     * @return the sum of all winnings in all pots.
     */
    public Map<String, Double> getPlayersWinnings() {
        return dealer.getWinningsInHand();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PokerTable that = (PokerTable) o;

        return new EqualsBuilder().append(this.tableId, that.tableId).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(tableId).toHashCode();
    }
}
