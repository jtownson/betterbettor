/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class Pot {

    private String potName;
    private Set<PokerPlayer> playersIn = new HashSet<>();
    private double potValue;

    public Pot() {
        this("Main pot");
    }

    public Pot(String potName) {
        this.potName = potName;
    }

    public Set<PokerPlayer> getPlayersIn() {
        return playersIn;
    }

    public double getPotValue() {
        return potValue;
    }

    public void addToPot(Double amount) {
        potValue += amount;
    }

    public String getPotName() {
        return potName;
    }

    public void setPotName(String potName) {
        this.potName = potName;
    }

    @Override
    public String toString() {
        return "Pot{" +
                "potName='" + potName + '\'' +
                ", potValue=" + potValue +
                '}';
    }
}
