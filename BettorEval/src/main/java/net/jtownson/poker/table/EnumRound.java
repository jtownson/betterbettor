/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

/**
 *
 */
public enum EnumRound {

    //unknown(-1),
    blinds(0),
    preFlop(1),
    flop(2),
    turn(3),
    river(4);

    private EnumRound(int roundNumber) {
        this.roundNumber = roundNumber;
    }

    private int roundNumber;

    public int getRoundNumber() {
        return roundNumber;
    }

    public EnumRound next() {
        return forCode( (roundNumber+1) % 5);
    }

    private static EnumRound forCode(int roundNumber) {
        for (EnumRound enumRound : values()) {
            if (enumRound.roundNumber == roundNumber) {
                return enumRound;
            }
        }
        throw new IllegalStateException("There is no betting round with code " + roundNumber);
    }
}
