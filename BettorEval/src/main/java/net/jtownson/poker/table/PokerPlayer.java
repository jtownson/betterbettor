/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

import ca.ualberta.cs.poker.Hand;
import net.jtownson.poker.oddsandequity.HandWeights;

import java.util.Arrays;

/**
 *
 */
public class PokerPlayer {

    private static final String UNKNOWN_HAND = "XxXxXxXxX";
    private static final String UNKOWN_POCKET_CARDS = "XxXx";
    private final String name;
    private int seatNumber;
    private double stack;
    private double betsMade;
    private double winningsInHand;
    private boolean hasFolded = true;
    private boolean hasSatOut = true;
    private boolean isDealtIn = false;
    private boolean isAllIn;
    private String showDownHand = UNKNOWN_HAND;
    private String pocketCards = UNKOWN_POCKET_CARDS;
    private HandWeights handWeights;

    public PokerPlayer(String name) {
        this(name, 0.0, 0);
    }

    public PokerPlayer(String name, double stack) {
        this(name, stack, 0);
    }

    public PokerPlayer(String name, int seatNumber) {
        this(name, 0.0, seatNumber);
    }

    public PokerPlayer(String name, double stack, int seatNumber) {
        this.name = name;
        this.stack = stack;
        this.seatNumber = seatNumber;
        this.handWeights = new HandWeights();
    }

    public String getName() {
        return name;
    }

    public double getStack() {
        return stack;
    }

    public int getSeatNumber() {
        return seatNumber;
    }

    public void setSeatNumber(int seatNumber) {
        this.seatNumber = seatNumber;
    }

    public void setStack(double stack) {
        this.stack = stack;
    }

    public double getBetsMade() {
        return betsMade;
    }

    public HandWeights getHandWeights() {
        return handWeights;
    }

    public void putIn(double amount) {
        this.betsMade += amount;
        stack -= amount;
    }

    public void giveFromBetsToDealer(double amount) {
        this.betsMade -= amount;
    }

    public boolean hasFolded() {
        return hasFolded;
    }

    public void foldAndMuck() {
        this.hasFolded = true;
    }

    public boolean isAllIn() {
        return isAllIn;
    }

    public void allIn() {
        isAllIn = true;
    }

    public void readyForNextHand() {
        betsMade = 0;
        winningsInHand = 0;
        showDownHand = UNKNOWN_HAND;
        pocketCards = UNKOWN_POCKET_CARDS;
        isDealtIn = true;
        hasFolded = false;
        isAllIn = false;
    }

    public boolean hasSatOut() {
        return hasSatOut;
    }

    public boolean isDealtIn() {
        return isDealtIn;
    }

    public void dealIn() {
        hasSatOut = false;
        isDealtIn = true;
        hasFolded = false;
    }

    public void dealOut() {
        hasSatOut = false;
        isDealtIn = false;
        hasFolded = true;
    }

    public void sitOut() {
        hasSatOut = true;
        isDealtIn = false;
        hasFolded = true;
    }

    public void sitIn() {
        hasSatOut = false;
        isDealtIn = false;
        hasFolded = true;
    }

    public boolean isHandShownDown() {
        return ! UNKNOWN_HAND.equals(showDownHand);
    }

    public String getShowDownHand() {
        return showDownHand;
    }

    public void setShowDownHand(String showDownHand) {
        this.showDownHand = showDownHand;
    }

    public boolean isHandKnown() {
        return ! UNKOWN_POCKET_CARDS.equals(pocketCards);
    }

    public String getPocketCards() {
        return pocketCards;
    }

    public void setPocketCards(String pocketCards) {
        this.pocketCards = pocketCards;
        setHandWeights();
    }

    private void setHandWeights() {
        Hand hand = new Hand(pocketCards);
        double normalization = handWeights.getNormalization();
        handWeights = HandWeights.handBasedWeights(Arrays.asList(hand));
        handWeights.normalizeTo(normalization);
    }

    public boolean isInHand() {
        return ! (hasFolded() || hasSatOut());
    }

    public double getWinningsInHand() {
        return winningsInHand;
    }

    public void setWinningsInHand(double winningsInHand) {
        this.winningsInHand = winningsInHand;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || ! (obj instanceof PokerPlayer)) {
            return false;
        }
        PokerPlayer rhs = (PokerPlayer)obj;
        return name.equals(rhs.name);
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder(name + " (seat " + seatNumber + ", ");
        if (hasFolded) {
            sb.append("folded");
        } else if (hasSatOut) {
            sb.append("sat out");
        } else {
            sb.append(String.format("%.2f, %.2f", betsMade, stack));
        }
        if ( ! pocketCards.equals(UNKOWN_POCKET_CARDS)) {
            sb.append(", " + pocketCards);
        }
        if ( ! showDownHand.equals(UNKNOWN_HAND)) {
            sb.append(", " + showDownHand);
        }
        sb.append(')');
        return sb.toString();
    }
}
