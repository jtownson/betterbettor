/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

/**
 * A list of players at the table.
 * Players can be added in any order.
 * Once all players have been added, call setDealerForRound.
 * This will sort the players by their seat numbers. The small blind
 * will be inferred and moved to position 0.
 */
public class Players implements Iterable<PokerPlayer> /*extends ArrayList<PokerPlayer>*/ {

    private ArrayList<PokerPlayer> playerList = new ArrayList<>();

    private int iLastToAct = -1;
    private int iNextToAct = -1;

    public Players() {}

    public Players(PokerPlayer... players) {
        this(Arrays.asList(players));
    }

    public Players(Collection<? extends PokerPlayer> players) {
        for(PokerPlayer player : players) {
            insert(player);
        }
    }

    @Override
    public Iterator<PokerPlayer> iterator() {
        return playerList.iterator();
    }

    @Override
    public void forEach(Consumer<? super PokerPlayer> action) {
        playerList.forEach(action);
    }

    @Override
    public Spliterator<PokerPlayer> spliterator() {
        return playerList.spliterator();
    }

    /**
     * Returns the player before player, regardless of folds,
     * but taking account of those sat out.
     * @param player
     * @return
     */
    public PokerPlayer getPlayerBefore(PokerPlayer player) {
        verifyNotFoldedOrSatout(player);
        List<PokerPlayer> notSatOut = getNotSatOut();
        int i = notSatOut.indexOf(player);
        return i == 0 ? null : notSatOut.get(i-1);
    }

    /**
     * Returns the player after player, regardless of folds,
     * but taking account of those sat out.
     * @param player
     * @return
     */
    public PokerPlayer getPlayerAfter(PokerPlayer player) {
        verifyNotFoldedOrSatout(player);
        List<PokerPlayer> notSatOut = getNotSatOut();
        int i = notSatOut.indexOf(player);
        return i == notSatOut.size()-1 ? null : notSatOut.get(i+1);
    }

    /**
     * The small blind. setDealerForRound must have been invoked for the result to
     * be defined.
     * @return
     */
    public PokerPlayer getSmallBlind() {
        List<PokerPlayer> notSatOut = getNotSatOut();
        if ( ! notSatOut.isEmpty()) {
            return notSatOut.get(0);
        }
        throw new IllegalStateException("There need to be some players in the hand.");
    }

    /**
     * The big blind. setDealerForRound must have been invoked for the result to
     * be defined.
     * @return
     */
    public PokerPlayer getBigBlind() {
        List<PokerPlayer> notSatOut = getNotSatOut();
        if ( ! notSatOut.isEmpty()) {
            return notSatOut.get(1);
        }
        throw new IllegalStateException("There need to be some players in the hand.");
    }

    /**
     * The player on the button. setDealerForRound must have been invoked for the result to
     * be defined.
     * @return
     */
    public PokerPlayer getButton() {
        List<PokerPlayer> notSatOut = getNotSatOut();
        if (notSatOut.size() == 2) {
            return notSatOut.get(0); // sb
        } else if ( ! notSatOut.isEmpty()) {
            return notSatOut.get(notSatOut.size()-1); // last player
        }
        throw new IllegalStateException("There need to be some players in the hand.");
    }

    /**
     * Get the player with the given name.
     * @param name the players name/id
     * @return null if there is not player with this name.
     */
    public PokerPlayer getByName(String name) {
        int i = indexOf(name);
        if (i != -1) {
            return playerList.get(i);
        }
        return null;
    }

    /**
     * @return the list index of player
     */
    public int indexOf(PokerPlayer player) {
        return indexOf(player.getName());
    }

    /**
     * @return the list index of player with name playerName
     */
    public int indexOf(String playerName) {
        for (int i = 0; i < playerList.size(); ++i) {
            if (playerList.get(i).getName().equals(playerName)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * This method declares that a player has taken their turn
     * and moves the cursor for the next to act.
     * @param player
     */
    public void hasActed(PokerPlayer player) {
        int i = playerList.indexOf(player);
        if (i == -1) {
            throw new IllegalArgumentException(
                    String.format("Player %s cannot act because he is not in this group of players.",
                            player.getName()));
        }
        iLastToAct = i;
        setNextToAct(++i % playerList.size(), 0);
    }

    public int size() {
        return playerList.size();
    }

    public PokerPlayer getLastToAct() {
        if (iLastToAct == -1) {
            return null;
        }
        return playerList.get(iLastToAct);
    }

    public void insert(PokerPlayer player) {
        if (playerList.size() == 0) {
            player.setSeatNumber(1);
            playerList.add(player);
            iLastToAct = 0;
            iNextToAct = 0;
        } else {
            PokerPlayer lastToAct = getLastToAct();
            if (lastToAct == null) {
                insertAfter(playerList.get(playerList.size() - 1), player);
            } else {
                insertAfter(lastToAct, player);
                reassignSeats();
            }
            hasActed(player);
        }
    }

    public void add(PokerPlayer player) {
        insert(player);
    }

    public boolean contains(PokerPlayer player) {
        return getByName(player.getName()) != null;
    }

    public PokerPlayer get(int i) {
        return playerList.get(i);
    }

    public void insertAfter(PokerPlayer after, PokerPlayer player) {

        int i = playerList.indexOf(after);
        if (i == -1) {
            throw new IllegalArgumentException();
        }

        int insertAt = i+1;
        playerList.add(insertAt, player);

        reassignSeats();
    }

    private void reassignSeats() {
        int initialSeat = 1;
        for (PokerPlayer p : playerList) {
            p.setSeatNumber(initialSeat++);
        }
    }

    /**
     * Get the next player to act. Folded or sat out players
     * are never returned.
     * @return
     */
    public PokerPlayer getNextToAct() {

        if (iNextToAct == -1 || getActiveInHand().size() == 0) {
            return null;
        }

        PokerPlayer p = get(iNextToAct);
        if (p.hasFolded() || p.hasSatOut() || p.isAllIn()) {
            iNextToAct = (iNextToAct + 1) % size();
            p = getNextToAct();
        }
        return p;
    }

    /**
     * Query the players who are in the hand ahead of the given player.
     * @param player this player must not have folded or sat out.
     * @return the players ahead who have bet, called or raised but not folded
     */
    public List<PokerPlayer> getPlayersInAhead(PokerPlayer player) {
        verifyNotFoldedOrSatout(player);
        List<PokerPlayer> unfolded = getUnfolded();
        return unfolded.subList(0, unfolded.indexOf(player));
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (PokerPlayer player : playerList) {
            if ( ! player.hasSatOut()) {
                sb.append(player);
                sb.append('\n');
            }
        }
        return sb.toString();
    }

    /**
     * Query the players who are yet to act behind the given player.
     * @param player this player must not have folded or sat out.
     * @return the players to act behind.
     */
    public List<PokerPlayer> getPlayersInBehind(PokerPlayer player) {
        verifyNotFoldedOrSatout(player);
        List<PokerPlayer> unfolded = getUnfolded();
        return unfolded.subList(unfolded.indexOf(player) + 1, unfolded.size());
    }

    /**
     * Query the players who are yet to act behind. This is the same
     * as getPlayersInBehind except that all-in players are also
     * filtered out (i.e. those all-in are still in but not active)
     */
    public List<PokerPlayer> getPlayersToActBehind(PokerPlayer player) {
        verifyNotFoldedSatoutOrAllIn(player);
        List<PokerPlayer> active = getActiveInHand();
        return active.subList(active.indexOf(player) + 1, active.size());
    }

    /**
     * Utility method to call readyForNextHand on each player.
     */
    public void setReadyForNextHand() {
        playerList.removeAll(getSatOut());
        for (PokerPlayer player : getDealtIn()) {
            player.readyForNextHand();
        }
        clearActionCursors();
    }

    public void dealInEverybody() {
        for (PokerPlayer player : playerList) {
            player.dealIn();
        }
    }

    /**
     * This initializes the players order and is needed
     * before playing a round.
     * Rotate the players around so that iteration begins
     * with the small blind.
     * @param dealerName id for the button player.
     */
    public void setDealerForRound(String dealerName) {

        Collections.sort(playerList, getSeatNumberComparator());

        int iSb = activeIndexAfter(dealerName, 0);
        if (getDealtIn().size() == 2) {
            iSb = indexOf(dealerName);
        }

        if (iSb == -1) {
            throw new IllegalArgumentException(
                    "You cannot assign a player named " + dealerName +
                    " to the button because he is not sitting at this table.");
        }
        pivotOn(iSb);

        iNextToAct = 0;
    }

    public void clear() {
        playerList.clear();
        clearActionCursors();
    }
    private void verifyNotFoldedOrSatout(PokerPlayer player) {
        if (player.hasFolded() || player.hasSatOut()) {
            throw new IllegalArgumentException("Player " + player.getName() + " is not in the hand.");
        }
    }

    private void verifyNotFoldedSatoutOrAllIn(PokerPlayer player) {
        if (player.hasFolded() || player.hasSatOut() || player.isAllIn()) {
            throw new IllegalArgumentException("Player " + player.getName() + " is not active in the hand.");
        }
    }

    private int indexAfter(String playerName) {
        int iPlayer = indexOf(playerName);
        if (iPlayer == playerList.size() - 1) {
            return 0;
        } else if (iPlayer == -1) {
            return -1;
        }
        return iPlayer + 1;
    }

    public void setDealerForRound(PokerPlayer player) {
        setDealerForRound(player.getName());
    }

    public ListIterator<PokerPlayer> unfoldedPlayerIterator() {
        return getUnfolded().listIterator();
    }

    public ListIterator<PokerPlayer> activePlayerIterator() {
        return getActiveInHand().listIterator();
    }

    private void setNextToAct(int i, int c) {

        if (c == playerList.size()) {
            iNextToAct = -1;
        } else {

            PokerPlayer p = playerList.get(i);

            if ( ! p.hasSatOut() && p.isDealtIn() && ! p.hasFolded() && ! p.isAllIn()) {
                iNextToAct = i;
            } else {
                setNextToAct(++i % playerList.size(), ++c);
            }
        }
    }

    private List<PokerPlayer> playerFilter(Predicate<PokerPlayer> predicate) {
        List<PokerPlayer> filtered = new ArrayList<>(playerList.size());
        for (PokerPlayer player : playerList) {
            if (predicate.test(player)) {
                filtered.add(player);
            }
        }
        return filtered;
    }

    public List<PokerPlayer> getActiveInHand() {
        return playerFilter(p -> ! (p.hasFolded() || p.hasSatOut() || p.isAllIn()));
    }

    public List<PokerPlayer> getUnfolded() {
        return playerFilter(p -> !p.hasFolded());
    }

    public List<PokerPlayer> getDealtIn() {
        return playerFilter(p -> p.isDealtIn());
    }

    public List<PokerPlayer> getNotSatOut() {
        return playerFilter(p -> !p.hasSatOut());
    }

    public List<PokerPlayer> getSatOut() {
        return playerFilter(p -> p.hasSatOut());
    }

    private int activeIndexAfter(String playerName, int c) {

        if (c == playerList.size()) {
            return -1;
        }

        int iPlayer = indexOf(playerName);

        if (iPlayer == -1) {
            throw new IllegalArgumentException(
                    String.format("There is no active player after %s because %s is not a player.", playerName, playerName));
        }

        int iNext;

        if (iPlayer == playerList.size() - 1) {
            iNext = 0;
        } else {
            iNext = iPlayer + 1;
        }

        PokerPlayer nextPlayer = playerList.get(iNext);
        if ( ! nextPlayer.isDealtIn()) {
            return activeIndexAfter(nextPlayer.getName(), c+1);
        }
        return iNext;
    }

    private void pivotOn(int iSb) {
        List<PokerPlayer> l = new ArrayList<>(playerList);
        playerList.clear();
        playerList.addAll(l.subList(iSb, l.size()));
        playerList.addAll(l.subList(0, iSb));
    }

    private Comparator<PokerPlayer> getSeatNumberComparator() {
        return (p1, p2) -> Integer.compare(p1.getSeatNumber(), p2.getSeatNumber());
    }

    private void clearActionCursors() {
        iLastToAct = -1;
        iNextToAct = -1;
    }

}
