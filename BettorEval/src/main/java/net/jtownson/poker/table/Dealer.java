/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

import ca.ualberta.cs.poker.Hand;
import ca.ualberta.cs.poker.HandEvaluator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.util.*;

/**
 *
 */
public class Dealer {

    private PokerTable table;

    public Dealer(PokerTable table) {
        this.table = table;
    }

    /**
     * Allocate bets players have made in the round into the pot(s).
     */
    public void gatherBets() {
        gatherBets(table.getPlayers(), table.getPots());
    }

    public void awardPots() {
        awardPots(table);
    }

    public double getAmountToCall() {
        return getHighestBet(table.getPlayers());
    }

    /**
     * Return a map with all winnings assigned to a player in the hand.
     * @return
     */
    public Map<String, Double> getWinningsInHand() {
        Players players = table.getPlayers();

        Map<String, Double> winnings = new HashMap<>(players.size());
        for (PokerPlayer player : players) {
            winnings.put(player.getName(), player.getWinningsInHand());
        }
        return winnings;
    }

    /**
     * Return the value of pots in which the player is involved, plus
     * any bets amounts infront in the current round (stricly speaking,
     * bets infront are not in the pot until the dealer has
     * gathered them at the end of the round).
     * @param player
     * @return
     */
    public double getPotValueForPlayer(PokerPlayer player) {
        // TODO use player's available stack for betsInFront (should fix chat parser stacks first).
        return existingPotValue(player) + betsInFront();
    }

    /**
     * Divide winnings between players in main and side pots.
     * @param table the table with cards on board (e.g. "Ac Kd 4s Jh 2d")
     */
    private void awardPots(PokerTable table) {

        List<Pot> pots = table.getPots();
        Players players = table.getPlayers();

        gatherBets(players, pots);

        for (Pot pot : pots) {

            Map<String, Double> playersEquities = getPlayersEquities(table.getBoardCards(), pot);

            double value = pot.getPotValue();

            for (String playerName : playersEquities.keySet()) {
                PokerPlayer player = players.getByName(playerName);
                double equity = playersEquities.get(playerName);
                double winningsInThisPot = equity * value;
                double currentStack = player.getStack();
                double winningsInOtherPots = player.getWinningsInHand();

                player.setWinningsInHand(winningsInOtherPots + winningsInThisPot);
                player.setStack(currentStack + winningsInThisPot);
            }
        }
    }

    private double betsInFront() {
        double betsInfront = 0;
        for (PokerPlayer p : table.getPlayers()) {
            betsInfront += p.getBetsMade();
        }
        return betsInfront;
    }

    private double existingPotValue(PokerPlayer player) {
        double existingValue = 0;
        List<Pot> pots = table.getPots();
        for (Pot pot : pots) {
            if (pot.getPlayersIn().contains(player)) {
                existingValue += pot.getPotValue();
            }
        }
        return existingValue;
    }

    private void gatherBets(Players players, List<Pot> pots) {

        double lowestBet = getLowestBet(players);

        if (pots.isEmpty()) {
            pots.add(new Pot());
        }

        while (lowestBet > 0) {

            Pot pot = pots.get(pots.size()-1);

            for (PokerPlayer player : players) {

                double bet = player.getBetsMade();

                if (bet > 0) {

                    // folded hands may have irregular uncalled amounts
                    double availableInFront = Math.min(bet, lowestBet);

                    player.giveFromBetsToDealer(availableInFront);

                    pot.addToPot(availableInFront);

                    if (player.isInHand()) {
                        pot.getPlayersIn().add(player);
                    }
                }
            }

            lowestBet = getLowestBet(players);
            if (lowestBet > 0) {
                pots.add(new Pot());
            }
        }

        setCorrectPotNames(pots);
    }

    /**
     * For determining whether the pot was won or tied.
     * A single player with 1.0 equity and all other with 0.0
     * indicates an outright winner. Otherwise equities are split
     * between equal hands.
     * @return a map of player name vs his equity in this pot
     */
    Map<String, Double> getPlayersEquities(String board, Pot pot) {

        List<PlayerHandRank> playersHandRanks = getSortedPlayersHandRanks(board, pot);

        Map<String, Double> playersEquities = new HashMap<>();

        int winners = populateEquities(playersHandRanks, playersEquities);

        normalizeToOne(playersEquities, winners);

        return playersEquities;
    }

    private List<PlayerHandRank> getSortedPlayersHandRanks(String board, Pot pot) {

        List<PlayerHandRank> playerHandRanks = new ArrayList<>();

        Iterator<PokerPlayer> pi = pot.getPlayersIn().iterator();
        while(pi.hasNext()) {
            PokerPlayer pp = pi.next();
            playerHandRanks.add(new PlayerHandRank(pp, board));
        }

        Collections.sort(playerHandRanks);

        return playerHandRanks;
    }

    private int populateEquities(
            List<PlayerHandRank> playersHandRanks, Map<String, Double> playersEquities) {

        PlayerHandRank leadingEntry = playersHandRanks.get(0);
        int winningRank = leadingEntry.handRank;
        PokerPlayer firstPlayer = leadingEntry.player;
        int winners = 1;

        playersEquities.put(firstPlayer.getName(), 1.0);

        for (int i = 1; i < playersHandRanks.size(); ++i) {

            PlayerHandRank playerHandRank = playersHandRanks.get(i);

            int nextRank = playerHandRank.handRank;
            PokerPlayer pp = playerHandRank.player;

            if (nextRank == winningRank) {
                playersEquities.put(pp.getName(), 1.0);
                ++winners;
            } else {
                playersEquities.put(pp.getName(), 0.0);
            }
        }
        return winners;
    }

    private void normalizeToOne(Map<String, Double> playersEquities, int winners) {
        Iterator<Map.Entry<String, Double>> pii = playersEquities.entrySet().iterator();

        while (pii.hasNext()) {
            Map.Entry<String, Double> pi = pii.next();
            double currentEquity = pi.getValue();
            pi.setValue(currentEquity / winners);
        }
    }

    private static class PlayerHandRank implements Comparable<PlayerHandRank> {
        private final int handRank;

        private final PokerPlayer player;

        PlayerHandRank(PokerPlayer player, String board) {
            if ( ! player.isInHand()) {
                handRank = 0;
            } else {
                if (player.isHandKnown()) {
                    handRank = HandEvaluator.rankHand(new Hand(player.getPocketCards() + " " + board));
                } else if (player.isHandShownDown()) {
                    handRank = HandEvaluator.rankHand(new Hand(player.getShowDownHand()));
                } else {
                    handRank = 1;   // sufficient when everybody else has folded or mucks at showdown.
                }
            }
            this.player = player;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(handRank).toHashCode();
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null || ! (obj instanceof  PlayerHandRank)) {
                return false;
            }
            PlayerHandRank rhs = (PlayerHandRank)obj;
            return this.handRank == rhs.handRank;
        }
        @Override
        public int compareTo(PlayerHandRank rhs) {
            return Integer.compare(rhs.handRank, this.handRank);
        }

    }

    private void setCorrectPotNames(List<Pot> pots) {

        if (pots.size() > 0) {

            if (StringUtils.isBlank(pots.get(0).getPotName())) {
                pots.get(0).setPotName("Main pot");
            }

            for (int i = 1; i < pots.size(); ++i) {
                pots.get(i).setPotName("Side pot " + i);
            }
        }
    }

    private static double getHighestBet(Players players) {
        double highestBet = 0;
        for (PokerPlayer player : players) {
            if (player.isInHand()) {
                double bet = player.getBetsMade();
                if (bet > highestBet) {
                    highestBet = bet;
                }
            }
        }
        return highestBet;
    }

    private static double getLowestBet(Players players) {
        double lowestBet = Double.MAX_VALUE;
        for (PokerPlayer player : players) {
            if (player.isInHand()) {
                double bet = player.getBetsMade();
                if (bet < lowestBet && bet != 0.0) {
                    lowestBet = bet;
                }
            }
        }
        return lowestBet == Double.MAX_VALUE ? 0.0 : lowestBet;
    }
}
