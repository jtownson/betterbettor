/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.table;

/**
 * Created by jtownson on 26/01/14.
 */
public class Situation {

    private final PokerPlayer player;
    private final int playersAhead;
    private final int playersBehind;
    private final double potSize;
    private final double amountToCall;
    private final double callEx;

    public Situation(PokerPlayer player, int playersAhead, int playersBehind, double potSize, double amountToCall, double callEx) {
        this.player = player;
        this.playersAhead = playersAhead;
        this.playersBehind = playersBehind;
        this.potSize = potSize;
        this.amountToCall = amountToCall;
        this.callEx = callEx;
    }

    public Situation(Situation s) {
        this.player = s.player;
        this.playersAhead = s.playersAhead;
        this.playersBehind = s.playersBehind;
        this.potSize = s.potSize;
        this.amountToCall = s.amountToCall;
        this.callEx = s.callEx;
    }

    public PokerPlayer getPlayer() {
        return player;
    }

    public int getPlayersAhead() {
        return playersAhead;
    }

    public int getPlayersBehind() {
        return playersBehind;
    }

    public double getPotSize() {
        return potSize;
    }

    public double getAmountToCall() {
        return amountToCall;
    }

    public double getCallEx() { return callEx; }

    @Override
    public String toString() {
        return String.format(
                "%s has to call %.2f for a pot of %.2f, with %s players in and %s players to act. Call expectation %s",
                player.getName(), amountToCall, potSize, playersAhead, playersBehind, callEx);
    }
}
