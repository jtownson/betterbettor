/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

import net.jtownson.poker.controller.GameType;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by jtownson on 16/01/14.
 */
public class GameTypeParser {

    public static final String FUN_MONEY = "FUN";

    private static String descPattern = "(.+?) ([PN]?L) (\\D?)(.+?)\\/\\D?(.+?)";
    private static Pattern gameDescPattern = Pattern.compile(descPattern);


    public static GameType GameType(String iPokerDesc) {
        return GameType(iPokerDesc, "");
    }

    public static GameType GameType(String iPokerGameDesc, String tableCurrency) {
        Matcher m = gameDescPattern.matcher(iPokerGameDesc);
        if (m.matches()) {
            String name = m.group(1);
            String limitType = m.group(2);
            String currencySymbol = m.group(3);
            double smallBlind = Double.valueOf(m.group(4));
            double bigBlind = Double.valueOf(m.group(5));
            String currency = getTableCurrency(currencySymbol, tableCurrency);

            return new GameType(name, currency, currencySymbol, limitType, smallBlind, bigBlind);
        } else {
            throw new IllegalStateException("Do not understand format of game type description " + iPokerGameDesc);
        }
    }

    private static String getTableCurrency(String currencySymbol, String declaredTableCurrency) {

        if (StringUtils.isBlank(currencySymbol)) {
            return FUN_MONEY;
        }
        return declaredTableCurrency;
    }

}
