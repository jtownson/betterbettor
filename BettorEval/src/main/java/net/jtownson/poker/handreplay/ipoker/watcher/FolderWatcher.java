package net.jtownson.poker.handreplay.ipoker.watcher;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

class FolderWatcher implements Runnable {

    private WatchKey watchKey = null;
    private ChangeListener listener = null;

    protected FolderWatcher(Path path, ChangeListener changeListener, WatchService watchService) throws IOException {
        watchKey = path.register(watchService, changeListener.getEventTypes());
        listener = changeListener;
    }

    @Override
    public void run() {
        while (!cancel) {
            for (WatchEvent anEvent : watchKey.pollEvents()) {
                listener.onEvent((WatchEvent<Path>) anEvent);
            }
        }
    }

    private boolean cancel = false;

    protected void cancel() {
        this.cancel = true;
    }
}
