/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.controller.GameReplayer;
import net.jtownson.poker.controller.GameType;
import net.jtownson.poker.handreplay.ipoker.binding.*;
import net.jtownson.poker.table.Players;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.*;

import static net.jtownson.poker.handreplay.ipoker.GameTypeParser.GameType;

/**
 * Read iPoker history files and callback events to a ActionController.
 * Note to implementors of callbacks: this impl filters out everything
 * except no-limit holdem games anyhow, because it doesn't properly support
 * other types. However, you can do further filtering for particular players
 * or tables with certain nunbers of players in the callback.
 */
public class IPokerHistoryGamePlayer implements GameReplayer {

    public static final String IPOKER_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    private LinkedList<Session> sessions;
    private ListIterator<Session> sessionIterator;
    private List<String> parseErrors;
    private PokerTable table;

    /**
     * Replay either a single history file or a directory of files.
     *
     * @param historyFileOrDir
     */
    public IPokerHistoryGamePlayer(Path historyFileOrDir, PokerTable table) {

        checkFileExistsAndAllThat(historyFileOrDir);
        this.table = table;

        final List<Session> sessions = new ArrayList<>();
        final List<String> errors = new ArrayList<>();

        walkAndParseTree(historyFileOrDir, sessions, errors);

        sortSessionsByDate(sessions);

        this.sessions = new LinkedList<>(sessions);
        this.parseErrors = errors;
    }

    public PokerTable getTable() {
        return table;
    }

    public List<String> getParseErrors() {
        return parseErrors;
    }


    @Override
    public void start(ActionController actionCallback) {

        Iterator<Session> si = getSessionIterator();
        while (si.hasNext()) {
            Session session = si.next();
            replaySession(session, actionCallback);
        }
    }

    @Override
    public void stop() {
        throw new UnsupportedOperationException(
                "Only really applicable to multi-threaded, ui players.");
    }

    @Override
    public void rewind(int numberOfHands) {
        throw new UnsupportedOperationException(
                "Only really applicable to multi-threaded, ui players.");
    }

    @Override
    public void forwind(int numberOfHands) {
        throw new UnsupportedOperationException(
                "Only really applicable to multi-threaded, ui players.");
    }

    private Iterator<Session> getSessionIterator() {
        if (sessionIterator == null) {
            sessionIterator = sessions.listIterator();
        }
        return sessionIterator;
    }

    // iplayer confusingly refers to a game of poker (i.e. some players
    // sitting at a table playing some hands) as a 'session', whilst
    // a hand is referred to as a 'game'.
    private void replaySession(Session session, ActionController actionCallback) {

        GameType gameType = getGameType(session);

        PokerTable table = getTable(gameType);
        Players players = table.getPlayers();

        if ( ! actionCallback.acceptGame(table, gameType)) {
            return;
        }

        actionCallback.startGame(table);

        try {
            List<Game> hands = session.getGame();

            for (Game hand : hands) {

                if (actionCallback.acceptHand(table, hand.getGamecode())) {

                    adaptPlayerList(hand.getGeneral().getPlayers().getPlayer(), players);

                    actionCallback.startHand(table, hand.getGamecode(), parseDate(hand.getGeneral().getStartdate()));

                    try {
                        replayHand(hand, table, actionCallback);
                    } catch(Exception e) {
                        actionCallback.errorCondition(table, String.format(
                                "Failed to replay game %s due to a parsing error or problem in the history file. " +
                                "Low-level exception message was %s.", hand.getGamecode(), e.getMessage()),
                                ActionController.ErrorType.parsingOrFormatError);
                    } finally {
                        actionCallback.endHand(table);
                    }
                }
            }
        } finally {
            actionCallback.endGame(table);
        }
    }

    private PokerTable getTable(GameType gameType) {
        if ( table.getGameType() == null || (! table.getGameType().equals(gameType))) {
            table.setGameType(gameType);
        }
        return table;
    }

    private void replayHand(Game game, PokerTable table, ActionController actionCallback) {

        List<Round> rounds = getRoundsInHand(game);

        for (Round round : rounds) {
            replayRound(round, table, actionCallback);
        }
    }

    private void replayRound(Round round, PokerTable table, ActionController actionCallback) {

        actionCallback.startRound(table);
        try {
            dealCards(round, table, actionCallback);
            replayBetting(round, table, actionCallback);
        } finally {
            actionCallback.endRound(table);
        }
    }

    private List<Cards> dealCards(Round round, PokerTable table, ActionController actionCallback) {

        List<Cards> cardsInRound = getCardsForRound(round);

        for (Cards cards : cardsInRound) {

            String cardStr = CardStringParser.parseIPokerCards(cards.getContent());

            switch(cards.getType()) {
                case POCKET:
                    if ( ! "X X".equals(cardStr)) {
                        PokerPlayer player = getPokerPlayerByName(cards.getPlayer(), table.getPlayers());
                        actionCallback.handSeen(table, player, cardStr);
                    }
                    break;
                case FLOP:
                    actionCallback.dealsFlop(table, cardStr);
                    break;
                case TURN:
                    actionCallback.dealsTurn(table, cardStr);
                    break;
                case RIVER:
                    actionCallback.dealsRiver(table, cardStr);
                    break;
            }
        }
        return cardsInRound;
    }

    private void replayBetting(Round round, PokerTable table, ActionController actionCallback) {

        Players players = table.getPlayers();

        // iPoker histories have actions in random order
        // (presumably some kind of optimisation when writing on the hoof).
        List<Action> actionsInRound = getActionsInRound(round);

        for (Action action : actionsInRound) {

            PokerPlayer player = getPokerPlayerByName(action.getPlayer(), players);
            EnumAction actionType = EnumAction.forCode(action.getType());
            double actionValue = getValue(action);
            switch (actionType) {
                case sitOut1:
                case sitOut2:
                    actionCallback.sitsOut(table, player);
                    break;
                case postSmallBlind:
                    actionCallback.postSmallBlind(table, player, actionValue);
                    break;
                case postBigBlind:
                    actionCallback.postBigBlind(table, player, actionValue);
                    break;
                case check:
                    actionCallback.checks(table, player);
                    break;
                case bet:  // need to handle side pots, etc.
                    actionCallback.bets(table, player, actionValue);
                    break;
                case fold:
                    actionCallback.folds(table, player);
                    break;
                case call:
                    actionCallback.calls(table, player, actionValue);
                    break;
                case raise: // need to handle side pots, etc.
                case allIn:
                    actionCallback.raises(table, player, actionValue);
                    break;
            }
        }
    }

    // parse Omaha PL $0.50/$1, Holdem NL $0.01/$0.02, etc
    private GameType getGameType(Session session) {
        String gameType = session.getGeneral().getGametype();
        String currencySymbol = session.getGeneral().getTablecurrency();
        return GameType(gameType, currencySymbol);
    }

    private double getValue(Action action) {
        return stripCurrency(action.getSum());
    }

    private double stripCurrency(String valueWithCurrentSymbols) {
        String numToParse = valueWithCurrentSymbols.replaceAll("[^\\d\\.]", "");
        return Double.parseDouble(numToParse);
    }

    private List<Round> getRoundsInHand(Game game) {
        List<Round> rounds = game.getRound();
        Collections.sort(rounds, getRoundNumberComparator());
        return rounds;
    }

    private List<Action> getActionsInRound(Round round) {

        List<Action> actions = new ArrayList<>();
        List<Object> actionOrCards = round.getActionOrCards();

        for (Object obj : actionOrCards) {
            if (obj instanceof Action) {
                actions.add((Action) obj);
            }
        }
        Collections.sort(actions, getActionNumberComparator());
        return actions;
    }

    private List<Cards> getCardsForRound(Round round) {

        List<Cards> cardsForRound = new ArrayList<Cards>();

        List<Object> actionOrCards = round.getActionOrCards();

        for (Object obj : actionOrCards) {
            if (obj instanceof Cards) {
                cardsForRound.add((Cards) obj);
            }
        }
        return cardsForRound;
    }

    private PokerPlayer getPokerPlayerByName(String name, Players players) {
        // avoid sorting to avoid breaking play order.
        for (PokerPlayer pokerPlayer : players) {
            if (pokerPlayer.getName().equals(name)) {
                return pokerPlayer;
            }
        }
        throw new IllegalStateException("There is no player called " + name + " sitting at this table.");
    }

    private void adaptPlayerList(List<Player> players, Players ring) {

        LinkedList<PokerPlayer> pokerPlayers = new LinkedList<>();
        String buttonName = null;
        for (Player player : players) {

            double stack = stripCurrency(player.getChips());
            String name = player.getName();
            int seat = player.getSeat();
            PokerPlayer pokerPlayer = new PokerPlayer(name, stack, seat);
            pokerPlayer.dealIn();
            if (player.isDealer()) {
                buttonName = pokerPlayer.getName();
            }

            pokerPlayers.add(pokerPlayer);
        }

        for (PokerPlayer pokerPlayer : pokerPlayers) {
            if ( ring.getByName(pokerPlayer.getName()) == null) {
                ring.insert(pokerPlayer);
            }
        }
        ring.setDealerForRound(buttonName);
    }

    private Comparator<Round> getRoundNumberComparator() {
        return (r1, r2) -> Integer.compare(r1.getNo(), r2.getNo());
    }

    private Comparator<Action> getActionNumberComparator() {
        return (a1, a2) -> Integer.compare(a1.getNo(), a2.getNo());
    }

    private void sortSessionsByDate(List<Session> sessions) {

        Comparator<Session> sessionComparator = (s1, s2) -> {
            Date d1 = parseDate(s1.getGeneral().getStartdate());
            Date d2 = parseDate(s2.getGeneral().getStartdate());
            return Long.compare(d1.getTime(), d2.getTime());
        };

        Collections.sort(sessions, sessionComparator);
    }

    private void walkAndParseTree(Path historyFileOrDir, final List<Session> sessions, final List<String> errors) {
        try {
            Files.walkFileTree(historyFileOrDir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    parseIPokerHandHistory(file, errors, sessions);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                    errors.add(exc.getMessage());
                    throw exc;
                }

            });
        } catch (IOException e) {
            throw new IllegalStateException(
                    "Got into a muddle parsing IPoker tree/file " + historyFileOrDir + " " + e.getMessage());
        }
    }

    private void checkFileExistsAndAllThat(Path historyFileOrDir) {
        if (!Files.exists(historyFileOrDir)) {
            throw new IllegalArgumentException(historyFileOrDir + " does not exist.");
        } else if (!(Files.isDirectory(historyFileOrDir) || Files.isRegularFile(historyFileOrDir))
                || !Files.isReadable(historyFileOrDir)) {
            throw new IllegalArgumentException(historyFileOrDir + " must be a readable directory or file.");
        }
    }

    private void parseIPokerHandHistory(Path file, List<String> errors, List<Session> sessions) {

        LowLevelIPokerParser parser = new LowLevelIPokerParser();
        try {
            InputStream is = Files.newInputStream(file, StandardOpenOption.READ);
            try {
                sessions.add(parser.parseIPokerHandHistory(is));
            } catch (IllegalStateException e) {
                errors.add(String.format(
                        "Failed to read history file %s due to parse exception %s", file, e.getMessage()));
            } finally {
                IOUtils.closeQuietly(is);
            }
        } catch (IOException e) {
            errors.add("Failed to read history file due to low-level IO error " + e.getMessage());
        }
    }

    static Date parseDate(String date) {
        try {
            return new SimpleDateFormat(IPOKER_DATE_FORMAT).parse(date);
        } catch(Exception e) {
            return new Date();
        }
    }
}
