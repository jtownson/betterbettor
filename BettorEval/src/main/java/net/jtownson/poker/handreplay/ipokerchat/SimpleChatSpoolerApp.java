/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import java.util.concurrent.BlockingQueue;

import static java.lang.System.*;

/**
 * Help collect test data for chat parser.
 * Created by jtownson on 23/01/14.
 */
public class SimpleChatSpoolerApp {

    public static void main(String[] args) {
        if (args.length == 0) {
            err.println("Usage: " + SimpleChatSpoolerApp.class.getSimpleName() + " <ipoker username>");
            exit(1);
        }
        String iPokerUsername = args[0];

        IPokerChatSpoolerProvider spoolerProvider = new IPokerChatSpoolerProvider(iPokerUsername);

        spoolerProvider.startSpooler();

        BlockingQueue<String> msgQueue = spoolerProvider.getMessageQueue();

        try {
            String msg = "";
            while ( ! IPokerChatProvider.TERMINATION_MESSAGE.equals(msg)) {

                out.println(msg);
                msg = msgQueue.take();
            }

        } catch (InterruptedException e) {

        } finally {
            spoolerProvider.stopSpooler();
        }
    }
}
