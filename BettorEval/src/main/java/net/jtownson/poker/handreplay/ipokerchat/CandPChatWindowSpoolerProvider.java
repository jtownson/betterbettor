package net.jtownson.poker.handreplay.ipokerchat;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Chat provider for ipoker chat copied and pasted from the chat window.
 *
 */
public class CandPChatWindowSpoolerProvider implements IPokerChatProvider {

    private static String redundantLeader = "Dealer: ";
    private List<String> chatText = new ArrayList<>();

    public CandPChatWindowSpoolerProvider(Path chatTextFile) throws IOException {
        List<String> rawLines = rawLines(chatTextFile);
        appendToChatList(rawLines);
    }

    public CandPChatWindowSpoolerProvider(List<String> chatText) {
        appendToChatList(chatText);
    }

    private void appendToChatList(List<String> rawLines) {
        for (String rawChat : rawLines) {
            rawChat = rawChat.trim();
            if ( ! StringUtils.isBlank(rawChat)) {
                chatText.add(rawChat.replace(redundantLeader, ""));
            }
        }
    }

    private List<String> rawLines(Path chatTextFile) throws IOException {
        FileInputStream fis = new FileInputStream(chatTextFile.toFile());
        try {
            List<String> chat = IOUtils.readLines(fis);
            chat.add(TERMINATION_MESSAGE);
            return chat;
        } finally {
            fis.close();
        }
    }

    @Override
    public void startSpooler() {

    }

    @Override
    public void stopSpooler() {

    }

    @Override
    public BlockingQueue<String> getMessageQueue() {
        return new ArrayBlockingQueue<>(chatText.size(), true, chatText);
    }

    @Override
    public BlockingQueue<String> getErrorQueue() {
        return new ArrayBlockingQueue<>(0);
    }
}
