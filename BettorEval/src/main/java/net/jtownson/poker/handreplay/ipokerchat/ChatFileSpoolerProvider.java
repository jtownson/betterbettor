/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedTransferQueue;

/**
 * Created by jtownson on 22/01/14.
 */
public class ChatFileSpoolerProvider  implements IPokerChatProvider {

    private BlockingQueue<String> chatQueue;
    private BlockingQueue<String> errorQueue;

    public ChatFileSpoolerProvider(Path chatFile) throws IOException {
        verifyFileExistsAndCanRead(chatFile);
        chatQueue = getChat(chatFile);
        errorQueue = new LinkedTransferQueue<>();
    }

    private BlockingQueue<String> getChat(Path chatFile) throws IOException {
        InputStream chatStream = new FileInputStream(chatFile.toFile());
        try {
            List<String> chat = IOUtils.readLines(chatStream, "UTF-8");
            return new LinkedBlockingDeque<>(chat);
        } finally {
            chatStream.close();
        }
    }

    private void verifyFileExistsAndCanRead(Path chatFile) {
        File f = chatFile.toFile();
        if ( ! (f.isFile() && f.canRead())) {
            throw new IllegalArgumentException(
                    String.valueOf(chatFile) + " is not a valid chat file.");
        }
    }

    @Override
    public void startSpooler() {

    }

    @Override
    public void stopSpooler() {

    }

    @Override
    public BlockingQueue<String> getMessageQueue() {
        return chatQueue;
    }

    @Override
    public BlockingQueue<String> getErrorQueue() {
        return errorQueue;
    }
}
