/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

import net.jtownson.poker.handreplay.ipoker.binding.Session;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import java.io.InputStream;

/**
 *
 */
public class LowLevelIPokerParser {

    /**
     * Read an iPoker history/session xml file.
     *
     * @param is a handle to the file
     * @return the top-level session object.
     * @throws IllegalStateException if the file parse files.
     */
    public Session parseIPokerHandHistory(InputStream is) {
        try {
            return unmarshal(is);
        } catch (JAXBException e) {
            throw new IllegalStateException(e);
        }
    }

    public Session unmarshal(InputStream inputStream) throws JAXBException {
        String packageName = Session.class.getPackage().getName();
        return (Session)JAXBContext.newInstance(packageName)
                .createUnmarshaller().unmarshal(inputStream);
    }
}
