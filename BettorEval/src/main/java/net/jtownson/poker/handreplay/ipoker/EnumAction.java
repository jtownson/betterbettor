/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

/**
 * Maps iPoker action type="3" codes to human readable actions.
 */
public enum EnumAction {

    unknown(-1),
    fold(0),
    postSmallBlind(1),
    postBigBlind(2),
    call(3),
    check(4),
    bet(5),
    allIn(7),
    sitOut1(8),
    sitOut2(9),
    raise(23);

    private EnumAction(int actionTypeCode) {
        this.actionTypeCode = actionTypeCode;
    }

    private int actionTypeCode;

    public static EnumAction forCode(int actionTypeCode) {
        for (EnumAction enumAction : values()) {
            if (enumAction.actionTypeCode == actionTypeCode) {
                return enumAction;
            }
        }
        return unknown;
    }
}
