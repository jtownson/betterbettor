/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 */
public class IPokerChatSpoolerProvider implements IPokerChatProvider {

    public static final String START_SPOOLER_SCRIPT = "StartIPokerSpooler.bat";
    public static final String STOP_SPOOLER_SCRIPT = "StopIPokerSpooler.bat";

    private LinkedBlockingQueue<String> chatList = new LinkedBlockingQueue<>();
    private LinkedBlockingQueue<String> errorList = new LinkedBlockingQueue<>();
    private ProcessBuilder processStarter;
    private ProcessBuilder processStopper;
    private Process iPokerSpookerProcess;
    private Thread stdErrReader;
    private Thread stdOutReader;

    public IPokerChatSpoolerProvider(String iPokerUsername) {
        processStarter = new ProcessBuilder("cmd.exe", "/c", START_SPOOLER_SCRIPT, iPokerUsername);
        processStarter.directory(getSpoolerExeDir());
        processStopper = new ProcessBuilder("cmd.exe", "/c", STOP_SPOOLER_SCRIPT);
        setEnvironment();
    }

    @Override
    public synchronized void startSpooler() {

        if (iPokerSpookerProcess != null) {
            return;
        }

        try {

            iPokerSpookerProcess = processStarter.start();

            stdErrReader = new StreamReaderThread(iPokerSpookerProcess.getErrorStream(), errorList);
            stdOutReader = new StreamReaderThread(iPokerSpookerProcess.getInputStream(), chatList);

            stdErrReader.start();
            stdOutReader.start();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public synchronized void stopSpooler() {
        try {
            processStopper.start();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public void joinReaderThread() throws InterruptedException {
        stdOutReader.join();
    }

    private void reset() {
        iPokerSpookerProcess = null;
        stdErrReader = null;
        stdOutReader = null;
    }

    @Override
    public BlockingQueue<String> getMessageQueue() {
        return chatList;
    }

    @Override
    public BlockingQueue<String> getErrorQueue() {
        return errorList;
    }

    class StreamReaderThread extends Thread
    {
        private InputStream is;
        private LinkedBlockingQueue<String> msgQueue;

        StreamReaderThread(InputStream is, LinkedBlockingQueue<String> msgQueue)
        {
            this.is = is;
            this.msgQueue = msgQueue;
        }

        public void run()
        {
            InputStreamReader isr = new InputStreamReader(is);
            try
            {
                BufferedReader br = new BufferedReader(isr);
                String line;
                while ( (line = br.readLine()) != null) {
                    msgQueue.offer(line);
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            } finally {
                msgQueue.offer(TERMINATION_MESSAGE);
                try {isr.close();} catch(Exception e) {}
            }
        }

    }

    private File getSpoolerExeDir() {
        URL exeDir = getClass().getResource(getClass().getSimpleName()+".class");
        try {
            return new File(exeDir.toURI()).getParentFile();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(
                    String.format(
                            "Error starting %s. Unable to locate %s. Got exception %s.",
                            this.getClass().getName(), processStarter.command(), e.getMessage()), e);
        }
    }

    private void setEnvironment() {
        Map<String, String> env = processStarter.environment();
        String pathKey = getPathKey(env);
        env.put(pathKey, getExeDirPath(pathKey));
    }

    private String getExeDirPath(String pathKey) {
        return System.getenv(pathKey) + ";" + getSpoolerExeDir().getPath() + ";.";
    }

    private String getPathKey(Map<String, String> env) {
        for(String key : env.keySet()) {
            if ("Path".equalsIgnoreCase(key)) {
                return key;
            }
        }
        return "Path";
    }
}
