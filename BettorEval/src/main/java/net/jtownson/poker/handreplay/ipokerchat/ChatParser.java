/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.controller.GameType;
import net.jtownson.poker.table.Players;
import net.jtownson.poker.table.PokerPlayer;
import net.jtownson.poker.table.PokerTable;
import org.apache.commons.lang.StringUtils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static net.jtownson.poker.handreplay.ipoker.GameTypeParser.GameType;

enum ChatParser {

    dealer("(.+?) is the dealer", false){
        void apply(ActionController callback, Matcher matcher) {
            // chat entries come as...
            // 47id47 is the dealer
            // Starting a new hand (#4548221415)
            // but we don't know if the dealer name applies until the
            // the filter is applied in startHand.
            String playerName = matcher.group(1);
            this.playerName = playerName;
            newHand.isDisabled = false;
        }},

    newHand("Starting a new hand \\(#(\\d+)\\)", true){

        void apply(ActionController callback, Matcher matcher) {

            callback.endHand(pokerTable);

            // if the hand is rejected, disable all the regex (except this one)
            // so chat is skipped until the next new hand
            String handId = matcher.group(1);
            if (callback.acceptHand(pokerTable, handId)) {
                setAllDisabledStatesExcept(dealer, false);
                getLazyPlayer(callback, dealer.playerName);
                pokerTable.getPlayers().setDealerForRound(dealer.playerName);
                callback.startHand(pokerTable, handId, new Date());
            } else {
                dealer.playerName = null;
                backupPattern = pattern;
                setAllDisabledStatesExcept(dealer, true);
            }
        }},

    winsHand("Game #\\d+: Winner is (.+?) ([^\\d]?)(\\d+.?\\d*)", true){
        void apply(ActionController callback, Matcher matcher) {
            callback.endRound(pokerTable);
            //callback.endHand(pokerTable);
        }},

    winsSidePot2("Game #\\d+: (.+?) wins the side pot [^\\d]?\\((\\d+.?\\d*)\\) with .+? - (.+)", true) {
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            String hand = matcher.group(3);

            PokerPlayer player = getLazyPlayer(callback, playerName);

            callback.shows(pokerTable, player, hand);
        }},

    winsSidePot("Game #\\d+: (.+?) wins the side pot [^\\d]?\\((\\d+.?\\d*)\\) with .+?", true) {
        void apply(ActionController callback, Matcher matcher) {

            String [] cards = getBoardCards(matcher, 5);

            if (cards != null) {
                popMatcherContext(5);

                String playerName = matcherContext.pop().group(1);
                String hand = StringUtils.join(cards, ' ');
                PokerPlayer player = getLazyPlayer(callback, playerName);

                callback.shows(pokerTable, player, hand);
            }
        }},

    winsMainPot2("Game #\\d+: (.+?) wins the main pot \\((\\d+.?\\d*)\\) with .+? - (.+)", true) {
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            String hand = matcher.group(3);
            PokerPlayer player = getLazyPlayer(callback, playerName);

            callback.shows(pokerTable, player, hand);
        }},

    winsMainPot("Game #\\d+: (.+?) wins the main pot [^\\d]?\\((\\d+.?\\d*)\\) with .+?", true) {
        void apply(ActionController callback, Matcher matcher) {

            String [] cards = getBoardCards(matcher, 5);

            if (cards != null) {
                popMatcherContext(5);

                String playerName = matcherContext.pop().group(1);
                String hand = StringUtils.join(cards, ' ');
                PokerPlayer player = getLazyPlayer(callback, playerName);

                callback.shows(pokerTable, player, hand);
                //callback.endRound(pokerTable);
                //callback.endHand(pokerTable);
            }
        }},

    smallBlind("(.+?) posts Small Blind ([^\\d]?)(\\d+.?\\d*)", true){
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            String currencySymbol = matcher.group(2);
            Double amount = parseDouble(matcher.group(3));
            this.playerName = playerName;

            guessGameType(currencySymbol, amount);

            PokerPlayer player = getLazyPlayer(callback, playerName);

            callback.postSmallBlind(pokerTable, player, amount);
        }},

    bigBlind("(.+?) posts Big Blind ([^\\d]?)(\\d+.?\\d*)", true){
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            Double amount = parseDouble(matcher.group(3));
            this.playerName = playerName;

            pokerTable.getGameType().setBigBlind(amount);

            PokerPlayer player = getLazyPlayer(callback, playerName);
            callback.postBigBlind(pokerTable, player, amount);
        }},

    call("(.+?) calls ([^\\d]?)(\\d+.?\\d*)", true){
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            Double amount = parseDouble(matcher.group(3));
            PokerPlayer player = getLazyPlayer(callback, playerName);
            callback.calls(pokerTable, player, amount);
        }},

    check("(.+?) checks", true){
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            PokerPlayer player = getLazyPlayer(callback, playerName);
            callback.checks(pokerTable, player);
        }},

    fold("(.+?) folds", true){
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            PokerPlayer player = getLazyPlayer(callback, playerName);
            callback.folds(pokerTable, player);
        }},

    sitOut("(.+?) has left the table", true) {
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            PokerPlayer player = pokerTable.getPlayers().getByName(playerName);
            if (player != null) {
                callback.sitsOut(pokerTable, player);
            }
        }},

    sitsIn("Player (.+?) has joined the table", true) {
        @Override
        void apply(ActionController callback, Matcher matcher) {
            // This event can happen at any time during a hand
            // so it breaks our assumption that we can just add
            // players lazily to the end of the player list, with
            // ascending seat numbers. Will thus require insertion
            // at the current player cursor and renumbering of seats.
            // Easier to ignore this event and add the player lazily
            // when they post their first blind.
            //String playerName = matcher.group(1);
            //PokerPlayer player = getLazyPlayer(callback, playerName);
            //player.dealOut();
            //pokerTable.getPlayers().setDealerForRound(dealer.playerName);
        }
    },

    bet("(.+?) bets ([^\\d]?)(\\d+.?\\d*)", true){
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            Double amount = parseDouble(matcher.group(3));
            PokerPlayer player = getLazyPlayer(callback, playerName);

            callback.bets(pokerTable, player, amount);
        }},

    raise("(.+?) raises to ([^\\d]?)(\\d+.?\\d*)", true){
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            PokerPlayer player = getLazyPlayer(callback, playerName);

            double raiseTo = parseDouble(matcher.group(3));

            callback.raises(pokerTable, player, raiseTo);
        }},

    allin("(.+?) goes All-in ([^\\d]?)(\\d+.?\\d*)", true) {
        void apply(ActionController callback, Matcher matcher) {

            String playerName = matcher.group(1);
            double betAmount = parseDouble(matcher.group(3));
            PokerPlayer player = getLazyPlayer(callback, playerName);

            callback.allIn(pokerTable, player, betAmount);
        }},

    dealing("Dealing cards", true){
        void apply(ActionController callback, Matcher matcher) {
            callback.endRound(pokerTable);
        }},

    yourcards("Your cards", true) {
        @Override
        void apply(ActionController callback, Matcher matcher) {

            String [] cards = getBoardCards(matcher, 2);

            if (cards != null) {
                String hand = cards[0] + " " + cards[1];
                String herosName = pokerTable.getHerosName();
                if (StringUtils.isNotBlank(herosName)) {
                    callback.handSeen(pokerTable, getLazyPlayer(callback, herosName), hand);
                }
            }
        }
    },


    flop("Dealing Flop", true){
        void apply(ActionController callback, Matcher matcher) {

            String [] board = getBoardCards(matcher, 3);
            if (board != null) {
                callback.endRound(pokerTable); // end of pre-flop
                String flop = board[0] + " " + board[1] + " " + board[2];
                callback.dealsFlop(pokerTable, flop);
            }
        }},

    turn("Dealing Turn", true){
        void apply(ActionController callback, Matcher matcher) {

            String [] board = getBoardCards(matcher, 4);
            if (board != null) {
                callback.endRound(pokerTable);
                callback.dealsTurn(pokerTable, board[3]);
            }
        }},

    river("Dealing River", true){
        void apply(ActionController callback, Matcher matcher) {

            String [] board = getBoardCards(matcher, 5);
            if (board != null) {
                callback.endRound(pokerTable);
                callback.dealsRiver(pokerTable, board[4]);
            }
        }};

    protected static final String cardRegex = "([\\dTJQKA][hcds])?\\s?";

    protected String iPokerMessage;
    protected String playerName;
    protected boolean isDisabled = true;
    protected List<String> cards;
    protected Pattern pattern;
    protected Pattern backupPattern;
    protected PokerTable pokerTable;
    protected Stack<Matcher> matcherContext = new Stack<>();

    ChatParser(String regex, boolean disabled) {
        pattern = Pattern.compile(regex);
        isDisabled = disabled;
    }

    // the best way to make the chat reader work is to set the player info
    // before starting it, however, in absence of this, it can infer the
    // players by lazily adding them as they perform actions in the hand
    // the first player to be identified in the hand it the button,
    // followed by the blinds. We can then insert extra players between
    // the blinds and the button as they call, bet or fold.
    // The only requirement that Players imposes on us here is that players
    // who act later in the hand have higher seat numbers.

    protected PokerPlayer getLazyPlayer(ActionController callback, String playerName) {

        Players players = pokerTable.getPlayers();

        PokerPlayer player = players.getByName(playerName);

        if (player == null && StringUtils.isNotBlank(playerName)) {
            player = new PokerPlayer(playerName);
            callback.takesSeat(pokerTable, player);
            callback.dealIn(pokerTable, player);
            players.setDealerForRound(dealer.playerName);
        } else if (player != null && ! player.isDealtIn()) {
            player.dealIn();
        }

        return player;
    }

    void guessGameType(String currencySymbol, double smallBlind) {

        double bigBlind = smallBlind * 2;

        if (currencySymbol == null) {
            currencySymbol = "";
        }

        // could start mapping $->USD, £->GBP, etc but is not one-to-one and not that important anyhow.
        String tableCurrency = "";

        GameType gameType = GameType(
                String.format("Holdem NL %s%s/%s%s", currencySymbol, smallBlind, currencySymbol, bigBlind),
                tableCurrency);

        pokerTable.setGameType(gameType);
    }

    // The flop/turn/river messages are on several lines...
    // Like this
    // Dealing Turn
    // 4h
    // Jd
    // Qs
    // Ah
    // and sometimes like this
    // Dealing Turn
    // 4h Jd Qs Ah
    // ...so swap the regex for a set number of lines (3, 4 or 5 according to the street)
    // and then revert back once all the cards have been read.
    String[] getBoardCards(Matcher matcher, int cardsExpected) {

        matcherContext.push(matcher);

        if (cards == null) {
            backupPattern = pattern;
            pattern = Pattern.compile(StringUtils.repeat(cardRegex, cardsExpected));
            cards = new ArrayList<>(cardsExpected);
        }
        else {
            if (cards.size() < cardsExpected) {

                if (matcher.groupCount() == cardsExpected) {
                    for (int i = 1; i <= cardsExpected; i++) {
                        String card = matcher.group(i);
                        if (card != null) {
                            cards.add(card);
                        } else {
                            break;
                        }
                    }
                } else {
                    throw new IllegalStateException(String.format(
                            "Flop/turn/river chat regex failed, for %s. Cards expected = %s",
                            matcher, cardsExpected));
                }
            }
            if (cards.size() == cardsExpected) {
                return arrayOfCardsAfterReset();
            }
        }
        return null;
    }

    Matcher popMatcherContext(int n) {
        for (int i = 0; i < n-1; i++) {
            matcherContext.pop();
        }
        return matcherContext.pop();
    }

    private String[] arrayOfCardsAfterReset() {
        String [] board = new String[cards.size()];
        cards.toArray(board);
        pattern = backupPattern;
        cards = null;
        return board;
    }

    private static double parseDouble(String s) {
        NumberFormat ukFormat = NumberFormat.getNumberInstance();
        try {
            return ukFormat.parse(s).doubleValue();
        } catch(ParseException e) {
            throw new IllegalArgumentException(s + " is not a parseable number.");
        }
    }

    boolean actionApplied(String chatMessage, ActionController callback) {
        if (isDisabled) {
            return false;
        }
        Matcher m = pattern.matcher(chatMessage);
        if (m.matches()) {
            iPokerMessage = chatMessage;
            apply(callback, m);
            return true;
        }
        return false;
    }

    static void reset() {
        setAllDisabledStatesExcept(dealer, true);
    }

    static void setAllDisabledStatesExcept(ChatParser initial, boolean disabledState) {
        for (ChatParser regex : values()) {
            if (regex != initial) {
                regex.isDisabled = disabledState;
            }
        }
    }

    abstract void apply(ActionController callback, Matcher matcher);
}
