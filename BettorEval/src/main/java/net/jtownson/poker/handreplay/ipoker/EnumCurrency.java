/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

/**
 *
 */
public enum EnumCurrency {

    GBP('\u00A3'),
    EUR('\u20AC'),
    USD('\u0024'),
    unknown('\u0023');

    private char symbol;

    private EnumCurrency(char symbol) {
        this.symbol = symbol;
    }

    public String value() {
        return name();
    }

    public static EnumCurrency fromValue(char c) {
        for (EnumCurrency enumCurrency : values()) {
            if (enumCurrency.symbol == c) {
                return enumCurrency;
            }
        }
        return unknown;
    }
}
