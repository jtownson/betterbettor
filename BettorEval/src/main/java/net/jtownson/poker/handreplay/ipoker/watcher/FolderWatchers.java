package net.jtownson.poker.handreplay.ipoker.watcher;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.WatchService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FolderWatchers {

    private static FolderWatchers watchers = null;
    private static WatchService watchService = null;
    private static final ExecutorService executor = Executors.newCachedThreadPool();

    private FolderWatchers() throws IOException {
        watchService = FileSystems.getDefault().newWatchService();
    }

    public static FolderWatchers getInstance() throws IOException {
        if (null == watchers) {
            watchers = new FolderWatchers();
        }
        return watchers;
    }

    public FolderWatcherFuture addFolderListener(Path path, ChangeListener changeListener) throws IOException {
        FolderWatcher aWatcher = new FolderWatcher(path, changeListener, watchService);
        Future f = executor.submit(aWatcher);
        FolderWatcherFuture future = new FolderWatcherFuture(aWatcher, f);
        return future;
    }

    public void cancelFolderWatching(FolderWatcherFuture folderWatcherFuture) {
        folderWatcherFuture.cancel(true);
    }
}
