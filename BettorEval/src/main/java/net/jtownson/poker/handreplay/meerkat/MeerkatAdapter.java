package net.jtownson.poker.handreplay.meerkat;

import com.biotools.meerkat.Action;
import com.biotools.meerkat.Card;
import com.biotools.meerkat.GameInfo;
import com.biotools.meerkat.Player;
import com.biotools.meerkat.util.Preferences;
import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.table.PokerTable;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Adapts meerkat's Player API to ActionController
 */
public class MeerkatAdapter implements Player {

    private static final Logger log = Logger.getLogger(MeerkatAdapter.class);

    private PokerTable table;
    private ActionController controller;
    private Preferences preferences;
    private GameInfo gameInfo;
    private int ourSeat;


    public MeerkatAdapter(PokerTable table, ActionController controller) {
        this.table = table;
        this.controller = controller;
    }

    @Override
    public void init(Preferences preferences) {
        //log.
        this.preferences = preferences;
    }

    @Override
    public void holeCards(Card card1, Card card2, int ourSeat) {
        this.ourSeat = ourSeat;
        controller.handSeen(table, table.getPlayers().getByName(table.getHerosName()), card1.toString() + card2.toString());
    }

    @Override
    public Action getAction() {
        double amountToCall = gameInfo.getAmountToCall(ourSeat);
        return Action.checkOrFoldAction(amountToCall);
    }

    @Override
    public void actionEvent(int i, Action action) {

    }

    @Override
    public void stageEvent(int i) {

    }

    @Override
    public void showdownEvent(int i, Card card, Card card2) {

    }

    @Override
    public void gameStartEvent(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
        controller.startGame(table);
    }

    @Override
    public void dealHoleCardsEvent() {

    }

    @Override
    public void gameOverEvent() {

    }

    @Override
    public void winEvent(int i, double v, String s) {

    }

    @Override
    public void gameStateChanged() {

    }

    /*
    public JPanel getSettingsPanel() {
        JPanel jp = new JPanel();
        final JCheckBox acMode = new JCheckBox("Always Call Mode", preferences.getBooleanPreference("ALWAYS_CALL_MODE"));
        acMode.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                preferences.setPreference("ALWAYS_CALL_MODE", acMode.isSelected());
            }
        });
        jp.add(acMode);
        return jp;
    }*/
}
