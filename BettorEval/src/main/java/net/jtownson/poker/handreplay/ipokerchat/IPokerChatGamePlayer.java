/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipokerchat;

import net.jtownson.poker.controller.ActionController;
import net.jtownson.poker.controller.GameReplayer;
import net.jtownson.poker.table.PokerTable;
import org.apache.log4j.Logger;
import org.apache.log4j.NDC;

import java.util.concurrent.BlockingQueue;

/**
 * Process iPoker chat window text returned from the IPokerSpooler
 * and replay to a ActionController
 */
public class IPokerChatGamePlayer implements GameReplayer {

    private static Logger log = Logger.getLogger(IPokerChatGamePlayer.class);

    private IPokerChatProvider chatProvider;
    private PokerTable table;

    public IPokerChatGamePlayer(IPokerChatProvider chatProvider, PokerTable table) {
        this.chatProvider = chatProvider;
        this.table = table;
    }

    public PokerTable getTable() {
        return table;
    }

    @Override
    public void start(ActionController callback) {

        ChatParser.reset();
        chatProvider.startSpooler();

        BlockingQueue<String> msgQueue = chatProvider.getMessageQueue();

        try {
            String msg = "";
            while ( ! IPokerChatProvider.TERMINATION_MESSAGE.equals(msg)) {

                logChatMessage(msg);
                handleActionMessage(msg, callback);
                msg = msgQueue.take();
            }

        } catch (InterruptedException e) {

        } finally {
            chatProvider.stopSpooler();
        }
    }

    @Override
    public void stop() {
        chatProvider.stopSpooler();
    }

    @Override
    public void rewind(int numberOfHands) {

    }

    @Override
    public void forwind(int numberOfHands) {

    }

    private void handleActionMessage(String msg, ActionController callback) {
        for (ChatParser chatParser : ChatParser.values()) {
            chatParser.pokerTable = table;
            if (chatParser.actionApplied(msg, callback)) {
                break;
            }
        }
    }

    private void logChatMessage(String msg) {
        NDC.push(table.getTableId());
        try {
            log.info(msg);
        } finally {
            NDC.pop();
        }
    }
}
