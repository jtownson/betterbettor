package net.jtownson.poker.handreplay.ipoker;

import net.jtownson.poker.handreplay.ipoker.watcher.ChangeListener;
import net.jtownson.poker.handreplay.ipoker.watcher.FolderWatcherFuture;
import net.jtownson.poker.handreplay.ipoker.watcher.FolderWatchers;
import org.apache.commons.lang.Validate;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Watches an IPoker history folder for changes and
 * applies them to a table model.
 */
public class HistoryDirWatcher implements Closeable {

    private FolderWatcherFuture watcherFuture;

    public HistoryDirWatcher(Path directory) throws IOException {

        File dirFile = directory.toFile();
        Validate.isTrue(dirFile.exists());
        Validate.isTrue(dirFile.isDirectory());
        Validate.isTrue(dirFile.canRead());

        watcherFuture = FolderWatchers.getInstance().addFolderListener(directory,
                new ChangeListener(ENTRY_CREATE, ENTRY_MODIFY) {
                    @Override
                    public void onEvent(WatchEvent anEvent) {
                        System.out.println("LISTENER " + anEvent.kind().name() + " " + anEvent.context());
                    }
                });
    }

    @Override
    public void close() throws IOException {
        FolderWatchers.getInstance().cancelFolderWatching(watcherFuture);
    }
}
