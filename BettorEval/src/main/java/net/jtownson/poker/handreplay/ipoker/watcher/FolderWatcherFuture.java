package net.jtownson.poker.handreplay.ipoker.watcher;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class FolderWatcherFuture implements Future {

    private FolderWatcher watcher = null;
    private Future future = null;

    public FolderWatcherFuture(FolderWatcher watcher, Future future) {
        this.watcher = watcher;
        this.future = future;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        watcher.cancel();
        return future.cancel(mayInterruptIfRunning);
    }

    @Override
    public Object get() throws InterruptedException, ExecutionException {
        return future.get();
    }

    @Override
    public Object get(long timeout, TimeUnit unit) throws InterruptedException,
            ExecutionException, TimeoutException {
        return future.get(timeout, unit);
    }

    @Override
    public boolean isCancelled() {
        return future.isCancelled();
    }

    @Override
    public boolean isDone() {
        return future.isDone();
    }
}
