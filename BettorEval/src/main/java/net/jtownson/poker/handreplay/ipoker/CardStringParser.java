/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
package net.jtownson.poker.handreplay.ipoker;

/**
 * iPoker prints cards as CK for Kc, etc.
 * Translate them to normal notation.
 * e.g. CK DA equals KcAd
 */
public class CardStringParser {


    public static String parseIPokerCards(String iPokerCards) {
        StringBuilder sb = new StringBuilder();
        String [] cardsBackwards = iPokerCards.split("\\s+");
        for (int i = 0; i < cardsBackwards.length; ++i) {
            String cardBackwards = cardsBackwards[i];
            if ("X".equals(cardBackwards)) {
                sb.append(cardBackwards);
            } else if (cardBackwards.matches("[HCDS]10")) {
                sb.append('T');
                sb.append(cardBackwards.substring(0,1).toLowerCase());
            }
            else {
                sb.append(cardBackwards.substring(1, 2));
                sb.append(cardBackwards.substring(0, 1).toLowerCase());
            }
            if (i != cardsBackwards.length - 1) {
                sb.append(' ');
            }
        }
        return sb.toString();
    }
}
