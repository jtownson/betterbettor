﻿/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Threading;


namespace IPokerSpooler 
{

    public class IPokerSpooler
    {

        public static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;

            if (args.Length < 1)
            {
                Console.Error.WriteLine("Usage: IPokerSpooler <username>");
                Environment.Exit(1);
            }

            string iPokerUsername = args[0];

            new IPokerSpooler().StartSpooling(iPokerUsername);
        }

        private void StartSpooling(string iPokerUsername)
        {
            
            IPokerTextExtractor textExtractor = new IPokerTextExtractor(iPokerUsername);

            List<string> oldChatDocument = new List<string>();
            List<string> newChatDocument;

            while (true)
            {
                Thread.Sleep(1500);

                // There are no automation events raised when 
                // the dealer chat changes. This clunky approach
                // gets the entire chat window text and diffs it with that
                // from the previous invokation.
                newChatDocument = textExtractor.GetTextFromChatWindow();

                if (newChatDocument == null)
                {
                    break;
                }

                List<string> newChatLines = GetNewLinesInReverse(oldChatDocument, newChatDocument);

                WriteToConsole(newChatLines);

                oldChatDocument = newChatDocument;
            }
        }

        private List<string> GetNewLinesInReverse(List<string> oldDocument, List<string> newDocument)
        {
            List<string> newLines = new List<string>();

            string lastOld = String.Empty;

            if (oldDocument.Count != 0)
            {
                lastOld = oldDocument[oldDocument.Count - 1];
            }

            foreach (string newLine in Backwards(newDocument))
            {
                if (newLine == String.Empty)
                {
                    continue;
                }

                if (newLine != lastOld)
                {
                    newLines.Add(newLine);
                    
                }
                else
                {
                    return newLines;
                }
            }
            return newLines; // NB: they are in reverse
        }

        private static IEnumerable<T> Backwards<T>(IList<T> items)
        {
            for (int i = items.Count - 1; i >= 0; i--)
            {
                yield return items[i];
            }
        }

        private static void WriteToConsole(List<string> newChatLines)
        {
            foreach (string chatLine in Backwards(newChatLines))
            {
                Console.WriteLine(chatLine);
            }
        }
    }
}