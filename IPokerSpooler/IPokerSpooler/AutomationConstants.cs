﻿/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPokerSpooler
{
    public static class AutomationConstants
    {
        public const int patternIdInvoke = 10000;
        public const int propertyIdBoundingRectangle = 30001;
        public const int controlTypeIdPane = 50033;
        public const int controlTypeIdTable = 50036;
        public const int patternIdValue = 10002;
        public const int propertyIdControlType = 30003;
        public const int propertyIdName = 30005;
        public const int controlTypeIdHyperlink = 50005;
    }
}
