﻿/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using interop.UIAutomationCore;
using System.Runtime.InteropServices;


namespace IPokerSpooler
{
    class IPokerTextExtractor
    {

        private int propertyIdControlType = AutomationConstants.propertyIdControlType;
        private int propertyIdName = AutomationConstants.propertyIdName;
        private int controlTypeIdHyperlink = AutomationConstants.controlTypeIdHyperlink;

        private IPokerChatWindowFinder chatWindowFinder;

        public IPokerTextExtractor(string iPokerUsername)
        {
            chatWindowFinder = new IPokerChatWindowFinder(iPokerUsername);
        }

        public List<string> GetTextFromChatWindow()
        {
            IntPtr iPokerChatWindowHandle = chatWindowFinder.FindIPokerWindow();

            if (iPokerChatWindowHandle != IntPtr.Zero)
            {
                IUIAutomation automation = new CUIAutomation();
                try
                {
                    IUIAutomationElement chatWindowAutomationElement = automation.ElementFromHandle(iPokerChatWindowHandle);

                    return BuildListOfHyperlinksFromElement(automation, chatWindowAutomationElement);
                }
                catch (Exception)
                {
                    return null;
                }                
            }

            return null;
        }

        private List<string> BuildListOfHyperlinksFromElement(IUIAutomation automation, IUIAutomationElement windowElement)
        {
            
            IUIAutomationCacheRequest cacheRequest = automation.CreateCacheRequest();

            cacheRequest.AddProperty(propertyIdName);
            cacheRequest.AddProperty(propertyIdControlType);

            TreeScope scope = TreeScope.TreeScope_Element;

            cacheRequest.TreeScope = scope;

            IUIAutomationCondition controlCondition = automation.ControlViewCondition;
            IUIAutomationCondition linkCondition = automation.CreatePropertyCondition(propertyIdControlType, controlTypeIdHyperlink);
            IUIAutomationCondition condition = automation.CreateAndCondition(controlCondition, linkCondition);

            IUIAutomationElementArray elementArray =
                windowElement.FindAllBuildCache(TreeScope.TreeScope_Descendants, condition, cacheRequest);

            List<string> linkText = new List<string>();

            if (elementArray != null)
            {
                int cLinks = elementArray.Length;

                for (int idxLink = 0; idxLink < cLinks; ++idxLink)
                {
                    IUIAutomationElement elementLink = elementArray.GetElement(idxLink);

                    string elmtName = elementLink.CachedName.Trim();

                    if (elmtName != null && elmtName != String.Empty && ! elmtName.StartsWith("Dealer"))
                    {                        
                        linkText.Add(elmtName);
                    }
                }
            }
            return linkText;
        }
    }

}
