﻿/*
	Copyright © Jeremy Townson 2013

    This file is part of betterbettor.

    betterbettor is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    betterbettor is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with betterbettor.  If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;


namespace IPokerSpooler
{

    public class WindowFinder
    {
        public static IntPtr SearchForWindow(string wndclass, string title, uint pid)
        {
            SearchData sd = new SearchData { Wndclass = wndclass, Title = title, processId = pid };
            EnumWindows(new EnumWindowsProc(EnumProc), ref sd);
            return sd.hWnd;
        }

        public static IntPtr SearchForWindow(string wndclass, string title)
        {
            SearchData sd = new SearchData { Wndclass = wndclass, Title = title };
            EnumWindows(new EnumWindowsProc(EnumProc), ref sd);
            return sd.hWnd;
        }

        public static IntPtr SearchForWindow(uint pid)
        {
            SearchData sd = new SearchData { processId = pid };
            EnumWindows(new EnumWindowsProc(EnumProc), ref sd);
            return sd.hWnd;
        }

        private static string GetWindowClassName(IntPtr hWnd)
        {
            StringBuilder sb = new StringBuilder(1024);
            GetClassName(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        private static string GetWindowText(IntPtr hWnd)
        {
            StringBuilder sb = new StringBuilder(1024);
            GetWindowText(hWnd, sb, sb.Capacity);
            return sb.ToString();
        }

        private static uint GetWindowPid(IntPtr hWnd)
        {
            uint pid = 0;
            GetWindowThreadProcessId(hWnd, out pid);
            return pid;
        }

        public static bool EnumProc(IntPtr hWnd, ref SearchData data)
        {
            uint processId = GetWindowPid(hWnd);

            if (data.processId == 0 || data.processId == processId) 
            {
                string wndClass = GetWindowClassName(hWnd);

                if (data.Wndclass == String.Empty || wndClass.StartsWith(data.Wndclass)) 
                {
                    string wndText = GetWindowText(hWnd);

                    if (data.Title == String.Empty || wndText.Contains(data.Title)) 
                    {
                        data.hWnd = hWnd;
                        return false;
                    }

                }
            }
            return true;
        }

        public class SearchData
        {
            public string Wndclass = String.Empty;
            public string Title = String.Empty;
            public uint processId = 0;
            public IntPtr hWnd = IntPtr.Zero;
        }


        // When you don't want the ProcessId, use this overload and pass IntPtr.Zero for the second parameter
        [DllImport("user32.dll")]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        private delegate bool EnumWindowsProc(IntPtr hWnd, ref SearchData data);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool EnumWindows(EnumWindowsProc lpEnumFunc, ref SearchData data);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

    }
}
